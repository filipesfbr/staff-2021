class Elfo {
    constructor( nome ){
        this._nome = nome;
    }

    matarElfo(){
        this.status = "morto";
    }

    get estaMorto(){
        return this.status = "morto";
    }

    get nome(){
        return this._nome;
    }

    atacarComFlecha(){
        setTimeout( () => { // setTimeout() = ESPERAR UM TEMPO ANTES DE EXECUTAR. PRIMEIRO A AÇÃO, DEPOIS O TEMPO (EM MILISSEGUNDOS(2000 ( 2s )))
            console.log("Atacou");
        }, 2000);
        
    }
}

let legolas = new Elfo("Legolas");
legolas.matarElfo();
legolas.estaMorto;
legolas.atacarComFlecha();

