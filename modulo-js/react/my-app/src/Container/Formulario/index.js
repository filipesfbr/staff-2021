import React, { useState, useEffect } from 'react';
import EpisodioAPI from '../../models/EpisodiosAPI';




function Formulario( props ) {
  const [ qtdVezesAssistido, setQtdVezesAssistido ] = useState(1);
  const [ nome, setNome ] = useState('Filipe');
  const [ idFilme, setIdFilme ] = useState ( 0 );
  const [ notas, setNotas ] = useState( [] );

  useEffect( () => {
    let episodioApi = new EpisodioAPI();
    episodioApi.buscarNota( idFilme ).then( e => setNotas( e ) );
  }, [ idFilme ] );


  return (
    <React.Fragment>
      <h1>Quantidade de Vezes Assistidas: { qtdVezesAssistido } </h1>
      <button type="button" onClick={ () => setQtdVezesAssistido( qtdVezesAssistido + 1) } >Já Assisti</button>
      <input type="text" onBlur={ evt => setNome( evt.target.value ) } />
      <p>{ nome }</p>
      <input type="text" onBlur={ evt => setIdFilme( evt.target.value ) } />
      { notas.map( ( e, i ) => <div key={ i }>{ e.nota }</div> ) }
    </React.Fragment>
  )

}

export default Formulario;