import React, { Component } from 'react';

import EpisodioUi from '../Components/EpisodioUi';
import MensagemFlash from '../Components/MensagemFlash';
import MeuInputNumero from '../Components/MeuInputNumero';
import BotaoUi from '../Components/BotaoUi';

import Mensagem from '../Constants/mensagem';

import './App.css';

import EpisodioAPI from '../models/EpisodiosAPI';
import ListaEpisodios from '../models/ListaEpisodios';

export default class App extends Component {
  constructor( props ) {
    super( props );
    this.episodioAPI = new EpisodioAPI();
    this.sortear = this.sortear.bind( this );
    this.marcarComoAssistido = this.marcarComoAssistido.bind( this );
    this.state = {
      deveExibirMensagem: false,
      deveExibirErro: false,
      mensagem: ''
    }
  }

  componentDidMount() {
    this.episodioAPI.buscar()
      .then( episodio => {
        this.listaEpisodios = new ListaEpisodios( episodio );
        this.setState( state => {
           return {
            ...state,
            episodio: this.listaEpisodios.episodioAleatorio 
          } });
      }); 
  }


  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio;
    
    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });

  }

  marcarComoAssistido() {
    const { episodio } = this.state;
    episodio.assistido();
    this.setState((state) => {
      return { 
            ...state,
            episodio
      }
    });
  }

  registrarNota( { erro, nota } ) {
    this.setState((state) => {
      return { 
        ...state,
        deveExibirErro: erro
      }

    });
    if( erro ) {
      return;
    }
    
    const { episodio } = this.state;
    let mensagem, corMensagem;
    
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota ).then( () => {
        corMensagem = 'verde';
        mensagem = Mensagem.SUCESSO.REGISTRO_NOTA;
        this.exibirMensagem({ corMensagem, mensagem });
      } );
    }else{
      corMensagem = 'vermelho';
      mensagem = Mensagem.ERRO.NOTA_INVALIDA;
      this.exibirMensagem({ corMensagem, mensagem });
    }
  }

  exibirMensagem = ({ corMensagem, mensagem }) => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: true,
            mensagem,
            corMensagem
      }
    });
  }

  atualizarMensagem = devoExibir => {
    this.setState((state) => {
      return { 
            ...state,
            deveExibirMensagem: devoExibir
      }
    });
  }
  
  render() {
    const { episodio, deveExibirMensagem, corMensagem, mensagem, deveExibirErro } = this.state;
    const { listaEpisodios } = this;

    return (
      !episodio ?
        ( <h3>Aguarde...</h3> ) :
        ( <div className="App">
           <MensagemFlash atualizar={ this.atualizarMensagem } exibir={ deveExibirMensagem } mensagem={ mensagem } cor={ corMensagem } />

           <header className="App-header">

            <BotaoUi classe="preto" link={{ pathname: "/avaliacoes", state: { listaEpisodios } }} nome="Lista de avaliações" />
            <BotaoUi classe="preto" link={{ pathname: "/todos", state: { listaEpisodios } }} nome="Todos Episódios" />

            <EpisodioUi episodio={ episodio } />

            <div className="botoes">
              <BotaoUi classe="verde" metodo={ this.sortear } nome="Próximo" />
              <BotaoUi classe="azul" metodo={ this.marcarComoAssistido } nome="Já Assisti!" />
            </div>

            {episodio.notas.length > 0 && (
              <span>Nota: { episodio.media }</span>
            )}

            <MeuInputNumero
                placeholder="Nota de 1 a 5"
                mensagem={ Mensagem.DESCRICAO.INPUT_EPISODIO }
                visivel={ episodio.foiAssistido || false }
                obrigatorio
                exibirErro={ deveExibirErro }
                atualizarValor={ this.registrarNota.bind( this ) }/>
                
          </header>
        </div> )
    );
  }
}