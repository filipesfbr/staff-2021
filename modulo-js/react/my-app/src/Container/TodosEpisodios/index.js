import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import EpisodioAPI from '../../models/EpisodiosAPI';
import ListaEpisodios from '../../models/ListaEpisodios';
import BotaoUi from '../../Components/BotaoUi';

export default class TodosEpisodios extends Component {
  constructor( props ) {
    super( props )
    this.episodioAPI = new EpisodioAPI();
    this.state= {
      ordenacao: () => { },
      tipoOrdenacaoDataEstreia: 'ASC',
      tipoOrdenacaoDuracao: 'ASC'
    };
    
  }

  
  componentDidMount(){
    this.episodioAPI.buscar()
      .then( episodio => {
        this.setState( state => {
          return {
            ...state,
            listaEpisodios: new ListaEpisodios( episodio )
          }
        })
      })
  }

  alterarOrdenacaoParaDataEstreia = () => {
    const { tipoOrdenacaoDataEstreia } = this.state;
    this.setState( { 
      ordenacao: ( a, b ) => new Date( ( tipoOrdenacaoDataEstreia === 'ASC' ? a : b ).dataEstreia )
       - new Date ( ( tipoOrdenacaoDataEstreia === 'ASC' ? b : a ).dataEstreia ),
       tipoOrdenacaoDataEstreia: tipoOrdenacaoDataEstreia === 'ASC' ? 'DESC' : 'ASC'
    })
  }

  alternarOrdenacaoParaDuracao = () => {
    const { tipoOrdenacaoDuracao } = this.state;
    this.setState( { 
      ordenacao: ( a, b ) => ( ( tipoOrdenacaoDuracao === 'ASC' ? a : b ).duracao )
       - ( ( tipoOrdenacaoDuracao === 'ASC' ? b : a ).duracao ),
       tipoOrdenacaoDuracao: tipoOrdenacaoDuracao === 'ASC' ? 'DESC' : 'ASC'
    })
  }


  render() {
    const { listaEpisodios } = this.state
    return (
      !listaEpisodios ?
      ( <h3>Aguarde...</h3>)
      :
      <React.Fragment>
      <BotaoUi classe="laranja" link="/" nome="Página Inicial" />
        <h1>Todos Episódios: </h1>
        {
          listaEpisodios.todos && (
            listaEpisodios.todos.map( ( episodio, i) => {
              return <li key={ i }>
              <Link to={{ pathname: `/detalhe`, state: { episodio: episodio } }}>{ episodio.nome }</Link> 
              </li>
            })
          )
        }
        
      </React.Fragment>
    )

  }
}


