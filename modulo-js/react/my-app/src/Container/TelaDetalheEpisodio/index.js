import React, { Component } from 'react';

import EpisodioAPI from '../../models/EpisodiosAPI';
import BotaoUi from '../../Components/BotaoUi';

export default class TelaDetalheEpisodio extends Component {
  constructor(props) {
    super(props)
    this.episodioApi = new EpisodioAPI();

    this.state = {
      detalhes: null,
      carregado : false,
      episodio : this.props.location.state.episodio
    }
  }


  componentDidMount() {
    this.episodioApi.episodioDetalhes(this.state.episodio.id)
      .then(detalhes => {

        const { episodio } = this.state;

        episodio.notaImdb = ((detalhes.notaImdb / 10) * 5).toFixed(2) ;
        episodio.sinopse = detalhes.sinopse;
        episodio.dataEstreia = detalhes.dataEstreia;

        this.setState(state => {
          return {
            ...state,
            carregado: true,
            episodio
          }
        })
      })

  }


  render() {
    const { episodio, carregado } = this.state;
   

    return (
      !carregado ?
        (<h3>Aguarde...</h3>)
        : (
          <div className="App-header">
            <BotaoUi classe="preto" link="/" nome="Página Inicial" />

            <p>Nome: { episodio.nome }</p>
            <img src={ episodio.imagem } alt={ episodio.nome } />
            <p>Temporada/Episodio: { episodio.temporadaEpisodio }</p>
            <p>Duração em minutos: { episodio.duracaoEmMin }</p>
            <p>Nota: { episodio.media || 'Sem nota'}</p>
            <p>Nota IMDB: { episodio.notaImdb }</p>
            <p>Sinopse: { episodio.sinopse }</p>
            <p>Data Estreia: { new Date( episodio.dataEstreia ).toLocaleString()}</p>

          </div>
        )
    );
  }
}


