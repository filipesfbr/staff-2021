import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import App from './App';
import TelaDetalheEpisodio from '../Container/TelaDetalheEpisodio';
import TodosEpisodios from './TodosEpisodios';
import ListaAvaliacoes from './Avaliacoes';
import { RotasPrivadas } from '../Components/RotasPrivadas';
import Formulario from './Formulario';

export default class Rotas extends Component {
  render(){
    return (
      <Router>
        <Route path="/" exact component={ App } />
        <Route path="/formulario" component={ Formulario } />
        <Route path="/avaliacoes" exact component={ ListaAvaliacoes } />
        <Route path="/detalhe" exact component={ TelaDetalheEpisodio } />
        <RotasPrivadas path="/todos" exact component={ TodosEpisodios } />
      </Router>
    );
  }
}

/* const PaginaTeste = () =>
  <div>
    <h1>Página Teste</h1>
    <Link to="/">Página App</Link>
  </div> */