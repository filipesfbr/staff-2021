import React from 'react';
import { Link } from 'react-router-dom';

import BotaoUi from '../../Components/BotaoUi';

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state;

  return (
    <React.Fragment>
      <BotaoUi classe="preto" link="/" nome="Página Inicial" />
      {
        listaEpisodios.avaliados && (
          listaEpisodios.avaliados.map( ( episodio, i ) => {
            
            return <li key={i}>
              <Link to={{ pathname: `/detalhe`, state: { episodio: episodio } }}>
                { `${episodio.nome} - ${episodio.media}` }
              </Link>
            </li>
            
            // return <BotaoUi
            // key={ i } 
            // classe="linkCor"
            // link={{ pathname: "/detalhe", state: { episodio }}} 
            // nome={ `Episódio: ${ episodio.nome } | Nota: ${ episodio.media }` } />

          })
        )
      } 
     
    </React.Fragment>
  );
} 

export default ListaAvaliacoes;