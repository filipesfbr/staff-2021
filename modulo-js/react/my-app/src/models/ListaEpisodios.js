import Episodio from "./Episodio";

function sortear( min, max ) {
  min = Math.ceil( min );
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

export default class ListaEpisodios {
  constructor( episodioAPI ) {
    this.todos = episodioAPI.map( episodio => new Episodio( episodio ) );
  }
  

  get episodioAleatorio() {
    const indice = sortear( 0, this.todos.length );
    return this.todos[ indice ];
  }

  get avaliados() {
    const avaliados = this.todos.filter( e => e.notas.length > 0 )
                                .sort( ( a, b ) => a.temporada - b.temporada || a.ordemEpisodio - b.ordemEpisodio );
    return avaliados;
  }

}