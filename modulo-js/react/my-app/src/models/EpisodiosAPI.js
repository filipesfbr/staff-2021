import axios from 'axios';

const endereco = 'http://localhost:9000/api';

export default class EpisodioAPI {

  buscar() {
    return axios.get( `${ endereco }/episodios` ).then( e => e.data ).then(a => a);
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( `${ endereco }/notas`, { nota, episodioId } );
  }

  buscarNotas(){
    return axios.get( `${endereco}/notas` ).then( n => n.data );
  }

  buscarNota( id ){
    return axios.get( `${ endereco }/notas?episodioId=${ id }` ).then( e => e.data );
  }

  episodioDetalhes( episodioId ) {
    return axios.get( `${endereco}/episodios/${episodioId}/detalhes `).then(e => e.data[0]);
  }

  buscarTodasNotas(){
    return axios.get( `${ endereco }/notas` ).then( e => e.data );
  }
  
}