import EpisodioAPI from './EpisodiosAPI'

export default class Episodio {
  constructor({ id, nome, duracao, temporada, ordemEpisodio, thumbUrl }) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.imagem = thumbUrl;
    this.qtdVezesAssistido = 0;
    this.episodioApi = new EpisodioAPI();
    this.notas = [];
  }

  get duracaoEmMin() {
    return `${this.duracao} min`;
  }

  get temporadaEpisodio() {
    return `${this.temporada.toString().padStart(2, 0)}/${this.ordem.toString().padStart(2, 0)}`;
  }

  avaliar(nota) {
    this.notas.push(parseInt(nota));
    return this.episodioApi.registrarNota({ nota: nota, episodioId: this.id });
  }

  assistido() {
    this.foiAssistido = true;
    this.qtdVezesAssistido += 1;
  }

  validarNota(nota) {
    return (nota >= 1 && nota <= 5);
  }

  get media() {
    let soma = this.notas.reduce(( a, b ) => a + b );
    parseInt(soma);
    return (soma / this.notas.length).toFixed(2);
  }

}