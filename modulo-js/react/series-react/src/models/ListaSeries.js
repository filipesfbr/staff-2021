import Serie from "./Serie"
import PropTypes from 'prop-types';

export default class ListaSeries {
  constructor() {
    this.todos = [
      {
        "titulo": "Stranger Things",
        "anoEstreia": 2016,
        "diretor": [
          "Matt Duffer",
          "Ross Duffer"
        ],
        "genero": [
          "Suspense",
          "Ficcao Cientifica",
          "Drama"
        ],
        "elenco": [
          "Winona Ryder",
          "David Harbour",
          "Finn Wolfhard",
          "Millie Bobby Brown",
          "Gaten Matarazzo",
          "Caleb McLaughlin",
          "Natalia Dyer",
          "Charlie Heaton",
          "Cara Buono",
          "Matthew Modine",
          "Noah Schnapp"
        ],
        "temporadas": 2,
        "numeroEpisodios": 17,
        "distribuidora": "Netflix"
      },
      {
        "titulo": "Game Of Thrones",
        "anoEstreia": 2011,
        "diretor": [
          "David Benioff",
          "D. B. Weiss",
          "Carolyn Strauss",
          "Frank Doelger",
          "Bernadette Caulfield",
          "George R. R. Martin"
        ],
        "genero": [
          "Fantasia",
          "Drama"
        ],
        "elenco": [
          "Peter Dinklage",
          "Nikolaj Coster-Waldau",
          "Lena Headey",
          "Emilia Clarke",
          "Kit Harington",
          "Aidan Gillen",
          "Iain Glen ",
          "Sophie Turner",
          "Maisie Williams",
          "Alfie Allen",
          "Isaac Hempstead Wright"
        ],
        "temporadas": 7,
        "numeroEpisodios": 67,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The Walking Dead",
        "anoEstreia": 2010,
        "diretor": [
          "Jolly Dale",
          "Caleb Womble",
          "Paul Gadd",
          "Heather Bellson"
        ],
        "genero": [
          "Terror",
          "Suspense",
          "Apocalipse Zumbi"
        ],
        "elenco": [
          "Andrew Lincoln",
          "Jon Bernthal",
          "Sarah Wayne Callies",
          "Laurie Holden",
          "Jeffrey DeMunn",
          "Steven Yeun",
          "Chandler Riggs ",
          "Norman Reedus",
          "Lauren Cohan",
          "Danai Gurira",
          "Michael Rooker ",
          "David Morrissey"
        ],
        "temporadas": 9,
        "numeroEpisodios": 122,
        "distribuidora": "AMC"
      },
      {
        "titulo": "Band of Brothers",
        "anoEstreia": 20001,
        "diretor": [
          "Steven Spielberg",
          "Tom Hanks",
          "Preston Smith",
          "Erik Jendresen",
          "Stephen E. Ambrose"
        ],
        "genero": [
          "Guerra"
        ],
        "elenco": [
          "Damian Lewis",
          "Donnie Wahlberg",
          "Ron Livingston",
          "Matthew Settle",
          "Neal McDonough"
        ],
        "temporadas": 1,
        "numeroEpisodios": 10,
        "distribuidora": "HBO"
      },
      {
        "titulo": "The JS Mirror",
        "anoEstreia": 2017,
        "diretor": [
          "Lisandro",
          "Jaime",
          "Edgar"
        ],
        "genero": [
          "Terror",
          "Caos",
          "JavaScript"
        ],
        "elenco": [
          "Amanda de Carli",
          "Alex Baptista",
          "Gilberto Junior",
          "Gustavo Gallarreta",
          "Henrique Klein",
          "Isaias Fernandes",
          "João Vitor da Silva Silveira",
          "Arthur Mattos",
          "Mario Pereira",
          "Matheus Scheffer",
          "Tiago Almeida",
          "Tiago Falcon Lopes"
        ],
        "temporadas": 5,
        "numeroEpisodios": 40,
        "distribuidora": "DBC"
      },
      {
        "titulo": "Mr. Robot",
        "anoEstreia": 2018,
        "diretor": [
          "Sam Esmail"
        ],
        "genero": [
          "Drama",
          "Techno Thriller",
          "Psychological Thriller"
        ],
        "elenco": [
          "Rami Malek",
          "Carly Chaikin",
          "Portia Doubleday",
          "Martin Wallström",
          "Christian Slater"
        ],
        "temporadas": 3,
        "numeroEpisodios": 32,
        "distribuidora": "USA Network"
      },
      {
        "titulo": "Narcos",
        "anoEstreia": 2015,
        "diretor": [
          "Paul Eckstein",
          "Mariano Carranco",
          "Tim King",
          "Lorenzo O Brien"
        ],
        "genero": [
          "Documentario",
          "Crime",
          "Drama"
        ],
        "elenco": [
          "Wagner Moura",
          "Boyd Holbrook",
          "Pedro Pascal",
          "Joann Christie",
          "Mauricie Compte",
          "André Mattos",
          "Roberto Urbina",
          "Diego Cataño",
          "Jorge A. Jiménez",
          "Paulina Gaitán",
          "Paulina Garcia"
        ],
        "temporadas": 3,
        "numeroEpisodios": 30,
        "distribuidora": null
      },
      {
        "titulo": "Westworld",
        "anoEstreia": 2016,
        "diretor": [
          "Athena Wickham"
        ],
        "genero": [
          "Ficcao Cientifica",
          "Drama",
          "Thriller",
          "Acao",
          "Aventura",
          "Faroeste"
        ],
        "elenco": [
          "Anthony I. Hopkins",
          "Thandie N. Newton",
          "Jeffrey S. Wright",
          "James T. Marsden",
          "Ben I. Barnes",
          "Ingrid N. Bolso Berdal",
          "Clifton T. Collins Jr.",
          "Luke O. Hemsworth"
        ],
        "temporadas": 2,
        "numeroEpisodios": 20,
        "distribuidora": "HBO"
      },
      {
        "titulo": "Breaking Bad",
        "anoEstreia": 2008,
        "diretor": [
          "Vince Gilligan",
          "Michelle MacLaren",
          "Adam Bernstein",
          "Colin Bucksey",
          "Michael Slovis",
          "Peter Gould"
        ],
        "genero": [
          "Acao",
          "Suspense",
          "Drama",
          "Crime",
          "Humor Negro"
        ],
        "elenco": [
          "Bryan Cranston",
          "Anna Gunn",
          "Aaron Paul",
          "Dean Norris",
          "Betsy Brandt",
          "RJ Mitte"
        ],
        "temporadas": 5,
        "numeroEpisodios": 62,
        "distribuidora": "AMC"
      }
    ].map( serie => new Serie ( serie ) );
  }


  invalidas(){ // undefined não pega
    let seriesInvalidas = this.todos.filter( serie => {
      return !serie.titulo       
      ||  !serie.anoEstreia      
      ||  !serie.diretor         
      ||  !serie.genero         
      ||  !serie.elenco         
      ||  !serie.temporadas      
      ||  !serie.numeroEpisodios
      ||  !serie.distribuidora   
      ||  serie.anoEstreia > new Date().getFullYear()
    })
    return `Séries Inválidas: ${seriesInvalidas.map(serie => serie.titulo).join(' - ')}`
  }

  invalidasMarcos() {
    const invalidas = this.todos.filter( serie => {
      const algumCampoInvalido = Object.values(serie).some( campo => campo === null || typeof campo === 'undefined');
      const anoEstreiaInvalido = serie.anoEstreia > new Date().getFullYear();
        return algumCampoInvalido || anoEstreiaInvalido;
    } );
    return `Séries Inválidas: ${invalidas.map(serie => serie.titulo).join(' - ') }`
  }

  filtrarPorAno( ano ){
    let seriesFiltradas = this.todos.filter( serie => {
      return serie.anoEstreia >= ano;
    })
    return seriesFiltradas;
  }

  filtrarPorAnoMarcos( ano ){
    return this.todos.filter( serie => serie.anoEstreia >= ano );
  }
 
  procurarPorNome(nomePesquisa){
    let elenco = this.todos.map(pessoa => pessoa.elenco);
    for(let i = 0; i < elenco.length; i++){
      for(let j = 0; j < elenco[i].length; j++){
        if(elenco[i][j] === nomePesquisa){
          return true;
        }
      }
    }
    return false;
  }

  procurarPorNomeMarcos( nome ){
    let encontrou = false;
    this.todos.forEach( serie => {
      let achou = serie.elenco.find( n => nome === n );
      if( achou ){
        encontrou = true;
      }
    })
    return encontrou;
  }

  mediaDeEpisodios(){
    let qtdTotalEpisodios = 0;
    let elencos = this.todos;
    for(let i = 0; i < elencos.length; i++){
      qtdTotalEpisodios += elencos[i].numeroEpisodios;
    }
    return (qtdTotalEpisodios/elencos.length).toFixed(2);
  }
  
  mediaDeEpisodiosMarcos(){
    return parseFloat(this.todos.map( serie => serie.numeroEpisodios )
            .reduce( ( prev, cur ) => prev + cur, 0) / this.todos.length );

  }
  
  totalSalarios( indice ){
    let serie = this.todos[indice];
    let qtdDiretores = serie.diretor.length;
    let qtdElencos = serie.elenco.length;
    
    return ((qtdDiretores * 100000) + (qtdElencos * 40000))
    .toLocaleString('pt-br', {style: 'currency', currency: 'BRL'});
  }

  queroGenero( genero ){
    let series = this.todos.filter( serie => {
      let generos = serie.genero;
      for(let atual of generos){ // o filter vai retornar um array para o series se a condição for true
        if( atual.toUpperCase() === genero.toUpperCase() )
          return true;
      }
      return false
    });
    return series.map(serie => serie.titulo);
  }

  queroTitulo( titulo ){
    let series = this.todos.filter( serie => { // O Filter itera sobre o array de séries todo e pega apenas o titulo de cada série e compara com o parametro se inclui
        if( serie.titulo.toUpperCase().includes(titulo.toUpperCase()) ){
          return true;
        }
      return false;
    });
    return series.map(serie => serie.titulo);
  }

  queroTituloMarcos( titulo ){
    const palavrasTitulo = titulo.split(' ');
    return this.todos.filter( serie => {
      let palavraSerie = serie.titulo.split(' ');
      return Boolean( palavraSerie.find(palavra => palavra.includes(palavraSerie)))
    })
  }

  creditos(){
    for(let serie of this.todos){
      console.log(`\t\t${serie.titulo}`)

      const { diretor: nomesDiretores, elenco: nomesElencos } = serie;

      let nomesDiretoresOrdenado = nomesDiretores.sort( ( diretor1, diretor2 ) => {
        const nome1Splitado = diretor1.trim().split( ' ' );
        const nome2Splitado = diretor2.trim().split( ' ' );

        return nome1Splitado[nome1Splitado.length - 1].localeCompare(nome2Splitado[nome2Splitado.length - 1]);
      });

      let nomesElencosOrdenado = nomesElencos.sort( ( integrante1, integrante2 ) => {
        const nome1Splitado = integrante1.trim().split( ' ' );
        const nome2Splitado = integrante2.trim().split( ' ' );

        return nome1Splitado[nome1Splitado.length - 1].localeCompare(nome2Splitado[nome2Splitado.length - 1]);
      });

      console.log(`\tDiretor(es)`)
      for(let diretor of nomesDiretoresOrdenado){
        console.log(`${diretor}`)
      }

      console.log(`\tElenco`)
      for(let elenco of nomesElencosOrdenado){
        console.log(`${elenco}`)
      }
    }
    
  }

  creditosMarcos( indice ){

    this.todos [ indice ].elenco.sort( ( a, b ) => {
      let arrayA = a.split( ' ' );
      let arrayB = b.split( ' ' );
      return arrayA [ arrayA.length - 1].localeCompare( arrayB[ arrayB.length - 1] );
    })

    this.todos [ indice ].diretor.sort( ( a, b ) => {
      let arrayA = a.split( ' ' );
      let arrayB = b.split( ' ' );
      return arrayA [ arrayA.length - 1].localeCompare( arrayB[ arrayB.length - 1] );
    })

    let arrayParaJuntar = [];

    arrayParaJuntar.push( `\t\t${ this.todos[ indice ].titulo }` );
    arrayParaJuntar.push( `\tDiretores` );

    this.todos[ indice ].diretor.forEach( d => {
      arrayParaJuntar.push( `${ d }` );
    })

    arrayParaJuntar.push( `\tElenco` );

    this.todos[ indice ].elenco.forEach( e => {
      arrayParaJuntar.push( `${ e }` );
    })

    return arrayParaJuntar.join( '\n ');
  }

  nomesAbreviados( nomes ) {
    let verifica = nomes.match( '\\s+[a-zA-Z]{1}\\.\\s+' );
    return ( verifica ) ? Boolean( verifica[0] ) : undefined;
  }

  seriesNomesAbreviados() {
    return this.todos.filter( serie => {
      return !serie.elenco.find( s => !this.nomesAbreviados( s ) );
    } )
  }

  letrasAbreviadas( nomes ) {
    if( this.nomesAbreviados( nomes ) ) {
      return nomes.match( '\\s+[a-zA-Z]{1}\\.\\s+' )[0].charAt(1);
    }
  }

  hashtag() {
    let palavra = '#';
    let array = this.seriesNomesAbreviados();
    array[0].elenco.forEach( s => {
      palavra = palavra.concat( this.letrasAbreviadas( s ) );
    } )
    return palavra;
  }

  testandoSome(){
    let array = [ 10, 20, 30, 40, 50];
    if( array.some(elementoArray => elementoArray > 70) ){
      return true;
    }
    return false;
  }

  testandoMatch(){
    let str = "Filipe S. Ferreira";
    let regexp = /([i - t])/gi;
    
    return str.match(regexp).replaceAll(' ', '');
  }

}

ListaSeries.propTypes = {
  invalidas: PropTypes.array, // Utilizar das propriedades do array

  filtrarPorAno: PropTypes.array,

  procurarPorNome: PropTypes.array,

  mediaDeEpisodios: PropTypes.array,

  totalSalarios: PropTypes.array,

  queroGenero: PropTypes.array,

  queroTitulo: PropTypes.array,

  creditos: PropTypes.array
};
