/* let dadosPokemon = document.getElementById("dadosPokemon");

let pokeApi = new PokeApi();
let buscou = pokeApi.buscarEspecifico(112);
buscou.then( pokemon => {
    let nome = dadosPokemon.querySelector( ".nome" );
    nome.innerHTML = pokemon.name;

    let imagem = dadosPokemon.querySelector( ".thumb" );
    imagem.src = pokemon.sprites.front_default;
}); */

function validaId() {
  const span = document.getElementById( 'validaId' );
  const id = document.getElementById( 'input-pokemon' ).value;

  if ( ( id >= 1 && id <= 898 ) || ( id >= 10001 && id <= 10220 ) ) {
    span.innerHTML = '';
    return true;
  }
  span.innerHTML = 'Digite um id válido';
  return false;
}

let idAtual;
const pokeApi = new PokeApi();

function buscarPokemon() { // eslint-disable-line no-unused-vars
  const id = document.getElementById( 'input-pokemon' ).value;

  if ( validaId() ) {
    if ( id !== idAtual ) {
      pokeApi.buscarEspecifico( id )
        .then( pokemon => {
          const poke = new Pokemon( pokemon );
          renderizar( poke ); // eslint-disable-line no-undef
        } );
    }

    idAtual = id;
    document.getElementById( 'input-pokemon' ).value = '';
  }
}

renderizar = ( pokemon ) => { // eslint-disable-line no-undef
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = `#${ pokemon.id } ${ pokemon.nome }`;

  /* let nome = dadosPokemon.querySelector(".nome");
  nome.innerHTML = pokemon.nome; */

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ pokemon.peso }`;

  this.tiposTabela( pokemon.tipos );
  // let tipos = dadosPokemon.querySelector(".tipos");
  // tipos.innerHTML = pokemon.tipos;

  this.estatisticasTabela( pokemon.estatisticas );
  // let estatistiinnerHTML = "Digite um id v
}

function estatisticasTabela( estatisticas ) { // eslint-disable-line no-unused-vars
  const tbody = document.getElementById( 'tbodyStats' );
  tbody.innerText = '';

  const trr = tbody.insertRow();
  const tdAtributo = trr.insertCell();
  const tdQuantidade = trr.insertCell();

  tdAtributo.innerText = 'Atributo';
  tdQuantidade.innerText = 'Quantidade';
  for ( let i = 0; i < estatisticas.length; i++ ) { // eslint-disable-line no-plusplus
    const tr = tbody.insertRow();


    const tdStat = tr.insertCell();
    const tdBase = tr.insertCell();


    tdStat.innerText = estatisticas[i].stat.name.toUpperCase().replace( '-', ' ' );
    tdBase.innerText = estatisticas[i].base_stat;
  }
}

function tiposTabela( tipos ) { // eslint-disable-line no-unused-vars
  const tbody = document.getElementById( 'tbodyTipo' );
  tbody.innerText = '';

  const trr = tbody.insertRow();
  const tdNome = trr.insertCell();
  tdNome.innerText = 'Tipos ';

  for ( let i = 0; i < tipos.length; i += 1 ) {
    const tr = tbody.insertRow();

    const tdNomeTipo = tr.insertCell();

    tdNomeTipo.innerText = tipos[i].toUpperCase();
  }
}

function sortearNumero() {
  return Math.floor( Math.random() * 893 + 1 );
}

function estouComSorte() { // eslint-disable-line no-unused-vars
  const span = document.getElementById( 'validaId' );
  let arraySorteados = JSON.parse( localStorage.getItem( 'arraySorteados' ) || '[]' );
  let numeroRandom = sortearNumero();

  while ( arraySorteados.indexOf( numeroRandom ) >= 0 ) {
    numeroRandom = sortearNumero();
  }

  arraySorteados = [...arraySorteados, numeroRandom];
  localStorage.setItem( 'arraySorteados', JSON.stringify( arraySorteados ) );

  span.innerHTML = '';
  pokeApi.buscarEspecifico( numeroRandom )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke ); // eslint-disable-line no-undef
    } );

  idAtual = numeroRandom;
  document.getElementById( 'input-pokemon' ).value = '';
}


function estouComSorte2() { // eslint-disable-line no-unused-vars
  const span = document.getElementById( 'validaId' );

  const arraySorteados = [];
  let numeroRandom = Math.random() * ( 893 - 1 ) + 1;
  let numeroSorteado = parseInt( numeroRandom ); // eslint-disable-line radix

  while ( arraySorteados.indexOf( numeroSorteado ) >= 0 ) {
    numeroRandom = Math.random() * ( 893 - 1 ) + 1;
    numeroSorteado = parseInt( numeroRandom ); // eslint-disable-line radix
  }
  arraySorteados.push( numeroSorteado );

  span.innerHTML = '';
  pokeApi.buscarEspecifico( numeroSorteado )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke ); // eslint-disable-line no-undef
    } );

  idAtual = numeroSorteado;
  document.getElementById( 'input-pokemon' ).value = '';
}
