class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objDaApi ) {
    this._id = objDaApi.id;
    this._nome = objDaApi.name;
    this._imagem = objDaApi.sprites.front_default;
    this._altura = objDaApi.height;
    this._peso = objDaApi.weight;

    this._tipos = objDaApi.types;
    this._estatisticas = objDaApi.stats;
  }

  get id() {
    return this._id;
  }

  get nome() {
    return this._nome.toUpperCase();
  }

  get imagem() {
    return this._imagem;
  }

  get altura() {
    return `${ this._altura * 10 } cm` // transformar em centímetros
  }

  get peso() {
    return `${ this._peso / 10 } kg` // transformar em kg
  }

  get tipos() {
    const resultadoTipos = [];
    for ( let i = 0; i < this._tipos.length; i += 1 ) {
      resultadoTipos.push( this._tipos[i].type.name );
    }
    return resultadoTipos;
  }

  get estatisticas() {
    return this._estatisticas;
  }
}
