function cardapioIFood(veggie = true, comLactose = false) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva',
    'pastel de carne'
  ]
  if (!comLactose) {
    cardapio.push('pastel de queijo');
  }
  cardapio.push('empada de legumes marabijosa');
    
    
  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    let indiceEnroladinho = cardapio.indexOf('enroladinho de salsicha');
    cardapio.splice(indiceEnroladinho, 1);

    let indicePastelCarne = cardapio.indexOf('pastel de carne');
    cardapio.splice(indicePastelCarne, 1);
  }
    
  let resultadoFinal = []
  for (let i = 0; i < cardapio.length; i++) {
    resultadoFinal.push(cardapio[i].toUpperCase());
  }
    
  return console.log(resultadoFinal);
}

//cardapioIFood() // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
   

/** Correção Marcos */

function cardapioIFoodM(veggie = true, comLactose = true) {
  let cardapio = [
    'enroladinho de salsicha',
    'cuca de uva'
  ]
  if ( comLactose) {
    cardapio.push('pastel de queijo');
  }

  cardapio = [...cardapio,
    'pastel de carne',
    'empada de legumes marabijosa'
  ];
    
    
  if (veggie) {
    // TODO: remover alimentos com carne (é obrigatório usar splice!)
    cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1);
    cardapio.splice(cardapio.indexOf('pastel de carne'), 1);
  }
    
  /*let resultadoFinal = []
  for (let i = 0; i < cardapio.length; i++) {
    resultadoFinal.push(cardapio[i].toUpperCase());
  }*/
  
  const resultadoFinal = cardapio.map( alimento => alimento.toUpperCase() );

  return resultadoFinal;
}

console.log(cardapioIFoodM());
   
