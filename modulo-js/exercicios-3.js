/*class Liga{
    constructor(nome ){
        this.nome = nome;
    }
}

class Time{
    constructor(nome, tipoEsporte, status, liga){
        this.nome = nome;
        this.tipoEsporte = tipoEsporte;
        this.status = status;
        this.liga = liga;
        this.jogadores = [];
    }

    
    adicionarJogador(nome, numero){
        this.jogadores.push({nome, numero});
    }

    
    buscarJogadorNome(nome) {
        for (let i = 0; i < this.jogadores.length; i++) {
            if(nome === this.jogadores[i].nome){
                return this.jogadores[i];
            }
        }
    }

    buscarJogadorNumero(numero) {
        for (let i = 0; i < this.jogadores.length; i++) {
            if(numero === this.jogadores[i].numero){
                return this.jogadores[i];
            }
        }
    }

}

let premierLeague = new Liga("Premier League");

let manCity = new Time("Man City", "Futebol", "ativo", premierLeague)

manCity.adicionarJogador("Gabriel Jesus", 33);
manCity.adicionarJogador("Aguero", 10);

console.log(manCity);

console.log(manCity.buscarJogadorNome("Gabruel Jesus"))
console.log(manCity.buscarJogadorNome("Gabriel Jesus")) 
console.log(manCity.buscarJogadorNome("Aguero")) 
console.log(manCity.buscarJogadorNumero(10))
console.log(manCity.buscarJogadorNumero(33)) 
*/

/** ------ Correção Marcos */
class Jogador{
    constructor(nome, numerp){
        this._nome = nome;
        this._numero = numero;
    }

    get nome(){
        return this._nome;
    }

    get numero(){
        return this._numero;
    }
}

class Time{
    constructor(nome, tipoEsporte, liga){
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "Ativo";
        this._liga = liga;
        this._jogadores = [];
    }

    adicionarJogadores( jogador ){
        this._jogadores.push ( jogador );
    }

    buscarPorNome( nome ){
        return this._jogadores.find(jogador => jogador._nome == nome);
    }

    buscarPorNumero( numero ){
        return this._jogadores.find(jogador => jogador._numero == numero);

    }
}


