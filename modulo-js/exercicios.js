/** Exercício 01 */
let circunferencia = {
    raio: 3,
    tipoCalculo: "B"
}
let area = {
    raio: 10,
    tipoCalculo: "A"
}

function calcularCirculo(circulo){
   
    if(circulo.tipoCalculo === "A"){
        return (Math.PI * (Math.pow(circulo.raio, 2)));
    }

    if(circulo.tipoCalculo === "B"){
        return Math.PI * (circulo.raio * 2);
    }
}

//console.log(calcularCirculo(area));


/** Exercício 02 */
function naoBissexto(ano){
    if(ano % 4 === 0){
        return false;
    }
    else{
        return true;
    }
}

//console.log(naoBissexto(2016));
//console.log(naoBissexto(2017));


/** Exercício 03 */
let teste1 = [3, 4, 6, 8, 7, 9, 10];

function somarPares(numeros){
    let soma = 0;
    for(let i = 0; i < numeros.length; i++){
        if(i % 2 === 0){
            soma += numeros[i];
        }
    }
    return soma;
}

//console.log(somarPares(teste1));


/** Exercício 04 */
function adicionar(valorUm){
    return function (valorDois){
        return valorUm + valorDois;
    };
}

//console.log(adicionar(1)(9))


/** Exercício 05 */
function imprimirBRL(valor){

    function adicionarPontos(valorReais){
        valorReais = valorReais.reverse()
        
        let array = [];
        
        /** Inverte os valores do array adicionando a um novo array.
         * Depois itera e adiciona a um novo array os valores do array invertido e quando for divisível por 3, adiciona o ponto.
         * Depois disso, reverte novamente o array para ficar com os valores no lugar certo.
         * Depois, passa os valores do array para uma variável string e retorna.
         */
        for(let i = 0; i < valorReais.length; i++){
            if(i % 3 === 0 && i !== 0){
                array.push(".");
            }
            array.push(valorReais[i]);
        }
        
        let retornoString = array.reverse().join("");
        
        return retornoString;
    }

    let valorArray = valor.toFixed(2).split(".");
    
    let valorReais = valorArray[0].split("");
    let valorCentavos = valorArray[1];
    

    let valor1 = adicionarPontos(valorReais);
    
    let retorno = "R$ " + valor1 + "," + valorCentavos;
    return retorno;

}

//console.log(imprimirBRL(-44675.484)); 


/** ________________________________________________________________________________________________ */


/** ------- Exercício 1 - Correção Marcos -------*/
function calcularCirculoM({ raio, tipoCalculo:tipo }){ //:tipo = troca o nome para raio
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2)
     : 2 * Math.PI * raio );

} 

//console.log(calcularCirculoM(circunferencia));

/** ------- Exercício 2 - Correção Marcos ------- */
function bissextoM( ano ){
    return (ano % 400 == 0) || ( ano % 4 == 0 && ano % 100 != 0 ); 
}

//console.log(bissextoM(2016))
//console.log(bissextoM(2017))

/** ------- Exercício 3 - Correção Marcos ------- */
function somarParesM(numeros){
    let resultado = 0;
    for (let i = 0; i < numeros.length; i+= 2) {
        resultado += numeros[i]; 
    }
    return resultado;
}

//console.log([1, 56, 4.34, 6, -2])
//console.log(somarParesM( [1, 56, 4.34, 6, -2] ))


/** ------- Exercício 4 - Correção Marcos ------- */
/*function adicionarM(valor1){
    return function(valor2){
        return valor1 + valor2;
    }
}*/

/* --- ARROW FUNCTIONS --- 

let adicionarEX = valor1 => { //RETORNA VALOR1
    return valor1;
}
//Mesmas coisas
function adicionarEX(valor1){
    return valor1;
}

let adicionarExemplo = valor1 => { //RETORNA VALOR1 + VALOR2
    valor2 => valor1 + valor2;
}
*/

let adicionarM = valor1 => valor2 => valor1 + valor2;

//console.log(adicionar(3)(4));


/** -------- Exercício Extra --------- */

// CURRYING

// 2 -> 4, 6, 8, 10

/*let divisivel = (divisor, numero) => (numero / divisor);
const divisor = 2;

console.log(divisivel(divisor, 4));
console.log(divisivel(divisor, 6));
console.log(divisivel(divisor, 8));
console.log(divisivel(divisor, 10));*/

const divisao = divisor => numero => (numero / divisor);

const divisivelPor = divisao(2); //passa a primeira parte da função (divisor)

console.log( divisivelPor( 4 )); //depois passa o restante, que é número para ser dividido
console.log( divisivelPor( 6 ));
console.log( divisivelPor( 8 ));
console.log( divisivelPor( 10 ));

