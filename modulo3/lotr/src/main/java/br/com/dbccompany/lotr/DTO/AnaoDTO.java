package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;

import java.util.ArrayList;
import java.util.List;

public class AnaoDTO {

    private Integer id;
    private String nome;
    private Double vida;
    private StatusEnum status = StatusEnum.RECEM_CRIADO;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private Integer qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public AnaoDTO(AnaoEntity anao) {
        this.id = anao.getId();
        this.nome = anao.getNome();
        this.vida = anao.getVida();
        this.status = anao.getStatus();
        this.qtdDano = anao.getQtdDano();
        this.experiencia = anao.getExperiencia();
        this.qtdExperienciaPorAtaque = anao.getQtdExperienciaPorAtaque();
        this.inventario = anao.getInventario();
    }

    public AnaoDTO() {

    }

    public AnaoEntity converter(){
        AnaoEntity anao = new AnaoEntity(this.nome);
        anao.setVida(this.vida);
        anao.setStatus(this.status);
        anao.setQtdDano(this.qtdDano);
        anao.setExperiencia(this.experiencia);
        anao.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        anao.setInventario(this.inventario);
        return anao;
    }

    public List<AnaoDTO> converterLista(List<AnaoEntity> listaAConverter){
        List<AnaoDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                AnaoDTO dto = new AnaoDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public float getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }
}

