package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnaoService{ // extends PersonagemService<AnaoRepository, AnaoEntity>{

    @Autowired
    private AnaoRepository repository;


    public List<AnaoDTO> trazerTodosOsPersonagens() {
        return this.converterLista((List<AnaoEntity>) repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO salvar( AnaoEntity e ) {
        AnaoEntity anaoNovo = this.salvarEEditar(e);
        return new AnaoDTO(anaoNovo);
    }

    @Transactional(rollbackFor = Exception.class)
    public AnaoDTO editar(AnaoEntity e, Integer id){
        e.setId(id);
        AnaoEntity anaoNovo = this.salvarEEditar(e);
        return new AnaoDTO(anaoNovo);
    }

    private AnaoEntity salvarEEditar(AnaoEntity e){
        return repository.save(e);
    }

    public AnaoDTO buscarPorId(Integer id){
        Optional<AnaoEntity> e = repository.findById(id);
        if(e.isPresent()) {
            AnaoEntity anaoBuscado = (AnaoEntity) repository.findById(id).get();
            return new AnaoDTO(anaoBuscado);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllIdIn(List<Integer> id){
        List<AnaoEntity> lista = repository.findAllByIdIn(id);
        List<AnaoDTO> lista2 = this.converterLista(lista);
        if(!lista.isEmpty()){
            return lista2;
        }
        return null;
    }

    public AnaoDTO buscarPorNome(String nome){
        AnaoEntity personagem =  repository.findByNome(nome);
        if(personagem != null){
            AnaoEntity anaoBuscado = repository.findByNome(nome);
            return new AnaoDTO(anaoBuscado);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllNome(String nome){
        List<AnaoEntity> lista = repository.findAllByNome(nome);
        List<AnaoDTO> lista2 = this.converterLista(lista);
        if (lista != null) {
            return lista2;
        }
        return null;
    }

    public AnaoDTO buscarPorVida(Double vida){
        AnaoEntity personagem = repository.findByVida(vida);
        if(personagem != null){
            return new AnaoDTO(personagem);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllVida(Double vida){
        List<AnaoEntity> personagens = repository.findAllByVida(vida);
        List<AnaoDTO> lista = this.converterLista(personagens);
        if(!personagens.isEmpty()){
            return lista;
        }
        return null;
    }

    public List<AnaoDTO> buscarAllVidaIn(List<Double> vida){
        List<AnaoEntity> personagens = repository.findAllByVidaIn(vida);
        List<AnaoDTO> lista = this.converterLista(personagens);
        if(!personagens.isEmpty()){
            return lista;
        }
        return null;
    }

    public AnaoDTO buscaPorStatus(StatusEnum status){
        AnaoEntity personagem = repository.findByStatus(status);
        if(personagem != null){
            AnaoEntity anaoBuscado = repository.findByStatus(status);
            return new AnaoDTO(personagem);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllStatus(StatusEnum status){
        List<AnaoEntity> personagens = repository.findAllByStatus(status);
        List<AnaoDTO> lista = this.converterLista(personagens);
        if(!personagens.isEmpty()){
            return lista;
        }
        return null;
    }

    public List<AnaoDTO> buscarAllStatusIn(List<StatusEnum> status){
        List<AnaoEntity> personagens = repository.findAllByStatusIn(status);
        List<AnaoDTO> lista = this.converterLista(personagens);
         if(!personagens.isEmpty()){
            return lista;
        }
        return null;
    }

    public AnaoDTO buscarPorDano(Double dano){
        AnaoEntity personagem = repository.findByQtdDano(dano);
        if(personagem != null){
            AnaoEntity anaoBuscado = repository.findByQtdDano(dano);
            return new AnaoDTO(anaoBuscado);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllDano(Double dano){
        List<AnaoEntity> personagens = repository.findAllByQtdDano(dano);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllDanoIn(List<Double> dano){
        List<AnaoEntity> personagens = repository.findAllByQtdDanoIn(dano);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public AnaoDTO buscarPorExperiencia(Integer experiencia){
        AnaoEntity personagem = repository.findByExperiencia(experiencia);
        if(personagem != null){
            return new AnaoDTO(personagem);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllExperiencia(Integer experiencia){
        List<AnaoEntity> personagens = repository.findAllByExperiencia(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllExperienciaIn(List<Integer> experiencia){
        List<AnaoEntity> personagens = repository.findAllByExperienciaIn(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public AnaoDTO buscarPorQtdExperienciaAtaque(Integer experiencia){
        AnaoEntity personagem = repository.findByQtdExperienciaPorAtaque(experiencia);
        if(personagem != null){
            return new AnaoDTO(personagem);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllQtdExperienciaAtaque(Integer experiencia){
        List<AnaoEntity> personagens = repository.findAllByQtdExperienciaPorAtaque(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<AnaoDTO> buscarAllQtdExperienciaAtaqueIn(List<Integer> experiencia){
        List<AnaoEntity> personagens = repository.findAllByQtdExperienciaPorAtaqueIn(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public AnaoDTO findByInventario( InventarioEntity inventario ){
        return new AnaoDTO(repository.findByInventario(inventario));
    }

    public List<AnaoDTO> findAllByInventario( InventarioEntity inventario ){
        return this.converterLista(repository.findAllByInventario(inventario));
    }

    public List<AnaoDTO> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return this.converterLista(repository.findAllByInventarioIn(inventarios));
    }

    public List<AnaoDTO> converterLista(List<AnaoEntity> listaAConverter){
        List<AnaoDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                AnaoDTO dto = new AnaoDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }
}
