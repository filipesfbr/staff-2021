package br.com.dbccompany.lotr.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Inventario_X_ItemId implements Serializable {
    private Integer id_inventario;
    private Integer id_item;

    public Inventario_X_ItemId(Integer id_inventario, Integer id_item) {
        this.id_inventario = id_inventario;
        this.id_item = id_item;
    }

    public int getId_inventario() {
        return id_inventario;
    }

    public void setId_inventario(Integer id_inventario) {
        this.id_inventario = id_inventario;
    }

    public int getId_item() {
        return id_item;
    }

    public void setId_item(Integer id_item) {
        this.id_item = id_item;
    }

    public Inventario_X_ItemId() {
    }
}
