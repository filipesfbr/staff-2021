package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.ElfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/elfo")
public class ElfoController {

    @Autowired
    private ElfoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ElfoDTO> trazerTodosOsPersonagens() {
        return service.trazerTodosOsPersonagens();
    }

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ElfoDTO salvar(@RequestBody ElfoDTO e ) {
        return service.salvar(e.converter());
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ElfoDTO editar(@RequestBody ElfoDTO e, @PathVariable Integer id){
        return service.editar(e.converter(), id);
    }

    @GetMapping(value = "/id/{id}")
    @ResponseBody
    public ElfoDTO buscarPorId(@PathVariable Integer id){
        return service.buscarPorId(id);
    }

    @PostMapping(value = "/nome/{nome}")
    @ResponseBody
    public ElfoDTO buscarPorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

    @PostMapping(value = "/nomes/{nome}")
    @ResponseBody
    public List<ElfoDTO> buscarAllNome(@PathVariable String nome){
        return service.buscarAllNome(nome);
    }

    @PostMapping(value = "/vida/{vida}")
    @ResponseBody
    public ElfoDTO buscarPorVida(@PathVariable Double vida){
        return service.buscarPorVida(vida);
    }

    @PostMapping(value = "/vidas/{vida}")
    @ResponseBody
    public List<ElfoDTO> buscarAllVida(@PathVariable Double vida){
        return service.buscarAllVida(vida);
    }

    @PostMapping(value = "/vidasIn/{vida}")
    @ResponseBody
    public List<ElfoDTO> buscarAllVidaIn(@PathVariable List<Double> vida) {
        return service.buscarAllVidaIn(vida);
    }

    @PostMapping(value = "/status/{status}")
    @ResponseBody
    public ElfoDTO buscaPorStatus(@RequestBody StatusEnum status){
        return service.buscaPorStatus(status);
    }

    @PostMapping(value = "/statuss/{status}")
    @ResponseBody
    public List<ElfoDTO> buscarAllStatus(@RequestBody StatusEnum status){
        return service.buscarAllStatus(status);
    }

    @PostMapping(value = "/statussIn/{status}")
    @ResponseBody
    public List<ElfoDTO> buscarAllStatusIn(@RequestBody List<StatusEnum> status){
        return service.buscarAllStatusIn(status);
    }

    @GetMapping(value = "/dano/{dano}")
    @ResponseBody
    public ElfoDTO buscarPorDano(@PathVariable Double dano){
        return service.buscarPorDano(dano);
    }

    @GetMapping(value = "/danos/{dano}")
    @ResponseBody
    public List<ElfoDTO> buscarAllDano(@PathVariable Double dano){
        return service.buscarAllDano(dano);
    }

    @GetMapping(value = "/danosIn/{dano}")
    @ResponseBody
    public List<ElfoDTO> buscarAllDanoIn(@PathVariable List<Double> dano){
        return service.buscarAllDanoIn(dano);
    }

    @GetMapping(value = "/experiencia/{experiencia}")
    @ResponseBody
    public ElfoDTO buscarPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarPorExperiencia(experiencia);
    }

    @GetMapping(value = "/experiencias/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarAllExperiencia(@PathVariable Integer experiencia){
        return service.buscarAllExperiencia(experiencia);
    }

    @GetMapping(value = "/experienciasIn/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarAllExperienciaIn(@PathVariable List<Integer> experiencia){
        return service.buscarAllExperienciaIn(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtk/{experiencia}")
    @ResponseBody
    public ElfoDTO buscarPorQtdExperienciaAtaque(@PathVariable Integer experiencia){
        return service.buscarPorQtdExperienciaAtaque(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtks/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarAllQtdExperienciaAtaque(@PathVariable Integer experiencia){
        return service.buscarAllQtdExperienciaAtaque(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtksIn/{experiencia}")
    @ResponseBody
    public List<ElfoDTO> buscarAllQtdExperienciaAtaqueIn(@PathVariable List<Integer> experiencia){
        return service.buscarAllQtdExperienciaAtaqueIn(experiencia);
    }
}
