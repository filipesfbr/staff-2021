package br.com.dbccompany.lotr.Service;


import br.com.dbccompany.lotr.Entity.InventarioEntity;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService < R extends PersonagemRepository<E>, E extends PersonagemEntity>{
    @Autowired
    private R repository;

    public List<E> trazerTodosOsPersonagens() {
        return (List<E>) repository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public E salvar( E e ) {
        return this.salvarEEditar(e);
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(E e, Integer id){
        e.setId(id);
        return this.salvarEEditar(e);
    }

    private E salvarEEditar(E e){
        return repository.save(e);
    }

    public E buscarPorId(Integer id){
        Optional<E> e = repository.findById(id);
        if(e.isPresent()) {
            return repository.findById(id).get();
        }
        return null;
    }


    public List<E> buscarAllIdIn(List<Integer> id){
        List<E> lista = repository.findAllByIdIn(id);
        if(!lista.isEmpty()){
            return lista;
        }
        return null;
    }

    public E buscarPorNome(String nome){
        E personagem = repository.findByNome(nome);
        if(personagem != null){
            return personagem;
        }
        return null;
    }


    public List<E> buscarAllNome(String nome){
        List<E> lista = repository.findAllByNome(nome);
        if (lista != null) {
            return lista;
        }
        return null;
    }

    public E buscarPorVida(Double vida){
        E personagem = repository.findByVida(vida);
        if(personagem != null){
            return personagem;
        }
        return null;
    }

    public List<E> buscarAllVida(Double vida){
        List<E> personagens = repository.findAllByVida(vida);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public List<E> buscarAllVidaIn(List<Double> vida){
        List<E> personagens = repository.findAllByVidaIn(vida);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }
    public E buscaPorStatus(StatusEnum status){
        E personagem = repository.findByStatus(status);
        if(personagem != null){
            return personagem;
        }
        return null;
    }

    public List<E> buscarAllStatus(StatusEnum status){
        List<E> personagens = repository.findAllByStatus(status);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public List<E> buscarAllStatusIn(List<StatusEnum> status){
        List<E> personagens = repository.findAllByStatusIn(status);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }
    public E buscarPorDano(Double dano){
        E personagem = repository.findByQtdDano(dano);
        if(personagem != null){
            return personagem;
        }
        return null;
    }

    public List<E> buscarAllDano(Double dano){
        List<E> personagens = repository.findAllByQtdDano(dano);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public List<E> buscarAllDanoIn(List<Double> dano){
        List<E> personagens = repository.findAllByQtdDanoIn(dano);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public E buscarPorExperiencia(Integer experiencia){
        E personagem = repository.findByExperiencia(experiencia);
        if(personagem != null){
            return personagem;
        }
        return null;
    }

    public List<E> buscarAllExperiencia(Integer experiencia){
        List<E> personagens = repository.findAllByExperiencia(experiencia);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public List<E> buscarAllExperienciaIn(List<Integer> experiencia){
        List<E> personagens = repository.findAllByExperienciaIn(experiencia);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public E buscarPorQtdExperienciaAtaque(Integer experiencia){
        E personagem = repository.findByQtdExperienciaPorAtaque(experiencia);
        if(personagem != null){
            return personagem;
        }
        return null;
    }

    public List<E> buscarAllQtdExperienciaAtaque(Integer experiencia){
        List<E> personagens = repository.findAllByQtdExperienciaPorAtaque(experiencia);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public List<E> buscarAllQtdExperienciaAtaqueIn(List<Integer> experiencia){
        List<E> personagens = repository.findAllByQtdExperienciaPorAtaqueIn(experiencia);
        if(!personagens.isEmpty()){
            return personagens;
        }
        return null;
    }

    public E findByInventario( InventarioEntity inventario ){
        return repository.findByInventario(inventario);
    }

    public List<E> findAllByInventario( InventarioEntity inventario ){
        return repository.findAllByInventario(inventario);
    }

    public List<E> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return repository.findAllByInventarioIn(inventarios);
    }

}
