package br.com.dbccompany.lotr.Service;


import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventarioService {
    @Autowired
    private InventarioRepository repository;

    public List<InventarioDTO> buscarTodosOsCampos(){
        return this.converterLista(repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO salvar( InventarioEntity inventario ) throws ArgumentosInvalidosInventario {
        try{
            return new InventarioDTO(repository.save(inventario));
        } catch (Exception e){
            throw new ArgumentosInvalidosInventario();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public InventarioDTO editar(InventarioEntity inventario, Integer id) throws ArgumentosInvalidosInventario {
        inventario.setId(id);
        InventarioEntity inventarioNovo = this.salvarEEditar(inventario);
        return new InventarioDTO(inventarioNovo);
    }

    private InventarioEntity salvarEEditar(InventarioEntity inventario) throws ArgumentosInvalidosInventario {
        try{
            return repository.save(inventario);
        } catch (Exception e){
            throw new ArgumentosInvalidosInventario();
        }

    }


    public InventarioDTO buscaPorId(Integer id) throws InventarioNaoEncontrado {
        try{
            return new InventarioDTO(repository.findById(id).get());
        } catch (Exception e){
            throw new InventarioNaoEncontrado();
        }
    }
    public List<InventarioDTO> buscarAllId(Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<InventarioDTO> buscarAllIdIn(List<Integer> ids){
        return this.converterLista(repository.findAllByIdIn(ids));
    }

    public InventarioDTO buscarPorPersonagem(PersonagemEntity personagem){
        InventarioEntity inventario = repository.findByPersonagem(personagem);
        if(inventario != null){
            return new InventarioDTO(inventario);
        }
        return null;
    }
    public List<InventarioDTO> findAllByPersonagem(PersonagemEntity personagem){
        return this.converterLista(repository.findAllByPersonagem(personagem));
    }

    public List<InventarioDTO> findAllByPersonagemIn(List<PersonagemEntity> personagens){
        return this.converterLista(repository.findAllByPersonagemIn(personagens));
    }

    public InventarioDTO findByInventarioItem( Inventario_X_Item inventarioItem ){
        return new InventarioDTO(repository.findByInventarioItem(inventarioItem));
    }

    public List<InventarioDTO> buscarAllInventarioItemIn(List<Inventario_X_Item> inventarioItem) {
        List<InventarioEntity> inventarios = repository.findAllByInventarioItemIn(inventarioItem);
        return this.converterLista(inventarios);
    }

    public List<InventarioDTO> converterLista(List<InventarioEntity> listaAConverter){
        List<InventarioDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            InventarioDTO dto = new InventarioDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
