package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/api/item" )
public class ItemController {
    //www.meusite.com/api/item

    @Autowired
    private ItemService service;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);

    @GetMapping( value = "/")
    @ResponseBody
    public List<ItemDTO> trazerTodosItens(){
        return service.trazerTodosOsItens();
    }

    @GetMapping(value = "/{id}" )
    @ResponseBody
    public ResponseEntity<Object> trazerItemEspecifico(@PathVariable Integer id) {
        try{
            ResponseEntity<Object> item = new ResponseEntity<>(service.buscarPorId(id), HttpStatus.ACCEPTED);
            return item;
        } catch ( ItemNaoEncontrado e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); //postoman - mensagem externa para cliente
        }
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarItem(@RequestBody ItemDTO item) {
        try{
            return new ResponseEntity<>(service.salvar( item.converter() ), HttpStatus.ACCEPTED);
        } catch (ArgumentosInvalidosInventario e){
            System.err.println((e.getMensagem()));
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ItemDTO editarItem(@RequestBody ItemDTO item, @PathVariable Integer id) throws ArgumentosInvalidosItem{
        return service.editar(item.converter(), id);
    }

    @GetMapping (value = "descricao/{descricao}")
    @ResponseBody
    public ItemDTO buscarPorDescricao(@PathVariable String descricao) {
        return service.buscarPorDescricao(descricao);
    }

    @GetMapping (value = "all/descricao/{descricao}")
    @ResponseBody
    public List<ItemDTO> buscarAllDescricao(@PathVariable String descricao) {
        return service.buscarAllDescricao(descricao);
    }

    @PostMapping (value = "all/descricoes")
    @ResponseBody
    public List<ItemDTO> buscarAllDescricaoIn(@RequestBody List<String> descricao) {
        return service.buscarAllDescricaoIn(descricao);
    }

    @PostMapping (value = "inventario/item")
    @ResponseBody
    public ItemDTO buscarPorInventarioItensIn(@RequestBody ItemDTO item) {
        return service.buscarPorInventarioItensIn(item.converter().getInventarioItens());
    }

    @PostMapping (value = "all/inventario")
    @ResponseBody
    public List<ItemDTO> buscarAllInventarioItemIn(@RequestBody ItemDTO item){
        return service.buscarAllInventarioItemIn(item.converter().getInventarioItens());
    }


    }