package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/anao")
public class AnaoController {

    @Autowired
    private AnaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<AnaoDTO> trazerTodosOsPersonagens() {
        return service.trazerTodosOsPersonagens();
    }

    @PostMapping (value = "/salvar")
    @ResponseBody
    public AnaoDTO salvar(@RequestBody AnaoDTO e ) {
        return service.salvar(e.converter());
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public AnaoDTO editar(@RequestBody AnaoDTO e, @PathVariable Integer id){
        return service.editar(e.converter(), id);
    }

    @GetMapping(value = "/id/{id}")
    @ResponseBody
    public AnaoDTO buscarPorId(@PathVariable Integer id){
        return service.buscarPorId(id);
    }

    @PostMapping(value = "/nome/{nome}")
    @ResponseBody
    public AnaoDTO buscarPorNome(@PathVariable String nome){
        return service.buscarPorNome(nome);
    }

    @PostMapping(value = "/nomes/{nome}")
    @ResponseBody
    public List<AnaoDTO> buscarAllNome(@PathVariable String nome){
        return service.buscarAllNome(nome);
    }

    @PostMapping(value = "/vida/{vida}")
    @ResponseBody
    public AnaoDTO buscarPorVida(@PathVariable Double vida){
        return service.buscarPorVida(vida);
    }

    @PostMapping(value = "/vidas/{vida}")
    @ResponseBody
    public List<AnaoDTO> buscarAllVida(@PathVariable Double vida){
        return service.buscarAllVida(vida);
    }

    @PostMapping(value = "/vidasIn/{vida}")
    @ResponseBody
    public List<AnaoDTO> buscarAllVidaIn(@PathVariable List<Double> vida) {
        return service.buscarAllVidaIn(vida);
    }

    @PostMapping(value = "/status/{status}")
    @ResponseBody
    public AnaoDTO buscaPorStatus(@RequestBody StatusEnum status){
        return service.buscaPorStatus(status);
    }

    @PostMapping(value = "/statuss/{status}")
    @ResponseBody
    public List<AnaoDTO> buscarAllStatus(@RequestBody StatusEnum status){
        return service.buscarAllStatus(status);
    }

    @PostMapping(value = "/statussIn/{status}")
    @ResponseBody
    public List<AnaoDTO> buscarAllStatusIn(@RequestBody List<StatusEnum> status){
        return service.buscarAllStatusIn(status);
    }

    @GetMapping(value = "/dano/{dano}")
    @ResponseBody
    public AnaoDTO buscarPorDano(@PathVariable Double dano){
        return service.buscarPorDano(dano);
    }

    @GetMapping(value = "/danos/{dano}")
    @ResponseBody
    public List<AnaoDTO> buscarAllDano(@PathVariable Double dano){
        return service.buscarAllDano(dano);
    }

    @GetMapping(value = "/danosIn/{dano}")
    @ResponseBody
    public List<AnaoDTO> buscarAllDanoIn(@PathVariable List<Double> dano){
        return service.buscarAllDanoIn(dano);
    }

    @GetMapping(value = "/experiencia/{experiencia}")
    @ResponseBody
    public AnaoDTO buscarPorExperiencia(@PathVariable Integer experiencia){
        return service.buscarPorExperiencia(experiencia);
    }

    @GetMapping(value = "/experiencias/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarAllExperiencia(@PathVariable Integer experiencia){
        return service.buscarAllExperiencia(experiencia);
    }

    @GetMapping(value = "/experienciasIn/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarAllExperienciaIn(@PathVariable List<Integer> experiencia){
        return service.buscarAllExperienciaIn(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtk/{experiencia}")
    @ResponseBody
    public AnaoDTO buscarPorQtdExperienciaAtaque(@PathVariable Integer experiencia){
        return service.buscarPorQtdExperienciaAtaque(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtks/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarAllQtdExperienciaAtaque(@PathVariable Integer experiencia){
        return service.buscarAllQtdExperienciaAtaque(experiencia);
    }

    @GetMapping(value = "/qtdExperienciaAtkIn/{experiencia}")
    @ResponseBody
    public List<AnaoDTO> buscarAllQtdExperienciaAtaqueIn(@PathVariable List<Integer> experiencia){
        return service.buscarAllQtdExperienciaAtaqueIn(experiencia);
    }
}
