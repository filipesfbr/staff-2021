package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.*;

import java.util.ArrayList;
import java.util.List;

public class ElfoDTO {

    private Integer id;
    private String nome;
    private Double vida;
    private StatusEnum status;
    private Double qtdDano = 0.0;
    private int experiencia = 0;
    private Integer qtdExperienciaPorAtaque = 1;
    private InventarioEntity inventario;

    public ElfoDTO(ElfoEntity elfo) {
        this.id = elfo.getId();
        this.nome = elfo.getNome();
        this.vida = elfo.getVida();
        this.status = elfo.getStatus();
        this.qtdDano = elfo.getQtdDano();
        this.experiencia = elfo.getExperiencia();
        this.qtdExperienciaPorAtaque = elfo.getQtdExperienciaPorAtaque();
        this.inventario = elfo.getInventario();
    }

    public ElfoEntity converter(){
        ElfoEntity elfo = new ElfoEntity(this.nome);
        elfo.setVida(this.vida);
        elfo.setStatus(this.status);
        elfo.setQtdDano(this.qtdDano);
        elfo.setExperiencia(this.experiencia);
        elfo.setQtdExperienciaPorAtaque(this.qtdExperienciaPorAtaque);
        elfo.setInventario(this.inventario);
        return elfo;
    }
    public ElfoDTO(){

    }

    public List<ElfoDTO> converterLista(List<ElfoEntity> listaAConverter){
        List<ElfoDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                ElfoDTO dto = new ElfoDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public float getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(Integer qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public InventarioEntity getInventario() {
        return inventario;
    }

    public void setInventario(InventarioEntity inventario) {
        this.inventario = inventario;
    }

}

