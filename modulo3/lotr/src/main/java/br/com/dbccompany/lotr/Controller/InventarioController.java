package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.InventarioNaoEncontrado;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/inventario")
public class InventarioController {
    //www.meusite.com/api/item

    @Autowired
    private InventarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioDTO> buscarTodosOsCampos(){
        return service.buscarTodosOsCampos();
    }

    @PostMapping(value = "/salvar")
    @ResponseBody
    public InventarioDTO salvarItem(@RequestBody InventarioDTO inventario) throws ArgumentosInvalidosInventario {
        try {
            return service.salvar(inventario.converter());
        }catch (Exception e){
            throw new ArgumentosInvalidosInventario();
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public InventarioDTO editarItem(@RequestBody InventarioDTO inventario, @PathVariable Integer id) throws ArgumentosInvalidosInventario {
        return service.editar(inventario.converter(), id);
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public InventarioDTO buscaPorId(@PathVariable Integer id) {
        try{
            return service.buscaPorId(id);
        } catch (InventarioNaoEncontrado e){
            System.err.println(e.getMensagem());
            return null;
        }
    }

    @GetMapping(value = "/ids/{id}")
    @ResponseBody
    public List<InventarioDTO> buscarAllId(@PathVariable Integer id){
        return service.buscarAllId(id);
    }

    @PostMapping (value = "/idsIn/{id}")
    @ResponseBody
    public List<InventarioDTO> buscarAllIdIn(@RequestBody List<Integer> ids){
        return service.buscarAllIdIn(ids);
    }

    @PostMapping (value = "/personagem")
    @ResponseBody
    public InventarioDTO buscarPorPersonagem(@RequestBody InventarioDTO inventario){
        return service.buscarPorPersonagem(inventario.converter().getPersonagem());
    }

    @PostMapping(value = "/personagens")
    @ResponseBody
    public List<InventarioDTO> findAllByPersonagem(@RequestBody InventarioDTO inventario){
        return service.findAllByPersonagem(inventario.converter().getPersonagem());
    }

    @PostMapping (value = "inventario")
    @ResponseBody
    public InventarioDTO findByInventarioItem(@RequestBody Inventario_X_Item inventario){
        return service.findByInventarioItem(inventario);
    }

    @PostMapping(value = "/inventariosIn")
    @ResponseBody
    public List<InventarioDTO> buscarAllInventarioItemIn(@RequestBody List<Inventario_X_Item> inventario){
        return service.buscarAllInventarioItemIn(inventario);
    }
}
