package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Exception.ArgumentoInvalidoInventarioItem;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
    @RequestMapping(value = "/api/inventario_x_item")
public class Inventario_X_ItemController {

    @Autowired
    private Inventario_X_ItemService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public Inventario_X_ItemDTO salvar(@RequestBody Inventario_X_ItemDTO inventario ) throws ArgumentoInvalidoInventarioItem{
        try {
            return service.salvar(inventario.converter());
        } catch (Exception e) {
            throw new ArgumentoInvalidoInventarioItem();
        }
    }

    @PutMapping(value = "/editar/{id} ")
    @ResponseBody
    public Inventario_X_ItemDTO editar(@RequestBody Inventario_X_ItemDTO inventario, @RequestBody Inventario_X_ItemId id) throws ArgumentosInvalidosItem {
        return service.editar(inventario.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAll(){
        return service.findAll();
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public Inventario_X_ItemDTO findById(@RequestBody Inventario_X_ItemDTO id ) throws InventarioItemNaoEncontrado {
        try{
            return service.findById(id.converter().getId());
        } catch (Exception e){
            throw new InventarioItemNaoEncontrado();
        }
    }

    @PostMapping (value = "/id/all/{id} ")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllById(@RequestBody Inventario_X_ItemDTO id ){
        return service.findAllById(id.converter().getId());
    }


    @PostMapping ( value = "inventario/{inventario}  ")
    @ResponseBody
    public Inventario_X_ItemDTO findByInventario( @RequestBody Inventario_X_ItemDTO inventario){
        return service.findByInventario(inventario.converter().getInventario());
    }

    @PostMapping(value = "inventario/all/{inventario} ")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByInventario( @RequestBody Inventario_X_ItemDTO inventario){
        return service.findAllByInventario(inventario.converter().getInventario());
    }

    @PostMapping(value = "item/{item}")
    @ResponseBody
    public Inventario_X_ItemDTO findByItem(@RequestBody Inventario_X_ItemDTO item ){
        return service.findByItem(item.converter().getItem());
    }

    @PostMapping(value = "item/all/{item}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByItem( @RequestBody Inventario_X_ItemDTO item ){
        return service.findAllByItem(item.converter().getItem());
    }

    @GetMapping(value = "quantidade/{quantidade}")
    @ResponseBody
    public Inventario_X_ItemDTO findByQuantidade( @PathVariable Integer quantidade ){
        return service.findByQuantidade(quantidade);
    }

    @GetMapping(value = "quantidade/all/{quantidade}")
    @ResponseBody
    public List<Inventario_X_ItemDTO> findAllByQuantidade(@PathVariable Integer quantidade ){
        return service.findAllByQuantidade(quantidade);
    }

}

