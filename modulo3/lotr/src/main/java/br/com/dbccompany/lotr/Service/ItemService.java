package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosInventario;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.LotrApplication;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    private Logger logger = LoggerFactory.getLogger(LotrApplication.class);
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:8081/salvar/logs";

    public List<ItemDTO> trazerTodosOsItens() {
        return this.converterLista(repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO salvar(ItemEntity item) throws ArgumentosInvalidosInventario {
        try{
            ItemEntity itemNovo = repository.save(item);
            return new ItemDTO(itemNovo);
        } catch (Exception e){
            //Log.envioErrro(e)
            throw new ArgumentosInvalidosInventario();
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO editar(ItemEntity item, Integer id) throws ArgumentosInvalidosItem {
        item.setId(id);
        ItemEntity itemNovo = this.salvarEEditar(item);
        return new ItemDTO(itemNovo);
    }

    private ItemEntity salvarEEditar(ItemEntity item) throws ArgumentosInvalidosItem {
        try{
            return repository.save(item);
        } catch (Exception e){
            throw new ArgumentosInvalidosItem();
        }

    }


    public ItemDTO buscarPorId(Integer id) throws ItemNaoEncontrado {
        try {
            return new ItemDTO(repository.findById(id).get());
        } catch (Exception e){
            logger.error("Item teve problema ao buscar específico: " + e.getMessage());
            restTemplate.postForObject(url, e.getMessage(), Object.class);
            throw new ItemNaoEncontrado();
        }
    }


    public List<ItemDTO> buscarAllId(Integer id){
        return this.converterLista(repository.findAllById(id));
    }

    public List<ItemDTO> buscarAllIdIn(List<Integer> id){
        List<ItemEntity> itens = repository.findAllByIdIn(id);
        List<ItemDTO> lista = this.converterLista(itens);
        if(!lista.isEmpty()){
            return lista;
        }
        return null;
    }
    public ItemDTO buscarPorDescricao(String descricao){
        ItemEntity itemBuscado = repository.findByDescricao(descricao);
        return new ItemDTO(itemBuscado);
    }

    public List<ItemDTO> buscarAllDescricao(String descricao){
        List<ItemEntity> lista = repository.findAllByDescricao(descricao);
        List<ItemDTO> lista2 = this.converterLista(lista);
        return lista2;
    }

    public List<ItemDTO> buscarAllDescricaoIn(List<String> descricao) {
        List<ItemEntity> itens = repository.findAllByDescricaoIn(descricao);
        List<ItemDTO> lista = this.converterLista(itens);
        if(!lista.isEmpty()){
            return lista;
        }
        return null;
    }

    public ItemDTO buscarPorInventarioItensIn(List<Inventario_X_Item> inventario){
        ItemEntity itemBuscado = (ItemEntity) repository.findAllByInventarioItensIn(inventario);
        return new ItemDTO(itemBuscado);
    }


    public List<ItemDTO> buscarAllInventarioItemIn(List<Inventario_X_Item> inventario){
        List<ItemEntity> lista = repository.findAllByInventarioItensIn(inventario);
        List<ItemDTO> lista2 = this.converterLista(lista);
        return lista2;

    }

    public List<ItemDTO> converterLista(List<ItemEntity> listaAConverter){
        List<ItemDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                ItemDTO dto = new ItemDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }

}
