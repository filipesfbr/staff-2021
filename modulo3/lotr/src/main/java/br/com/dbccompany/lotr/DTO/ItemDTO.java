package br.com.dbccompany.lotr.DTO;


import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;

import java.util.ArrayList;
import java.util.List;

public class ItemDTO {
    private Integer id;
    private String descricao;
    private Inventario_X_Item inventario;


    public ItemDTO( ItemEntity item ) {
        this.id = item.getId();
        this.descricao = item.getDescricao();
        this.inventario = null;
    }

    public ItemDTO(){

    }
    public ItemEntity converter(){
        ItemEntity item = new ItemEntity();
        item.setDescricao(this.descricao);
        item.setId(this.id);
        return item;
    }

    public List<ItemDTO> converterLista(List<ItemEntity> listaAConverter){
        List<ItemDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                ItemDTO dto = new ItemDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }


}