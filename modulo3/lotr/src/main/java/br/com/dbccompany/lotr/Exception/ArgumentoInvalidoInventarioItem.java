package br.com.dbccompany.lotr.Exception;

public class ArgumentoInvalidoInventarioItem extends Inventario_X_ItemException{
    public ArgumentoInvalidoInventarioItem() {
        super("Faltou campos!");
    }
}
