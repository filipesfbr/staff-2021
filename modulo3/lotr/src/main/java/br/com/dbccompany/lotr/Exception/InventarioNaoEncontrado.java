package br.com.dbccompany.lotr.Exception;

public class InventarioNaoEncontrado extends InventarioException{
    public InventarioNaoEncontrado(){
        super("Inventario não encontrado");
    }
}
