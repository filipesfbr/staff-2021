package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import java.util.List;
import java.util.Optional;
@Primary
@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    Optional<T> findById(Integer id);
    List<T> findAllByIdIn(List<Integer> id);

    T findByNome(String nome);
    List<T> findAllByNome(String nome);

    T findByVida(Double vida);
    List<T> findAllByVida(Double vida);
    List<T> findAllByVidaIn (List<Double> vida);

    T findByStatus(StatusEnum status);
    List<T> findAllByStatus(StatusEnum status);
    List<T> findAllByStatusIn (List<StatusEnum> status);

    T findByQtdDano (Double qtdDano);
    List<T> findAllByQtdDano(Double dano);
    List<T> findAllByQtdDanoIn(List<Double> qtdDano);

    T findByExperiencia (Integer experiencia);
    List<T> findAllByExperiencia(Integer experiencia);
    List<T> findAllByExperienciaIn(List<Integer> experiencia);

    T findByQtdExperienciaPorAtaque(Integer experiencia);
    List<T> findAllByQtdExperienciaPorAtaque (Integer experiencia);
    List<T> findAllByQtdExperienciaPorAtaqueIn(List<Integer> experiencia);

    T findByInventario( InventarioEntity inventario );
    List<T> findAllByInventario( InventarioEntity inventario );
    List<T> findAllByInventarioIn( List<InventarioEntity> inventarios );

}
