package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    Optional<ItemEntity> findById(Integer id);
    List<ItemEntity> findAllById(Integer id);
    List<ItemEntity> findAllByIdIn(List<Integer> ids);

    ItemEntity findByDescricao( String descricao );
    List<ItemEntity> findAllByDescricao(String descricao);
    List<ItemEntity> findAllByDescricaoIn(List<String> descricao);

    ItemEntity findByInventarioItens(InventarioEntity inventario);
    List<ItemEntity> findAllByInventarioItensIn(List<Inventario_X_Item> inventarioItem);

    List<ItemEntity> findAll ();
}
