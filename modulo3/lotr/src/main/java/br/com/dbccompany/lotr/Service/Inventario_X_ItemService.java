package br.com.dbccompany.lotr.Service;


import br.com.dbccompany.lotr.DTO.Inventario_X_ItemDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.Inventario_X_Item;
import br.com.dbccompany.lotr.Entity.Inventario_X_ItemId;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ArgumentosInvalidosItem;
import br.com.dbccompany.lotr.Exception.InventarioItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository respository;

    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private InventarioRepository invRepository;

    @Transactional(rollbackFor = Exception.class)
    public Inventario_X_ItemDTO salvar(Inventario_X_Item inventario ) throws ArgumentosInvalidosItem {
        return new Inventario_X_ItemDTO(this.salvarEEditar(inventario));
    }

    @Transactional(rollbackFor = Exception.class)
    public Inventario_X_ItemDTO editar(Inventario_X_Item inventario, Inventario_X_ItemId id) throws ArgumentosInvalidosItem {
        inventario.setId(id);
        return new Inventario_X_ItemDTO(this.salvarEEditar(inventario));

    }

    private Inventario_X_Item salvarEEditar( Inventario_X_Item inventario_x_item ) throws ArgumentosInvalidosItem {
        ItemEntity item = itemRepository.findById(inventario_x_item.getId().getId_item()).get();
        InventarioEntity inventario = invRepository.findById(inventario_x_item.getId().getId_inventario()).get();
        inventario_x_item.setInventario(inventario);
        inventario_x_item.setItem(item);
        try{
            return respository.save( inventario_x_item );
        } catch (Exception e){
            throw new ArgumentosInvalidosItem();
        }

    }


    public List<Inventario_X_ItemDTO> findAll(){
        return this.converterLista(respository.findAll());
    }

    public Inventario_X_ItemDTO findById(Inventario_X_ItemId id ) throws InventarioItemNaoEncontrado {
        try{
            return new Inventario_X_ItemDTO(respository.findById(id).get());
        } catch (Exception e){
            throw new InventarioItemNaoEncontrado();
        }

    }

    public List<Inventario_X_ItemDTO> findAllById(Inventario_X_ItemId id ){
        return this.converterLista(respository.findAllById(id));
    }

    public List<Inventario_X_ItemDTO> findAllByIdIn( List<Inventario_X_ItemId> ids ){
        return this.converterLista(respository.findAllByIdIn(ids));
    }

    public Inventario_X_ItemDTO findByInventario( InventarioEntity inventario){
        return new Inventario_X_ItemDTO(respository.findByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> findAllByInventario( InventarioEntity inventario){
        return this.converterLista(respository.findAllByInventario(inventario));
    }

    public List<Inventario_X_ItemDTO> findAllByInventarioIn( List<InventarioEntity> inventarios) {
        return this.converterLista(respository.findAllByInventarioIn(inventarios));
    }

    public Inventario_X_ItemDTO findByItem( ItemEntity item ){
        return new Inventario_X_ItemDTO(respository.findByItem(item));
    }

    public List<Inventario_X_ItemDTO> findAllByItem( ItemEntity item ){
        return this.converterLista(respository.findAllByItem(item));
    }

    public List<Inventario_X_ItemDTO> findAllByItemIn( List<ItemEntity> itens ){
        return this.converterLista(respository.findAllByItemIn(itens));
    }

    public Inventario_X_ItemDTO findByQuantidade( Integer quantidade ){
        return new Inventario_X_ItemDTO(respository.findByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> findAllByQuantidade( Integer quantidade ){
        return this.converterLista(respository.findAllByQuantidade(quantidade));
    }

    public List<Inventario_X_ItemDTO> findAllByQuantidadeIn( List<Integer> quantidades ){
        return this.converterLista(respository.findAllByQuantidadeIn(quantidades));
    }

    public List<Inventario_X_ItemDTO> converterLista(List<Inventario_X_Item> listaAConverter){
        List<Inventario_X_ItemDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            Inventario_X_ItemDTO dto = new Inventario_X_ItemDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }

/*
    public List<Inventario_X_Item> findAll(){
        return respository.findAll();
    }
    public Optional<Inventario_X_Item> findById(Inventario_X_ItemId id ){
        return respository.findById(id);
    }

    public List<Inventario_X_Item> findAllById(Inventario_X_ItemId id ){
        return respository.findAllById(id);
    }

    public List<Inventario_X_Item> findAllByIdIn( List<Inventario_X_ItemId> ids ){
        return respository.findAllByIdIn(ids);
    }

    public Inventario_X_Item findByInventario( InventarioEntity inventario){
        return respository.findByInventario(inventario);
    }

    public List<Inventario_X_Item> findAllByInventario( InventarioEntity inventario){
        return respository.findAllByInventario(inventario);
    }

    public List<Inventario_X_Item> findAllByInventarioIn( List<InventarioEntity> inventarios) {
        return respository.findAllByInventarioIn(inventarios);
    }

    public Inventario_X_Item findByItem( ItemEntity item ){
        return respository.findByItem(item);
    }

    public List<Inventario_X_Item> findAllByItem( ItemEntity item ){
        return respository.findAllByItem(item);
    }

    public List<Inventario_X_Item> findAllByItemIn( List<ItemEntity> itens ){
        return respository.findAllByItemIn(itens);
    }

    public Inventario_X_Item findByQuantidade( Integer quantidade ){
        return respository.findByQuantidade(quantidade);
    }

    public List<Inventario_X_Item> findAllByQuantidade( Integer quantidade ){
        return respository.findAllByQuantidade(quantidade);
    }

    public List<Inventario_X_Item> findAllByQuantidadeIn( List<Integer> quantidades ){
        return respository.findAllByQuantidadeIn(quantidades);
    }
    */


}
