package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Entity.StatusEnum;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ElfoService {

    @Autowired
    private ElfoRepository repository;

    public List<ElfoDTO> trazerTodosOsPersonagens() {
        return this.converterLista((List<ElfoEntity>) repository.findAll());
    }

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO salvar( ElfoEntity e ) {
        ElfoEntity elfoNovo = this.salvarEEditar(e);
        return new ElfoDTO(elfoNovo);
    }

    @Transactional(rollbackFor = Exception.class)
    public ElfoDTO editar(ElfoEntity e, Integer id){
        e.setId(id);
        ElfoEntity elfoNovo = this.salvarEEditar(e);
        return new ElfoDTO(elfoNovo);
    }

    private ElfoEntity salvarEEditar(ElfoEntity e){
        return repository.save(e);
    }

    public ElfoDTO buscarPorId(Integer id){
        Optional<ElfoEntity> e = repository.findById(id);
        if(e.isPresent()) {
            ElfoEntity elfoBuscado = repository.findById(id).get();
            return new ElfoDTO(elfoBuscado);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllIdIn(List<Integer> id){
        List<ElfoEntity> lista = repository.findAllByIdIn(id);
        if(!lista.isEmpty()){
            return this.converterLista(lista);
        }
        return null;
    }

    public ElfoDTO buscarPorNome(String nome){
        ElfoEntity personagem = repository.findByNome(nome);
        if(personagem != null){
            ElfoEntity elfoNovo = repository.findByNome(nome);
            return new ElfoDTO(elfoNovo);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllNome(String nome){
        List<ElfoEntity> lista = repository.findAllByNome(nome);
        if (lista != null) {
            return this.converterLista(lista);
        }
        return null;
    }

    public ElfoDTO buscarPorVida(Double vida){
        ElfoEntity personagem = repository.findByVida(vida);
        if(personagem != null){
            ElfoEntity buscado = repository.findByVida(vida);
            return new ElfoDTO(buscado);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllVida(Double vida){
        List<ElfoEntity> personagens = repository.findAllByVida(vida);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllVidaIn(List<Double> vida){
        List<ElfoEntity> personagens = repository.findAllByVidaIn(vida);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }
    public ElfoDTO buscaPorStatus(StatusEnum status){
        ElfoEntity personagem = repository.findByStatus(status);
        if(personagem != null){
            ElfoEntity aux = repository.findByStatus(status);
            return new ElfoDTO(aux);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllStatus(StatusEnum status){
        List<ElfoEntity> personagens = repository.findAllByStatus(status);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllStatusIn(List<StatusEnum> status){
        List<ElfoEntity> personagens = repository.findAllByStatusIn(status);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }
    public ElfoDTO buscarPorDano(Double dano){
        ElfoEntity personagem = repository.findByQtdDano(dano);
        if(personagem != null){
            ElfoEntity aux = repository.findByQtdDano(dano);
            return new ElfoDTO(aux);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllDano(Double dano){
        List<ElfoEntity> personagens = repository.findAllByQtdDano(dano);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllDanoIn(List<Double> dano){
        List<ElfoEntity> personagens = repository.findAllByQtdDanoIn(dano);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public ElfoDTO buscarPorExperiencia(Integer experiencia){
        ElfoEntity personagem = repository.findByExperiencia(experiencia);
        if(personagem != null){
            ElfoEntity aux = repository.findByExperiencia(experiencia);
            return new ElfoDTO(personagem);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllExperiencia(Integer experiencia){
        List<ElfoEntity> personagens = repository.findAllByExperiencia(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllExperienciaIn(List<Integer> experiencia){
        List<ElfoEntity> personagens = repository.findAllByExperienciaIn(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public ElfoDTO buscarPorQtdExperienciaAtaque(Integer experiencia){
        ElfoEntity personagem = repository.findByQtdExperienciaPorAtaque(experiencia);
        if(personagem != null){
            ElfoEntity aux = repository.findByQtdExperienciaPorAtaque(experiencia);
            return new ElfoDTO(aux);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllQtdExperienciaAtaque(Integer experiencia){
        List<ElfoEntity> personagens = repository.findAllByQtdExperienciaPorAtaque(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public List<ElfoDTO> buscarAllQtdExperienciaAtaqueIn(List<Integer> experiencia){
        List<ElfoEntity> personagens = repository.findAllByQtdExperienciaPorAtaqueIn(experiencia);
        if(!personagens.isEmpty()){
            return this.converterLista(personagens);
        }
        return null;
    }

    public ElfoDTO findByInventario( InventarioEntity inventario ){
        return new ElfoDTO(repository.findByInventario(inventario));
    }

    public List<ElfoDTO> findAllByInventario( InventarioEntity inventario ){
        return this.converterLista(repository.findAllByInventario(inventario));
    }

    public List<ElfoDTO> findAllByInventarioIn( List<InventarioEntity> inventarios ){
        return this.converterLista(repository.findAllByInventarioIn(inventarios));
    }

    public List<ElfoDTO> converterLista(List<ElfoEntity> listaAConverter){
        List<ElfoDTO> listaConvertida = new ArrayList<>();
        if(listaAConverter != null) {
            for (int i = 0; i < listaAConverter.size(); i++) {
                ElfoDTO dto = new ElfoDTO(listaAConverter.get(i));
                listaConvertida.add(dto);
            }
        }
        return listaConvertida;
    }

}
