/*db.banco.insert(
{
  "codigo": "011",
  "nome": "Alfa",
  "agencia": [
    {
      "codigo": "0001",
      "nome": "Web",
      "endereco": {
        "logradouro": "Rua Testando",
        "numero": 55,
        "complemento": "Loja 1",
        "cep": "00000-00",
        "bairro": "NA",
        "cidade": "NA",
        "estado": "NA",
        "pais": "Brasil"
      },
      "consolidacao": [
        {
          "saldoAtual": 0,
          "saque": 0,
          "deposito": 0,
          "numeroCorrentistas": 0
        }
      ],
      "conta": [
        {
          "codigo": "0",
          "tipoConta": "PF",
          "saldo": 0.0,
          "gerente": [
            {
              "nome": "",
              "cpf": "000.000.000-00",
              "estadoCivil": "Em relacionamento com minha cama",
              "dataNascimento": "00/00/0000",
              "codigoFuncionario": "0000011",
              "tipoGerente": "GC",
              "endereco": {
                "logradouro": "Rua Testando",
                "numero": 55,
                "complemento": "Loja 1",
                "cep": "00000-00",
                "bairro": "NA",
                "cidade": "NA",
                "estado": "NA",
                "pais": "Brasil"
              }
            }
          ],
          "cliente": [
            {
              "nome": "",
              "cpf": "000.000.000-00",
              "estadoCivil": "Em relacionamento com minha cama",
              "dataNascimento": "00/00/0000",
              "endereco": {
                "logradouro": "Rua Testando",
                "numero": 55,
                "complemento": "Loja 1",
                "cep": "00000-00",
                "bairro": "NA",
                "cidade": "NA",
                "estado": "NA",
                "pais": "Brasil"
              }
            }
          ],
          "movimentacoes": [
            {
              "tipo": "crebito",
              "valor": 1200.00
            },
            {
              "tipo": "a combinar",
              "valor": 1000.00
            },
            {
              "tipo": "dedito",
              "valor": 100.00
            }
          ]
        }
      ]
    }
  ]
}
)*/


//db.banco.find({ nome: { $regex: /A/ }});
//db.banco.find({ codigo: { $eq: "011" }}); =
//db.banco.find({ codigo: { $ne: "011" }}); <>
//db.banco.find({ "agencia.conta.movimentacoes.valor": { $gt: 10 }}); >
//db.banco.find({ "agencia.conta.movimentacoes.valor": { $lt: 10 } }); <
//db.banco.find({ "agencia.conta.movimentacoes.valor": { $lte: 10 } }); <=
//db.banco.find({ "agencia.conta.movimentacoes.valor": { $in: { 10, 100 } } }); in 
//db.banco.find().sort({"agencia.conta.movimentacoes.valor": -1});


