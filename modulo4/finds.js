//db.banco.find();

//db.banco.update( {nome: "Alfa"}, { $set: {nome: "Alfa 2"}} ); //usar o set para alterar só o nome, não excluir o restante
db.banco.update( {_id: ObjectId("60ba93fd564225674186e6f2") }, { $set: {nome: "Alfa 2"}} ); //usar o set para alterar só o nome, não excluir o restante

/*
    $unset = deletar um campo
    $inc = incrementar um valor específico
    $mul
    $rename = nome vai mudar a descrição. o set muda o valor de nome, o rename o nome
*/

db.banco.deleteOne( { nome: "Alfa" } ); //deletar o primeiro Alfa como nome
db.banco.deleteOne( {_id: ObjectId("60ba93fd564225674186e6f2")} ) //deleta de fato que quer, id específico

db.banco.deleteMany() //deleta todas