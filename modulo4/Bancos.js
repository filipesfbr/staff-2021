alfa = {
	codigo: "011",
	nome: "Alfa",
	agencia: [
		{
			codigo: "0001",
			nome: "Web",
			endereco: [
			{
				logradouro: "Rua Testando",
				numero: "55",
				complemento: "loja 1",
				bairro: "NA",
				cidade: "NA",
				estado: "NA",
				pais: "Brasil"
			}
			]
		},

		{
			codigo: "0002",
			nome: "California",
			endereco: [
			{
				logradouro: "Rua Testing",
				numero: "122",
				complemento: "",
				bairro: "Between Hyde and Powell Streets",
				cidade: "San Francisco",
				estado: "California",
				pais: "EUA"
			}
			]

		},

		{
			codigo: "0101",
			nome: "Londres",
			endereco: [
			{
				logradouro: "Rua Tesing",
				numero: "525",
				complemento: "",
				bairro: "Croydon",
				cidade: "Londres",
				estado: "Boroughs",
				pais: "England"
	        }
		    ]

		}
    ]
}

beta = {
    codigo: "241",
	nome: "Beta",
	agencia: [
		{
			codigo: "0001",
			nome: "Web",
			endereco: [
			{
				logradouro: "Rua Testando",
				numero: "55",
				complemento: "loja 2",
				bairro: "NA",
				cidade: "NA",
				estado: "NA",
				pais: "Brasil"
			}
			]
		}
	]
}

omega = {
    codigo: "307",
	nome: "Omega",
	agencia: [
		{
			codigo: "0001",
			nome: "Web",
			endereco: [
			{
				logradouro: "Rua Testando",
				numero: "55",
				complemento: "loja 3",
				bairro: "NA",
				cidade: "NA",
				estado: "NA",
				pais: "Brasil"
			}
			]
		},
		
		{
		    codigo: "8761",
			nome: "Itu",
			endereco: [
			{
				logradouro: "Rua do Meio",
				numero: "2233",
				complemento: "",
				bairro: "Qualquer",
				cidade: "Itu",
				estado: "São Paulo",
				pais: "Brasil"
			}
			]
		},
		
		{
		    codigo: "4567",
			nome: "Hermana",
			endereco: [
			{
				logradouro: "Rua do boca",
				numero: "222",
				complemento: "",
				bairro: "Caminito",
				cidade: "Buenos Aires",
				estado: "Buenos Aires",
				pais: "Argentina"
			}
			]
		}
	]   
}

db.banco.insert(alfa);
db.banco.insert(beta);
db.banco.insert(omega);



