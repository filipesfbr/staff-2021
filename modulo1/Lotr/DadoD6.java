import java.util.*;

public class DadoD6 implements Sorteador {
    
    public int sortear2(){
        return (int)Math.floor(Math.random() * 6+1);
    }
    
    public int sortear(){
        Random random = new Random();
        return  random.nextInt( 6 ) + 1;
    }
}