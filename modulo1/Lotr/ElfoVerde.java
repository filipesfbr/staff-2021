import java.util.*;
public class ElfoVerde extends Elfo{
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de vidro",
            "Flecha de vidro"
        )
    );
    
    public ElfoVerde(String nome){
        super(nome);
        super.qtdExperienciaPorAtaque = 2;
    }
    

    private boolean isDescricaoValida(String descricao){
        return this.DESCRICOES_VALIDAS.contains(descricao);
    }
    
    public void perderItem(Item item){
        if(isDescricaoValida(item.getDescricao())){
            this.inventario.adicionar(item);
        }
    }
    
    public void ganharItem(Item item){
        if(isDescricaoValida(item.getDescricao())){
            this.inventario.remover(item);
        }
    }
}
