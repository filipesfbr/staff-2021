public class Anao extends Personagem{
    private boolean escudoEquipado;
    
   public Anao(String nome){
        super(nome);
        this.vida = 110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
        this.escudoEquipado = false;
        super.qtdDano = 10.0;
    }
    
    /*public void equiparEscudo(){
            this.escudoEquipado = true;
    }*/
        
   public void equiparEDesequiparEscudo(){
        this.escudoEquipado = !this.escudoEquipado;
        super.qtdDano = this.escudoEquipado ? 5.0 : 10.0;
   }
           
   public String imprimirNomeClasse(){
       return "Anao";
    }
    
   
}
