import java.util.*;
import java.lang.String;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest{

    @Test
    public void testarOrdenacao(){
        Inventario inventario = new Inventario();
        Item termica = new Item(9, "Termica de Cafe");
        Item energetico = new Item(1, "Energetico");
        Item alicate = new Item(3, "Alicate");
        Item arpao = new Item(4, "Arpao");
        
        inventario.adicionar(termica);
        inventario.adicionar(energetico);
        inventario.adicionar(alicate);
        inventario.adicionar(arpao);
        
        assertEquals(9, inventario.getItens().get(0).getQuantidade());
        inventario.ordenarItens();
        assertEquals(1, inventario.getItens().get(0).getQuantidade());
        
        inventario.ordenarItens(TipoOrdenacao.DESC);
        assertEquals(9, inventario.getItens().get(0).getQuantidade());
        
    }
    
    @Test
    public void buscarApenasUmItem(){
        Inventario inventario = new Inventario();
        Item cafe = new Item(1, "Cafe");
        inventario.adicionar(cafe);
        Item resultado = inventario.buscar(new String( "Cafe" ));
        
        assertEquals( cafe, resultado );
    }
    
    @Test
    public void inverterDoisItens(){
        Inventario inventario = new Inventario();
        Item termica = new Item(1, "Termica de Cafe");
        Item energetico = new Item(1, "Energetico");
        
        inventario.adicionar(termica);
        inventario.adicionar(energetico);
        
        ArrayList<Item> resultado = inventario.inverter();
        assertEquals(energetico, resultado.get(0));
        assertEquals(termica, resultado.get(1));
        
        assertEquals(termica, inventario.obter(0));
        assertEquals(energetico, inventario.obter(1));
        
        assertEquals(2, resultado.size());
        
    }
    
    @Test
    public void buscarPorPalavra(){
        Inventario inventario = new Inventario();
        Item item = new Item(4, "Flecha");
        Item item2 = new Item(12, "Alicate");
        Item item3 = new Item(4, "Arpao");
        
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        
        assertEquals(item, inventario.buscar("Flecha"));
        assertEquals(item3, inventario.buscar("Arpao"));
        
    }
    
    @Test
    public void testarInverterLista(){
        Inventario inventario = new Inventario();
        Item item = new Item(4, "Flecha");
        Item item2 = new Item(12, "Alicate");
        Item item3 = new Item(4, "Arpao");
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        
        ArrayList<Item> lista = inventario.inverter();
        
        
        assertEquals("Flecha", inventario.getItem(0).getDescricao());
        assertEquals("Arpao", lista.get(0).getDescricao());
        
    }
    
    @Test
    public void criarInventarioSemPassarValor(){
        Inventario inventario = new Inventario();
        assertEquals(true, inventario.getItens().isEmpty());

    }

    /*
    @Test
    public void criarInventarioPassandoValor(){
        Inventario inventario = new Inventario(40);
        assertEquals(40, inventario.getItens().length);
    }*/

    @Test
    public void adicionarUmItemInvetario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        inventario.adicionar(item);
        
        assertEquals(true, inventario.getItens().contains(item));
    }

    @Test
    public void adicionarDoisItensInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        Item item2 = new Item(3, "Flecha de Luz");
        inventario.adicionar(item);
        inventario.adicionar(item2);
        
        assertEquals(item, inventario.getItens().get(0));
        assertEquals(item2, inventario.getItens().get(1));
    }


    @Test
    public void obterUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        
        inventario.adicionar(item);
        assertEquals(item, inventario.obter(0));
    }
    
    @Test
    public void removerUmItemInventario(){
        Inventario inventario = new Inventario();
        Item item = new Item(1, "Espada de aço");
        inventario.adicionar(item);
        inventario.remover(0);
        
        //Verificar se eh nulo, usa o assertNull
        assertEquals(false, inventario.getItens().contains(item));
    }
    
    @Test
    public void getDescricoesVariosItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada" );
        Item escudo = new Item(1, "Escudo" );
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        String resultado = inventario.getDescricoesItens2();
        assertEquals("Espada,Escudo", resultado);        
    }
    
    @Test
    public void getItemMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada" );
        Item lanca = new Item(5, "Lança" );
        Item escudo = new Item(1, "Escudo" );
        
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    
    @Test
    public void getItemMaiorQuantidadeComItensDeMesmaQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada" );
        Item lanca = new Item(5, "Lança" );
        Item escudo = new Item(5, "Escudo" );
        
        inventario.adicionar(espada);
        inventario.adicionar(lanca);
        inventario.adicionar(escudo);
        
        Item maiorQtd = inventario.getItemComMaiorQuantidade();
        assertEquals( lanca, maiorQtd );
    }
    //____________________

    @Test
    public void adicionarItensNoInventario(){
        Item item = new Item(1, "Flecha");
        Item item2 = new Item(4, "Arco");
        Item item3 = new Item(7, "Arco");

        Inventario inventario = new Inventario();

        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);

        assertEquals(1, inventario.getItem(0).getQuantidade());
        assertEquals(4, inventario.getItem(1).getQuantidade());
        assertEquals(7, inventario.getItem(2).getQuantidade());

    }

    @Test
    public void removerItensDoInventario(){
        Item item = new Item(1, "Flecha");
        Item item2 = new Item(4, "Arco");
        Item item3 = new Item(7, "Arco");

        Inventario inventario = new Inventario();

        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);

        inventario.remover(1);
        inventario.remover(0);

        assertEquals(false, inventario.getItens().contains(item2));
        assertEquals(false, inventario.getItens().contains(item));
        assertEquals("Arco", inventario.getItem(0).getDescricao());

    }

    @Test
    public void obterItemDoInventario(){
        Item item = new Item(1, "Flecha");
        Item item2 = new Item(4, "Arco");
        Item item3 = new Item(7, "Arco");

        Inventario inventario = new Inventario();

        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);

        assertEquals(4, inventario.obter(1).getQuantidade());      
        assertEquals(7, inventario.obter(2).getQuantidade());  
    }

    @Test
    public void testarRetornoMaiorItem(){
        Item item = new Item(1, "Flecha");
        Item item2 = new Item(4, "Arco");
        Item item3 = new Item(96, "Arco");
        Item item4 = new Item(96, "Armario");

        Inventario inventario = new Inventario();
        inventario.adicionar(item);
        inventario.adicionar(item2);
        inventario.adicionar(item3);
        inventario.adicionar(item4);

        assertEquals(96, inventario.getItemComMaiorQuantidade().getQuantidade());

    }
}
