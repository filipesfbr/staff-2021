import java.util.*;

public class EstatisticasInventario{
        private Inventario inventario;

    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    private boolean EVazio(){
        return this.inventario.getItens().isEmpty();
    }
    
    public double calcularMedia(){
        if(EVazio()){
            return Double.NaN;
        }
        
        double quantidadeTotalDeItens = 0;
        for(int i = 0; i < inventario.getItens().size(); i++){
            quantidadeTotalDeItens += inventario.getItens().get(i).getQuantidade();
        }
        return quantidadeTotalDeItens/inventario.getItens().size();
    }
    
    
    
    public int metadeItens(){
        int metade;
        if(inventario.getItens().size() % 2 == 0){
            metade = (inventario.getItens().size() + 1) / 2;
            
        } else{
            metade = inventario.getItens().size() / 2;
        }
        return metade;
    }
    
    public double calcularMediana(){
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        /*
        TODO: ordenar inventario;
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if (qtdImpar){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;
        
        
        */
        
        
        ArrayList<Integer> quantidadeItens = new ArrayList<>();
        for(int i = 0; i < inventario.getItens().size(); i++){
            quantidadeItens.add(inventario.getItem(i).getQuantidade());
        }
        
        Collections.sort(quantidadeItens);
        
       
        return quantidadeItens.get(metadeItens()); 
        
    }
    
    public int qtdItensAcimaDaMedia(){
        double media = calcularMedia();
        int contador = 0;
        for(int i = 0; i < inventario.getItens().size(); i++){
            if(inventario.getItem(i).getQuantidade() >= media){
                contador++;
            }
        }
        return contador;
    }
}
