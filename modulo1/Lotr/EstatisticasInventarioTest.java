
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class EstatisticasInventarioTest{
    
    @Test
    public void calcularMediaInventarioVazio(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        
        assertTrue(Double.isNaN( resultado ) );
    }
    
    @Test
    public void calcularMediaUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(1, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        
        assertEquals(1, resultado, 1e-8);
    }
    
    @Test
    public void calcularMediaQtdIguais(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item(3, "Espada de Madeira"));
        inventario.adicionar(new Item(3, "Escudo de Madeira"));
        double resultado = estatisticas.calcularMedia();
        
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void calcularMediaQtdDiferentes(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item(2, "Escudo de Madeira"));
        inventario.adicionar(new Item(4, "Escudo de Madeira"));
        inventario.adicionar(new Item(3, "Botas"));
        double resultado = estatisticas.calcularMedia();
        
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaUmItem(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(6, "Escudo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        
        assertEquals(6, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaQtdsImpar(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5, "Espada de Madeira"));
        inventario.adicionar(new Item(10, "Escudo de Madeira"));
        inventario.adicionar(new Item(20, "Botas de gelo"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        
        assertEquals(10, resultado, 1e-8);
    }
    
    @Test
    public void calcularMedianaQtdsPares(){
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item(5, "Espada de Madeira"));
        inventario.adicionar(new Item(10, "Escudo de Madeira"));
        inventario.adicionar(new Item(20, "Botas de gelo"));
        inventario.adicionar(new Item(20, "Adaga"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        
        assertEquals(20, resultado, 1e-8);
    }
    
    
    @Test
    public void testarItensAcimaDaMediaUmItem(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        inventario.adicionar(new Item(1, "Alicate"));
        double resultado = estatisticas.calcularMedia();
        
       
        
        assertEquals(1, estatisticas.qtdItensAcimaDaMedia());
        
    }
    
    @Test
    public void qtdItensAcimaDaMediaVariosItens() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 5, "Espada de Madeira" ));
        inventario.adicionar(new Item( 10, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 20, "Botas de gelo" ));
        inventario.adicionar(new Item( 30, "Adaga" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 2, resultado );
    }
    
    @Test
    public void qtdItensAcimaDaMediaComItensIgualMedia() {
        Inventario inventario = new Inventario();
        inventario.adicionar(new Item( 1, "Espada de Madeira" ));
        inventario.adicionar(new Item( 2, "Escudo de Madeira" ));
        inventario.adicionar(new Item( 3, "Botas" ));
        inventario.adicionar(new Item( 4, "Adaga" ));
        inventario.adicionar(new Item( 5, "Luva" ));
        EstatisticasInventario estatisticas = new EstatisticasInventario( inventario );
        int resultado = estatisticas.qtdItensAcimaDaMedia();
        assertEquals( 3, resultado );
    }
    //____________________
    
    @Test
    public void testarCalcularMedia(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(7, "Alicate"));
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(6, "Alicate"));
        inventario.adicionar(new Item(4, "Alicate"));
        
       
        
        assertEquals(3.8, estatisticas.calcularMedia(), 0.1);
        
    }
    
    
    
    @Test
    public void testarCalcularMediana(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(7, "Alicate"));
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(6, "Alicate"));
        inventario.adicionar(new Item(4, "Alicate"));
        
       
        
        assertEquals(4, estatisticas.calcularMediana(), 0.1);
        
    }
    
    @Test
    public void testarItensAcimaDaMedia(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(7, "Alicate"));
        inventario.adicionar(new Item(1, "Alicate"));
        inventario.adicionar(new Item(6, "Alicate"));
        inventario.adicionar(new Item(4, "Alicate"));
        
       
        
        assertEquals(3, estatisticas.qtdItensAcimaDaMedia());
        
    }
    
        
}
