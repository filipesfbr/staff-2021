import java.util.*;
public class ExercitoQueAtaca extends ExercitoElfo {
    private EstrategiaDeAtaque estrategia;
    
    public ExercitoQueAtaca(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia(EstrategiaDeAtaque estrategia){
        this.estrategia = estrategia;
    }
    
    public void atacar(ArrayList<Anao> anoes){
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.getElfos());
        for( Elfo elfo : ordem){
            for( Anao anao : anoes ) {
                elfo.atirarFlecha(anao);
            }
        }
    }
}
