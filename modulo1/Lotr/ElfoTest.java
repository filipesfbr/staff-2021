import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{

    @After
    public void tearDown(){
        System.gc();
    }
    
    
    @Test
    public void testarGanharItem(){
        Elfo elfo = new Elfo ("Legolas");
        Item dardo = new Item(34, "Dardo");
        
        elfo.ganharItem(dardo);
        assertEquals(dardo, elfo.getInventario().getItens().get(2));
    }
    
    @Test
    public void exemploMudancaValoresItem(){
        //cria um Elfo
        Elfo elfo = new Elfo ("Legolas"); 
        //cria um Item com o Item Flecha de Elfo
        Item flecha = elfo.getFlecha();
        
        //seta a quantidade do item criado
        flecha.setQuantidade(100);
        
        assertEquals( 100, flecha.getQuantidade());
    }
    
    @Test
    public void elfoDeveNascerCom2Flechas(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(2, elfo.getFlecha().getQuantidade());
    }
    
    @Test
    public void elfoAtiraFlechaPerdeUmaUnidadeGanhaXP(){
        Elfo elfo = new Elfo("Legolas");
        Anao anao = new Anao("Grimli");
        
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(1, elfo.getExperiencia());
    }
    
    
    @Test
    public void elfoAtira3FlechasGanha2XP(){
        Elfo elfo = new Elfo ("Legolas");
        Anao anao = new Anao("Grimli");
        
        for(int i = 0; i < 3; i++){
            elfo.atirarFlecha(anao);
        }
        
        assertEquals(2, elfo.getExperiencia());
        assertEquals(0, elfo.getFlecha().getQuantidade());
     }
    
    @Test
    public void elfoAtirarFlecha12VezesEVidaDoAnaoFica0(){
        Elfo elfo = new Elfo ("Legolas");
        elfo.getFlecha().setQuantidade(12);
        Anao anao = new Anao("Gimli");
        
        for(int i = 0; i < 12; i++){
            elfo.atirarFlecha(anao);
        }
        
        assertEquals(0, anao.getVida(), 0.1);
        assertEquals(12, elfo.getExperiencia());
        assertEquals(0, elfo.getFlecha().getQuantidade());
    }
   
}
