import java.util.*;
import java.lang.String;
import java.util.Comparator;
public class Inventario{
    private ArrayList<Item> itens;
    
    public Inventario(){
        this.itens = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    
    public Item getItem(int posicao){
        return this.itens.get(posicao);
    }
    
    public void adicionar(Item item){
        itens.add(item);
    }
    
    private boolean validaSePosicao(int posicao){
        return posicao >= this.itens.size();
    }
    
    public Item obter(int posicao){
        return this.validaSePosicao(posicao) ? null : this.itens.get(posicao);
        // posicao >= this.itens.size() ? null : this.itens.get(posicao);
    }
    
    
    public void remover(int posicao){
        if( !this.validaSePosicao(posicao)){
            this.itens.remove(posicao);
        }
    }
    
    public void remover(Item item){
        this.itens.remove(item);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        int i = 0;
        for(Item item : this.itens){
            String descricao = item.getDescricao();
            descricoes.append(descricao);
            //descricoes.append(",");
            
            i++;
            boolean deveColocarVirgula = i < this.itens.size() - 1;
            if( deveColocarVirgula ){
                descricoes.append(",");
            }
        }
        
        return descricoes.toString();
    }
    
    public String getDescricoesItens2(){
        StringBuilder descricoes = new StringBuilder();
        for( int i = 0; i < itens.size(); i++ ){
            String descricao = itens.get(i).getDescricao();
            descricoes.append(descricao);
            if( i < itens.size() -1){
                descricoes.append(",");
            }
        }
        return descricoes.toString();
    }
    
    
    public Item getItemComMaiorQuantidade(){
        Item maiorQuantidade = new Item();
        for(int i = 0; i < itens.size(); i++){
            if(itens.get(i).getQuantidade() > maiorQuantidade.getQuantidade()){
                maiorQuantidade = itens.get(i);
            }
        }
        return maiorQuantidade;
    }
    
    public Item buscar(String busca){        
        for(Item item : this.itens){
            if(item.getDescricao().equals(busca)){
                return item;
            }
        }
        return null;
    }
    
    public ArrayList inverter(){
        ArrayList<Item> retorno = new ArrayList<>();
        for(int i = this.itens.size() - 1; i >= 0; i--){
            retorno.add(itens.get(i));
        }
        return retorno;
    }
    
    public void ordenarItens(){
        /*Item itemAux;
        boolean parada = false;
        while(!parada){
            parada = true;
            for(int i = 0; i < this.itens.size() - 1; i++){
                if(this.itens.get(i).getQuantidade() > this.itens.get(i + 1).getQuantidade()){
                    itemAux = this.itens.get(i);
                    this.itens.set(i, this.itens.get(i + 1));
                    this.itens.set(i + 1,itemAux);
                    parada = false;
                }
            }
        } */
        //bubble sort
        for(int i = 0; i < this.itens.size(); i++){
            for( int j = 0; j < this.itens.size() -1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                boolean deveTrocar = atual.getQuantidade() > proximo.getQuantidade();
                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j, itemTrocado);
                   
                }
            }
        }
    }
    
    public void ordenarItens(TipoOrdenacao status){
        if(this.itens != null){
            if(status == TipoOrdenacao.ASC){
               this.ordenarItens(); 
            }else if(status == TipoOrdenacao.DESC){
                Item itemAux;
                boolean parada = false;
                while(!parada){
                    parada = true;
                    for(int i = 0; i < this.itens.size() - 1; i++){
                        if(this.itens.get(i).getQuantidade() < this.itens.get(i + 1).getQuantidade()){
                            itemAux = this.itens.get(i);
                            this.itens.set(i, this.itens.get(i + 1));
                            this.itens.set(i + 1,itemAux);
                            parada = false;
                        }
                    }
                }
            }  
        }
    }
}
