public class AnaoBarbaLonga extends Anao{
    DadoD6 dado = new DadoD6();
    private Sorteador sorteador;
    
    public AnaoBarbaLonga(String nome){
        super(nome);
        this.sorteador = new DadoD6();
    }

    public AnaoBarbaLonga(String nome, Sorteador sorteador){
        super(nome);
        this.sorteador = sorteador;
    }
    
    @Override
    protected void sofrerDano(){
        boolean devePerderVida = sorteador.sortear() <= 4;
        if(devePerderVida){
            super.sofrerDano();
        }
    }
    
    protected boolean podeSofrerDano(){
        return this.vida > 0 && dado.sortear() > 2;
    }
}