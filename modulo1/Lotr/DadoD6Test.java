import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    
    @Test
    public void testarDadoD6(){
        DadoD6 dado = new DadoD6();
        for(int i = 0; i < 10; i++){
            int sorteado = dado.sortear();
            assertTrue(sorteado > 0 && sorteado < 7);
        }
        
        
    }
}
