import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfoTest{
    @Test
    public void testarOrdenacaoElfosAtacantes(){
        ExercitoElfo exercito = new ExercitoElfo();

        ElfoVerde elfo = new ElfoVerde("Legolas");
        ElfoNoturno elfo1 = new ElfoNoturno("Legolas");
        ElfoDaLuz elfo2 = new ElfoDaLuz("Legolas");
        Elfo elfo3 = new Elfo("Legolas");
        ElfoVerde elfo4 = new ElfoVerde("Legolas");

        exercito.alistarElfo(elfo);
        exercito.alistarElfo(elfo1);
        exercito.alistarElfo(elfo2);
        exercito.alistarElfo(elfo3);
        exercito.alistarElfo(elfo4);

        ArrayList<Elfo> array = exercito.pelotaoNoturnosPorUltimo(exercito.getElfos());
        assertEquals(array.get(1), elfo);
        assertEquals(array.get(0), elfo4);
        assertEquals(array.get(2), elfo1);
    }

    @Test
    public void testarAtaqueIntercalado(){
        ExercitoElfo exercito = new ExercitoElfo();

        ElfoNoturno elfo1 = new ElfoNoturno("Legolas");
        ElfoVerde elfo = new ElfoVerde("Legolas");
        ElfoVerde elfo2 = new ElfoVerde("Legolas");
        ElfoNoturno elfo3 = new ElfoNoturno("Legolas");
        ElfoVerde elfo4 = new ElfoVerde("Legolas");
        ElfoVerde elfo5 = new ElfoVerde("Legolas");
        ElfoNoturno elfo6 = new ElfoNoturno("Legolas");
        ElfoNoturno elfo7 = new ElfoNoturno("Legolas");

        exercito.alistarElfo(elfo);
        exercito.alistarElfo(elfo1);
        exercito.alistarElfo(elfo2);
        exercito.alistarElfo(elfo3);
        exercito.alistarElfo(elfo4);
        exercito.alistarElfo(elfo5);
        exercito.alistarElfo(elfo6);
        exercito.alistarElfo(elfo7);

        ArrayList<Elfo> teste = exercito.pelotaoDeAtaqueIntercalado(exercito.getElfos());

        assertTrue(teste.get(0) instanceof ElfoVerde);
        assertTrue(teste.get(1) instanceof ElfoNoturno);
        assertTrue(teste.get(2) instanceof ElfoVerde);
        assertTrue(teste.get(3) instanceof ElfoNoturno);
        assertTrue(teste.get(4) instanceof ElfoVerde);
        assertTrue(teste.get(5) instanceof ElfoNoturno);
        assertTrue(teste.get(6) instanceof ElfoVerde);
        assertTrue(teste.get(7) instanceof ElfoNoturno);

    }

    @Test
    public void testarSenhorDosAneis(){
        ExercitoElfo exercito = new ExercitoElfo();

        ElfoNoturno elfo = new ElfoNoturno("Legolas");
        ElfoNoturno elfo1 = new ElfoNoturno("Legolas");
        ElfoVerde elfo2 = new ElfoVerde("Legolas");
        ElfoVerde elfo3 = new ElfoVerde("Legolas");

        elfo.getFlecha().setQuantidade(3);
        elfo1.getFlecha().setQuantidade(12);
        elfo2.getFlecha().setQuantidade(40);
        elfo3.getFlecha().setQuantidade(50);

        exercito.alistarElfo(elfo);
        exercito.alistarElfo(elfo1);
        exercito.alistarElfo(elfo2);
        exercito.alistarElfo(elfo3);

        ArrayList<Elfo> resultado = exercito.ataqueSenhorDosAneis(exercito.getElfos());

        assertEquals(50, resultado.get(0).getQtdFlecha());
        assertEquals(elfo1, resultado.get(2));
        assertFalse(resultado.contains(elfo));

    }
}
