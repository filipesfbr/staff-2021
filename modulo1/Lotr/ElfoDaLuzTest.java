import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    
    @Test
    public void testarAtaqueElfoDeLuz(){
        ElfoDaLuz elfo = new ElfoDaLuz("AAA");
        Anao anao = new Anao("bbbb");
        
        elfo.atacarComEspada(anao);
        assertEquals(79.0, elfo.getVida(), 0.1);
        
        elfo.atacarComEspada(anao);
        assertEquals(89.0, elfo.getVida(), 0.1);
        
        assertEquals(2, elfo.getExperiencia());
    }
}
