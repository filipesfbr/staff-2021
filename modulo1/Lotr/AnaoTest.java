
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnaoTest{
    @Test
    public void anaoNasceComEscudoNoInventario(){
        Anao anao = new Anao("Gimli");
        Item esperado = new Item(1, "Escudo");
        
        Item resultado = anao.getInventario().obter(0);
        
        assertEquals( esperado, resultado );
    }
    
    @Test
    public void anaoEquipadoTomaMetade(){
        Anao anao = new Anao("Gimli");
        anao.equiparEDesequiparEscudo();
        anao.sofrerDano();
        
        assertEquals( 105.0, anao.getVida(), 0.0001);
    }
    
    @Test
    public void anaoNaoEquipadoETomaDano(){
        Anao anao = new Anao("Gimli");
        anao.equiparEDesequiparEscudo();
        anao.equiparEDesequiparEscudo();
        anao.sofrerDano();
        
        assertEquals( 100.0, anao.getVida(), 0.0001);
    }
    
    @Test
    public void receberDanoComEscudo(){
        Anao anao = new Anao("Gimli");
        
        anao.sofrerDano();
        assertEquals(100.0, anao.getVida(), 0.1);
        
        
        anao.equiparEDesequiparEscudo();
        anao.sofrerDano();
        assertEquals(95, anao.getVida(), 0.1);
    }
    
    @Test
    public void ganharItem(){
        Anao anao = new Anao("Gimli");
        Item item = new Item(1, "Cachecol");
        
        anao.ganharItem(item);
        assertEquals("Escudo", anao.getInventario().getItens().get(0).getDescricao());
        assertEquals("Cachecol", anao.getInventario().getItens().get(1).getDescricao());
    }
    
    @Test
    public void perderItem(){
        Anao anao = new Anao("Gimli");
        Item item = new Item(1, "Cachecol");
        Item item2 = new Item(4, "Barcos");
        anao.getInventario().adicionar(item2);
        
        
        anao.perderItem(item);
        assertEquals("Barcos", anao.getInventario().getItens().get(1).getDescricao());
    }
    @Test
    public void matarOAnao(){
        Anao anao = new Anao("Gimli");
        
        for(int i = 0; i < 12; i++){
                anao.sofrerDano();
        }   
        assertEquals(Status.MORTO, anao.getStatus());
    }
    
    @Test
    public void receberDanoDepoisDeMorto(){
        Anao anao = new Anao("Gimli");
        
        for(int i = 0; i < 13; i++){
                anao.sofrerDano();
        }   
        assertEquals(Status.MORTO, anao.getStatus());
        assertEquals(0, anao.getVida(), 0.1);
    }
    @Test
    public void anaoDeveNascerCom110DeVida(){
        Anao anao = new Anao("Gimli");
        assertEquals(110.0, anao.getVida(), 0.1);
        assertEquals( Status.RECEM_CRIADO, anao.getStatus());
    }
    
    @Test
    public void sofreDanoEFicaCom100DeVida(){
        Anao anao = new Anao("Gimli");
        
        anao.sofrerDano();
        assertEquals(100.0, anao.getVida(), 0.1);
        assertEquals(Status.SOFREU_DANO, anao.getStatus());
    }
    
    @Test
    public void sofreDano12VezesEFicaCom0DeVida(){
        Anao anao = new Anao("Gimli");
        
        for(int i = 0; i < 12; i++){
                anao.sofrerDano();
        }   
        assertEquals(0.0, anao.getVida(), 0.1);
        assertEquals(Status.MORTO, anao.getStatus());
    }
}
