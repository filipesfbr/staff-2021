public class Elfo extends Personagem{
    protected int indiceFlecha;
    private static int qtdElfos;
    {
        this.indiceFlecha = 1;
        Elfo.qtdElfos = 0;
    }

    public Elfo(String nome) {
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        Elfo.qtdElfos++;
    }
    
    public static int getQuantidadeElfos(){
        return Elfo.qtdElfos;
    }

    public void atirarFlecha(Anao anao){
        if(this.podeAtirar()){
            this.getFlecha().setQuantidade(this.getQtdFlecha() - 1);
            super.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }    
    }
 
    public String imprimirNomeClasse(){
        return "Elfo";
    }
    
    protected boolean podeAtirar(){
        return this.getQtdFlecha() > 0;
    }
    
    public void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha(){
       return this.inventario.obter(indiceFlecha); 
    }
    
    protected int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }

}