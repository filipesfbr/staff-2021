import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfosNoturnosTest {
    
    @Test
    public void testarAtirarEPerder15DeVida(){
        ElfoNoturno elfo = new ElfoNoturno("AA");
        Anao anao = new Anao ("Joao");
        
        elfo.atirarFlecha(anao);
        
        assertEquals(3, elfo.getExperiencia());
        assertEquals(85.0, elfo.getVida(), 0.0001);
    }
    
    @Test
    public void testarAtirar2XEPerder15DeVida(){
        ElfoNoturno elfo = new ElfoNoturno("AA");
        Anao anao = new Anao ("Joao");
        
        elfo.atirarFlecha(anao);
        elfo.atirarFlecha(anao);
        
        assertEquals(90.0, anao.getVida(), 0.1);
        assertEquals(6, elfo.getExperiencia());
        assertEquals(70.0, elfo.getVida(), 0.0001);
    }
}
