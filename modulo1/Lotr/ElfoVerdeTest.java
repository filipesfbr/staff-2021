import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoVerdeTest {
    @Test
    public void testarAtirar2Flechas(){
        ElfoVerde elfo = new ElfoVerde("Legolas");
        Anao anao = new Anao("Grimli");
        
        elfo.atirarFlecha(anao);
        assertEquals(1, elfo.getFlecha().getQuantidade());
        assertEquals(2, elfo.getExperiencia());
    }
}
