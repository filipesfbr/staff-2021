public abstract class Personagem {
    protected String nome;
    protected int experiencia, qtdExperienciaPorAtaque;
    protected Inventario inventario;
    protected double vida;
    protected Status status;
    protected double qtdDano;

    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdDano = 0.0;
    }

    public Personagem(String nome){
        this.nome = nome;

    }

    protected void aumentarXP(){
        this.experiencia += qtdExperienciaPorAtaque;
    }

    protected void sofrerDano(){
        if(this.podeSofrerDano()){
            this.vida -= this.vida >= this.qtdDano ? this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
    }

    public abstract String imprimirNomeClasse();
    
    protected Status validacaoStatus(){
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }

    protected boolean podeSofrerDano(){
        return this.vida > 0;
    }  

    public String getNome(){
        return nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }

    public int getExperiencia(){
        return this.experiencia;
    }

    public Inventario getInventario(){
        return this.inventario;
    }

    public Status getStatus(){
        return this.status;
    }

    public double getVida(){
        return this.vida;
    }

    public void perderItem(Item item){
        this.inventario.remover(item);
    }

    public void ganharItem(Item item){
        this.inventario.adicionar(item);
    }
}
