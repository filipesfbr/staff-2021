import java.util.*;

public class ElfoDaLuz extends Elfo{
   
    private final double QTD_VIDA_GANHA = 10;
    private int qtdAtaques;
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    private int numeroDoAtaque = 1;
    
    {
        qtdAtaques = 0;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_VALIDAS.get(0) ));
        super.qtdDano = 21;
    }
    
    @Override
    public void perderItem(Item item){
        if(item.getDescricao().equals(DESCRICOES_VALIDAS.get(0))){
            this.inventario.remover(item);
        }
    }
    
    private boolean devePerderVida(){
        return qtdAtaques % 2 == 1;
    }
    
    private Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_VALIDAS.get(0));
    }
    
    private void ganharVida(){
        super.vida += QTD_VIDA_GANHA;
    }
    
    
    
    public void atacarComEspada(Anao anao){
        Item espada = getEspada();
        if(espada.getQuantidade() > 0){
            qtdAtaques++;
            anao.sofrerDano();
            if (this.devePerderVida()){
                sofrerDano();
            }else{ 
                ganharVida();
            }
        }
        
        /*
        if(this.numeroDoAtaque % 2 == 0){
            this.vida += 10.0;
            super.aumentarXP();
            anao.sofrerDano();
            this.numeroDoAtaque++;
        } else{
            super.aumentarXP();
            anao.sofrerDano();
            super.sofrerDano();
            this.numeroDoAtaque++;
        } */
    }
    
    
    public void atirarFlecha(Anao anao){
        if(this.podeAtirar()){
            this.getFlecha().setQuantidade(this.getQtdFlecha() - 1);
            super.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }    
    }
}
