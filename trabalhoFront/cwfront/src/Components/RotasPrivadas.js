import React from "react";
import { Route, Redirect } from 'react-router-dom'

const RotasPrivadas = ( { component: Component, ...resto } ) => (
  <Route { ...resto } render={ props => (
    localStorage.getItem( 'token' ) ?
      <Component { ...props } /> :
      <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  )}
  />
);


export { RotasPrivadas };