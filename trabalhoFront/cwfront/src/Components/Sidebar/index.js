import { Layout, Menu } from 'antd';
import { UserOutlined, ShopOutlined, NotificationOutlined, BankFilled, LayoutFilled, WalletFilled, ScheduleOutlined } from '@ant-design/icons';
import { Link, Switch } from 'react-router-dom';
import CadastroCliente from '../../Pages/CadastroCliente';
import CadastroTipoContato from '../../Pages/CadastroTipoContato';
import ListaClientes from '../../Pages/Listas/Clientes';
import { RotasPrivadas } from '../RotasPrivadas';
import CadastroEspaco from '../../Pages/CadastroEspaco';
import ListaEspacos from '../../Pages/Listas/Espacos';
import PesquisarEspaco from '../../Pages/Busca/Espaco';
import CadastroPacote from '../../Pages/CadastroPacote';
import ListaPacotes from '../../Pages/Listas/Pacotes';
import PesquisaPacote from '../../Pages/Busca/Pacote';
import Contratacao from '../../Pages/Contratacao';
import ListaContratacao from '../../Pages/Listas/Contratacao';
import PesquisarContratacao from '../../Pages/Busca/Contratacao';
import ClientePacote from '../../Pages/ClientePacote';
import ListaClientePacote from '../../Pages/Listas/ClientePacote';
import PesquisarClientePacote from '../../Pages/Busca/ClientePacote';
import Pagamento from '../../Pages/Pagamento';
import ListaPagamentos from '../../Pages/Listas/Pagamentos';


const { SubMenu } = Menu;
const { Sider } = Layout;

const Sidebar = () => {
  return (
    <Layout>
      <Sider className="site-layout-background" width={200}>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={['0']} defaultOpenKeys={['']} style={{ height: '100%' }} >

              <SubMenu key="sub1" icon={<UserOutlined />} title="Cliente">
                <Menu.Item key="1"> <Link to="/home/cadastrocliente" /> Cadastrar Cliente</Menu.Item>
                <Menu.Item key="2"> <Link to="/home/listarclientes" />Listar Clientes</Menu.Item>
                <Menu.Item key="3"> <Link tp="/home/listarcontatos"/> Listar Contatos</Menu.Item>
                <Menu.Item key="5"> <Link to="/home/cadastrotipo" /> Cadastrar TipoContato</Menu.Item>
              </SubMenu>
              <SubMenu key="sub2" icon={<ShopOutlined />} title="Espacos">
                <Menu.Item key="6"> <Link to="/home/cadastroespaco" /> Cadastrar Espaco</Menu.Item>
                <Menu.Item key="7"> <Link to="/home/listaespacos" />Listar Espacos</Menu.Item>
                <Menu.Item key="8"><Link to="/home/buscarespaco" />Buscar Espaco</Menu.Item>
              </SubMenu>
              <SubMenu key="sub3" icon={<LayoutFilled />} title="Pacotes">
                <Menu.Item key="9"> <Link to="/home/cadastropacote"/>Criar Pacote</Menu.Item>
                <Menu.Item key="10"> <Link to="/home/listarpacotes"/>Listar Pacotes</Menu.Item>
                <Menu.Item key="13"><Link to="/home/buscarpacotes"/>Buscar Pacotes</Menu.Item>
              </SubMenu>
              <SubMenu key="sub4" icon={<WalletFilled />} title="Contratação">
                <Menu.Item key="14"><Link to="/home/criarcontratacao"/> Criar Contratação</Menu.Item>
                <Menu.Item key="15"><Link to="/home/listarcontratacoes" />Listar Contratações</Menu.Item>
                <Menu.Item key="16"><Link to="/home/buscarcontratacao" /> Buscar Contratação</Menu.Item>
              </SubMenu>
              <SubMenu key="sub5" icon={<NotificationOutlined /> } title="Contrat. Pacote" >
                <Menu.Item key="17"><Link to="/home/criarcontratacaopac" />Criar Cont. Pacote</Menu.Item>
                <Menu.Item key="18"><Link to="/home/listarcontratacaopac" />Lista Cont. Pacote</Menu.Item>
                <Menu.Item key="19"><Link to="/home/buscarclientepacote" />Busca Cont. Pacote</Menu.Item>
              </SubMenu>
              <SubMenu key='sub6' icon={<BankFilled/>} title='Pagamento'>
                <Menu.Item key='20'><Link to='/home/pagar' />Pagar</Menu.Item>
                <Menu.Item key='21'><Link to='/home/pagamentos/' />Lista Pagamentos</Menu.Item>
              </SubMenu>
              <SubMenu key='sub7' icon={<ScheduleOutlined/>} title='Acessos'>
                <Menu.Item key='22'><Link to='/home/acesso/entrar' />Entrar</Menu.Item>
                <Menu.Item key='22'><Link to='/home/acesso/sair' />Sair</Menu.Item>
              </SubMenu>
            </Menu>
        </Sider>
          <Switch>
            <RotasPrivadas path='/home/cadastrocliente' component={ CadastroCliente } />
            <RotasPrivadas path='/home/cadastrotipo' component={ CadastroTipoContato } />
            <RotasPrivadas path='/home/listarclientes' component={ ListaClientes } />
            <RotasPrivadas path='/home/cadastroespaco' component={ CadastroEspaco } />
            <RotasPrivadas path='/home/listaespacos' component={ ListaEspacos } />
            <RotasPrivadas path='/home/buscarespaco' component={ PesquisarEspaco } />
            <RotasPrivadas path='/home/cadastropacote' component={ CadastroPacote } />
            <RotasPrivadas path='/home/listarpacotes' component={ ListaPacotes } />
            <RotasPrivadas path='/home/buscarpacotes' component={ PesquisaPacote } />
            <RotasPrivadas path='/home/criarcontratacao' component={ Contratacao } />
            <RotasPrivadas path='/home/listarcontratacoes' component={ ListaContratacao } />
            <RotasPrivadas path='/home/buscarcontratacao' component={ PesquisarContratacao } />
            <RotasPrivadas path='/home/criarcontratacaopac' component={ ClientePacote } />
            <RotasPrivadas path='/home/listarcontratacaopac' component={ ListaClientePacote } />
            <RotasPrivadas path='/home/buscarclientepacote' component= { PesquisarClientePacote } />
            <RotasPrivadas path='/home/pagar' component = { Pagamento } />
            <RotasPrivadas path='/home/pagamentos/' component = { ListaPagamentos } />
            {/* <RotasPrivadas path='/home/acesso/entrar' component= {  } /> */}
            {/* <RotasPrivadas path='/home/acesso/sair' component={  } */}
          </Switch>
      </Layout>
  )
}

export default Sidebar;