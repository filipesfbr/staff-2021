import { Form, Input, Button, Row, Col } from 'antd';
import { Component } from 'react';
import API from '../../Models/API';
import ClienteModel from '../../Models/ClienteModel';
import ContatoModel from '../../Models/ContatoModel';
import TipoContatoModel from '../../Models/TipoContatoModel';


export default class ClienteUi extends Component {
  constructor(props) {
    super(props)
    this.api = new API();
    this.state = {
      deveExibirSucesso: false,
      mensagemDeSucesso: 'Cadastro realizado!',
      deveExibirErro: false,
      mensagemDeErro: 'Dados de cadastro inválido!',
      tipoTelefone: false,
      tipoEmail: false
    }

    this.execute = this.execute.bind(this)
    this.onFinishFailed = this.onFinishFailed.bind(this)
  }

  componentDidMount(){
    let tipoTelefone = false;
    let tipoEmail = false;

    this.api.buscarTodosTiposContatos().then( tiposContato => {
      tiposContato.data.forEach( tipo => {
        if(tipo.nome === 'Telefone'){
          tipoTelefone = true;
        }
        if(tipo.nome === 'Email'){
          tipoEmail = true;
        }
      })
      if(tipoTelefone){
        this.setState( state =>{
          return {
            ...state,
            tipoTelefone
          }
        })
      }
      if(tipoEmail){
        this.setState( state => {
          return {
            ...state,
            tipoEmail
          }
        })
      }
      
      if(!tipoTelefone){
        let telefone = new TipoContatoModel('Telefone')
        this.api.salvarTipoContato(telefone);
      }
      if(!tipoEmail){
        let email = new TipoContatoModel('Email')
        this.api.salvarTipoContato(email);
      }
    }
    )
  }
  
  execute(values) {
    const { nome, cpf, dataNascimento, telefone, email } = values;
    let contatoTelefone = new ContatoModel(email, {id: 1});
    let contatoEmail = new ContatoModel(telefone, {id: 2});

    let contatos = [ contatoTelefone, contatoEmail ];

    let cliente = new ClienteModel(nome, cpf, dataNascimento, contatos);
    console.log(cliente)
    this.api.salvarCliente(cliente).then(e => {
      this.setState(state => {
        return {
          ...state,
          deveExibirSucesso: true
        }
      })
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirSucesso: false
          }
        })
      }, 3000)

    }).catch(e => {
      this.setState(state => {
        return {
          ...state,
          deveExibirErro: true
        }
      })
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirErro: false
          }
        })
      }, 3000)
    });
   
  }


  onFinishFailed(errorInfo) {
    console.log('Failed:', errorInfo);
    console.log("Nao deu")
  };



  render() {
    const { deveExibirSucesso, deveExibirErro, mensagemDeErro, mensagemDeSucesso } = this.state;

    return (
      <Row style={{ margin: '20px' }} type="flex" justify="center">
        <Col>
          <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={this.execute} onFinishFailed={this.onFinishFailed} >
            <Form.Item label="Nome" name="nome" rules={[{ required: true, message: 'Insira seu nome', },]}>
              <Input />
            </Form.Item>

            <Form.Item label="CPF" name="cpf" rules={[{ required: true, message: 'Insira seu cpf', }]} >
              <Input />
            </Form.Item>

            <Form.Item label="Nascimento" name="dataNascimento" rules={[{ required: true, message: 'Insira sua data de nascimento', }]} >
              <Input placeholder="YYYY-MM-DD" />
            </Form.Item>

            <Form.Item label="Telefone" name="telefone" rules={[{ required: true, message: 'Insira seu contato', },]} >
              <Input />
            </Form.Item>

            <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Insira seu contato', },]} >
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
              <div>
                {deveExibirSucesso ? mensagemDeSucesso : ''}
                {deveExibirErro ? mensagemDeErro : ''}
              </div>
            </Form.Item>
          </Form>
        </Col>
      </Row>

    );

  }

}