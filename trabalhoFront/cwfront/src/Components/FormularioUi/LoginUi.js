import { Form, Input, Button, Row, Col} from 'antd';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import API from '../../Models/API';
import UsuarioModel from '../../Models/UsuarioModel';
import './Forms.css'


const LoginUi = () => {
  const [contador, setContador] = useState();
  const [ mensagemErro ] = useState('Login inválido!')
  const [ deveExibir, setDeveExibir] = useState(false);
  const [redireciona, setRediciona] = useState(false);
  const api = new API();

  const onFinish = (values) => {
    const { username:login, password:senha } = values;
    let usuario = new UsuarioModel(login, senha);
    api.autenticarLogin(usuario).then( e => {
      setContador(contador + 1)
      verificaToken();
    }).catch(e => {
      setDeveExibir(true);

      setTimeout( () => {
        setDeveExibir(false);
      }, 2000)
    })
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  function verificaToken(){
    if(localStorage.getItem('token')){
      setRediciona(true);
    } else{
      setRediciona(false);
    }
  }

  return (
    <React.Fragment>
      { redireciona || localStorage.getItem('token') ? <Redirect push to="/home" /> : 
      <Row style={{margin: '20px'}} type="flex" align="middle" justify="center" >
        <Col>
          <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
            <Form.Item label="Username" name="username" rules={[ { required: true, message: 'Insira seu username!', }, ]} >
              <Input />
            </Form.Item>

            <Form.Item label="Password" name="password" rules={[ { required: true, message: 'Insira sua senha!', }, ]} >
              <Input.Password />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Form>
          
          <p className="mensagemErro">{deveExibir ? mensagemErro : ''}</p>
          
        </Col>
      </Row>
      }
    </React.Fragment>
    
  );
};

export default LoginUi;