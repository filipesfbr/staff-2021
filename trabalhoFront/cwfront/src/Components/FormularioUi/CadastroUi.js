import { Form, Input, Button, Row, Col } from 'antd';
import { useState } from 'react'
import API from '../../Models/API';
import CadastroModel from '../../Models/CadastroModel';
import '../../index.css'



const CadastroUi = ( props ) => {
  const api = new API();
  const [mensagemSucesso] = useState("Cadastro realizado!") 
  const [mensagemErro] = useState("Dados inválidos!")
  const [deveExibirErro, setDeveExibirErro] = useState(false);
  const [deveExibirSucesso, setDeveExibirSucesso] = useState(false);

  const onFinish = (values) => {
    const { nome, email, login, password } = values;
    let cadastro = new CadastroModel(nome, email, login, password);
    api.cadastrarUsuario(cadastro).then( response => {
      setDeveExibirSucesso(true);
      setTimeout( () => {
        setDeveExibirSucesso(false)
      }, 2000 );
    }).catch( response => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false)
      }, 2000 );
    })
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Row style={{margin: '20px'}} type="flex" align="middle" justify="center">
      <Col>
        <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
          <Form.Item label="Nome" name="nome" rules={[ { required: true, message: 'Insira seu nome', }, ]}>
            <Input />
          </Form.Item>

          <Form.Item label="Email" name="email" rules={[ { required: true, message: 'Insira seu e-mail', }, { type: 'email', message: 'E-mail inválido'}, ]} >
            <Input />
          </Form.Item>

          <Form.Item label="Login" name="login" rules={[ { required: true, message: 'Insira seu login'}, ]} >
            <Input />
          </Form.Item>

          <Form.Item label="Password" name="password" rules={[ { required: true, message: 'Insira sua senha', }, ]} >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

            <Button type="primary" htmlType="submit">
              Enviar
            </Button>
          </Form.Item>
        </Form>

        <p className="mensagem-sucesso">{deveExibirSucesso ? mensagemSucesso : ''}</p>
        <p className="mensagem-erro">{deveExibirErro ? mensagemErro : ''}</p>
      </Col>
    </Row>
    
  );
};

export default CadastroUi;