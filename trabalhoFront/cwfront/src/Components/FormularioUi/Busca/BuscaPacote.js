import { Form, Input, Button, Row, Col, Card, Tag } from 'antd';
import React from 'react';
import { useState } from 'react';
import API from '../../../Models/API';
import '../../../index.css'


const BuscaPacote = ( props ) => {
  const api = new API();
  const [pacote, setPacote] = useState();
  const [mensagemErro] = useState('Não localizado!')
  const [deveExibirErro, setDeveExibirErro] = useState(false);
  

  const onFinish = (values) => {
    const { idPacote } = values;
    api.buscarPacote(idPacote).then( response => {
      setPacote(response.data);
    }).catch( response => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false);
      }, 2500);
    })
  }

  const onFinishFailed = (errorInfo) => {
    
  };

  
  return (
    
    <React.Fragment>
      <Row style={{margin: '20px'}} type="flex"  justify="center">
        <Col>
          <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
            <Form.Item label="ID" name="idPacote" rules={[ { required: true, message: 'Insira um ID', }, ]}>
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Form>
          <p className='mensagem-erro'>{deveExibirErro ? mensagemErro : ''}</p>
          
          { pacote ? 
          <Card title={`${pacote.id}`} bordered="true" className="lista-dados">
            <p>Preço: { pacote.valor }</p>
            <p>Espacos: {pacote.espacoPacote.map( obj =>  <Tag color='cyan' >{obj.espaco.nome}</Tag> )} </p>
            </Card>
          : ''
          }
         
        </Col>
      </Row>
      
    </React.Fragment>
  )
}

export default BuscaPacote;