import { Form, Input, Button, Row, Col, Card } from 'antd';
import React from 'react';
import { useState } from 'react';
import API from '../../../Models/API';


const BuscaContratacao = ( props ) => {
  const api = new API();
  const [contratacao, setContratacao] = useState();
  const [mensagemErro] = useState('Não localizado!')
  const [deveExibirErro, setDeveExibirErro] = useState(false);
  

  const onFinish = (values) => {
    const { idContratacao } = values;
    api.buscarContratacao(idContratacao).then( response => {
      setContratacao(response.data);
    }).catch( response => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false);
      }, 2500);
    })

    console.log(contratacao)
  }

  const onFinishFailed = (errorInfo) => {
    
  };

  
  return (
    
    <React.Fragment>
      <Row style={{margin: '20px'}} type="flex"  justify="center">
        <Col>
          <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
            <Form.Item label="ID" name="idContratacao" rules={[ { required: true, message: 'Insira um ID', }, ]}>
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Form>
          <p className='mensagem-erro'>{deveExibirErro ? mensagemErro : ''}</p>

          { contratacao ? 
          <Card bordered="true" className="lista-dados">
            <p>ID: { contratacao.id }</p>
            <p>Nome: {contratacao.cliente.nome} </p>
            <p>Preço: { contratacao.valor }</p>
            <p>Quantidade: { contratacao.tipoContratacao }</p>
            <p>Prazo: {contratacao.prazo} </p>
            </Card>
          : ''
          }
         
        </Col>
      </Row>
      
    </React.Fragment>
  )
}

export default BuscaContratacao;