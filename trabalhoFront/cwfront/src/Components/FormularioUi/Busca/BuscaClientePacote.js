import { Form, Input, Button, Row, Col, Card } from 'antd';
import React from 'react';
import { useState } from 'react';
import API from '../../../Models/API';


const BuscaClientePacote = ( props ) => {
  const api = new API();
  const [clientePacote, setClientePacote] = useState();
  const [mensagemErro] = useState('Não localizado!')
  const [deveExibirErro, setDeveExibirErro] = useState(false);
  

  const onFinish = (values) => {
    const { idClientePacote } = values;
    api.buscarClientePacote(idClientePacote).then( response => {
      setClientePacote(response.data);
    }).catch( response => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false);
      }, 2500);
    })

  }

  const onFinishFailed = (errorInfo) => {
    
  };

  
  return (
    
    <React.Fragment>
      <Row style={{margin: '20px'}} type="flex"  justify="center">
        <Col>
          <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
            <Form.Item label="ID" name="idClientePacote" rules={[ { required: true, message: 'Insira um ID', }, ]}>
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

              <Button type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Form>
          <p className='mensagem-erro'>{deveExibirErro ? mensagemErro : ''}</p>

          { clientePacote ? 
          <Card bordered="true" className="lista-dados">
            <p>ID: { clientePacote.id }</p>
            <p>Cliente: {clientePacote.cliente.nome}</p>
            <p>Pacote ID: {clientePacote.pacote.id}</p>
            <p>Preço: {clientePacote.pacote.valor}</p>
        
           
            </Card>
          : ''
          }
         
        </Col>
      </Row>
      
    </React.Fragment>
  )
}

export default BuscaClientePacote;