import { Form, Input, Button, Row, Col} from 'antd';
import API from '../../Models/API';
import TipoContatoModel from '../../Models/TipoContatoModel'
import { useState } from 'react';


const TipoContatoUi = () => {
  const api = new API();
  const [mensagemDeSucesso] = useState('Cadastrado com sucesso!')
  const [mensagemDeErro] = useState('Dados inválidos!');
  const [deveExibirSucesso, setDeveExibirSucesso] = useState(false);
  const [deveExibirErro, setDeveExibirErro] = useState(false);





  const onFinish = (values) => {
    const { nome } = values;
    let tipoContato = new TipoContatoModel(nome);

    api.salvarTipoContato(tipoContato).then( e => {
      setDeveExibirSucesso(true)
      setTimeout( () => {
        setDeveExibirSucesso(false);
      }, 2000)
    }).catch( e => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false);
      }, 2000)
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  

  return (
    <Row style={{margin: '20px'}} type="flex" justify="center" >
      <Col>
        <Form name="basic" labelCol={{ span: 8, }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
          <Form.Item label="Nome" name="nome" rules={[ { required: true, message: 'Insira o tipo de contato', }, ]} >
            <Input />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

            <Button type="primary" htmlType="submit">
              Enviar
            </Button>
            <div>
              {deveExibirSucesso ? mensagemDeSucesso : ''}
              {deveExibirErro ? mensagemDeErro : ''}
            </div>

          </Form.Item>
        </Form>
      </Col>
    </Row>
    
  );
};

export default TipoContatoUi;