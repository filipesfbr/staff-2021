import { Form, Input, Button, Row, Col } from 'antd';
import { useState } from 'react';
import API from '../../Models/API';
import EspacoModel from '../../Models/EspacoModel'


const EspacoUi = ( props ) => {
  
  const api = new API();
  const [mensagemDeSucesso] = useState('Cadastrado com sucesso!')
  const [mensagemDeErro] = useState('Dados inválidos!');
  const [deveExibirSucesso, setDeveExibirSucesso] = useState(false);
  const [deveExibirErro, setDeveExibirErro] = useState(false);

  const onFinish = (values) => {
    const { nome, capacidade, valor } = values;
    let espaco = new EspacoModel(nome, parseInt(capacidade), valor);
    console.log(espaco);
    api.salvarEspaco(espaco).then(e => {
      setDeveExibirSucesso(true);
      setTimeout( () => {
        setDeveExibirSucesso(false);
      }, 2000);

    }).catch(e => {
      setDeveExibirErro(true);
      setTimeout( () => {
        setDeveExibirErro(false);
      }, 2000)
    })

  };

  const onFinishFailed = (errorInfo) => {
    
  };

  return (
    <Row style={{margin: '20px'}} type="flex" justify="center">
      <Col >
        <Form name="basic" labelCol={{ span: 11 }} wrapperCol={{ span: 16, }} initialValues={{ remember: true, }} onFinish={onFinish} onFinishFailed={onFinishFailed} >
          <Form.Item label="Nome" name="nome" rules={[ { required: true, message: 'Insira o nome', }, ]}>
            <Input />
          </Form.Item>

          <Form.Item label="Capacidade" name="capacidade" rules={[ { required: true, message: 'Insira a capacidade', } ]} >
            <Input />
          </Form.Item>

          <Form.Item label="Preço em minutos" name="valor"  rules={[ { required: true, message: 'Insira o valor'}, ]} >
            <Input prefix="R$ "/>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

            <Button type="primary" htmlType="submit">
              Enviar
            </Button>
            <div>
              {deveExibirSucesso ? mensagemDeSucesso : ''}
              {deveExibirErro ? mensagemDeErro : ''}
            </div>
          </Form.Item>
        </Form>
      </Col>
    </Row>
    
  );
};

export default EspacoUi;