import { Button } from 'antd';
import React from 'react';
import CadastroUi from '../../Components/FormularioUi/CadastroUi';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';


export default class Cadastro extends React.Component{

  render (){
    return(
      <React.Fragment>
        
        <CadastroUi></CadastroUi>
      
        <Link to="/" style={{margin: '20px', float: ''}}>
        <Button type="default">Login</Button>
        </Link>
        
      </React.Fragment>
    )
  }
}