import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Cadastro from '../CadastroUsuario';
import Login from '../Login';
import { RotasPrivadas } from '../../Components/RotasPrivadas';
import App from '../../App';

export default class Rotas extends Component {

  render(){
    return (
      <Router>
        <Route path='/cadastro' exact component={ Cadastro } />
        <Route path='/' exact component={ Login } />
        <RotasPrivadas path='/home' component={ App } />
        
      </Router>
    );
  }
}