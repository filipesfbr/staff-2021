import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col, Layout } from 'antd';
import ListaClientes from '../Listas/Clientes';
import API from '../../Models/API';
import ListaPacotes from '../Listas/Pacotes';
import ClientePacoteModel from '../../Models/ClientePacoteModel'

export default class ClientePacote extends React.Component {
  constructor(props) {
    super(props)
    this.api = new API();

    this.state = {
      espacoPacote: [],
      espacosAdd: [],
      mensagemSucesso: 'Contratação realizada!',
      mensagemErro: 'Dados inválidos!',
      deveExibirSucesso: false,
      deveExibirErro: false
    }

    this.execute = this.execute.bind(this)
  }

  execute(values) {
    const { idPacote, idCliente, quantidade } = values;
    let pacote = { id: idPacote };
    let cliente = { id: idCliente };
    let clientePacote = new ClientePacoteModel(cliente, pacote, parseInt(quantidade));
    this.api.salvarClientePacote(clientePacote).then( response => {
      this.setState(state => {
        return {
          ...state,
          deveExibirSucesso: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirSucesso: false
          }
        });
      }, 2000);
    }).catch ( response => {
      this.setState(state => {
        return {
          ...state,
          deveExibirErro: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirErro: false
          }
        });
      }, 2000);
    })
  }



  render() {
    const { deveExibirErro, deveExibirSucesso, mensagemErro, mensagemSucesso } = this.state;
    return (
      <React.Fragment>
        <Layout>

          <Row type="flex" justify="space-around" align="left">

            <Col span={12}>
              <h5>Pacotes disponíveis: </h5>
              <ListaPacotes></ListaPacotes>
            </Col>

            <Col span={7}>
              <h5>1. Criar Contratação: </h5>

              <Row style={{ margin: '20px' }} type="flex" justify="center">
                <Col>
                  <Form name="basic" labelCol={{ span: 9 }} wrapperCol={{ span: 20 }} initialValues={{ remember: true, }} onFinish={this.execute} >

                    <Form.Item label="ID Pacote" name="idPacote" rules={[{ required: true, message: 'Insira o ID Pacote', }]}>
                      <Input placeholder="ID Pacote" type="number"/>
                    </Form.Item>

                    <Form.Item label="ID Cliente" name="idCliente" rules={[{ required: true, message: 'Insira o ID Cliente', }]}>
                      <Input placeholder="ID Cliente" type="number"/>
                    </Form.Item>

                    <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'Insira a quantidade' },]} >
                      <Input placeholder="Quantidade" type="number" />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }} >

                      <Button type="primary" htmlType="submit">
                        Criar Contratação de Pacote
                      </Button>
                    </Form.Item>

                    <p className="mensagem-sucesso">{deveExibirSucesso ? mensagemSucesso : ''}</p>
                    <p className="mensagem-erro">{deveExibirErro ? mensagemErro : ''}</p>
                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row type="flex" align="left" style={{marginLeft: '70px'}} >
            <Col span={12}>
              <h5>Clientes:</h5>
              <ListaClientes></ListaClientes>
            </Col>
          </Row>

        </Layout>
      </React.Fragment>
    )
  }
}