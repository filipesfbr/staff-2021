import { Table, Row, Col } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaClientes extends Component{
  constructor( props ){
    super(props);
    this.api = new API();
    this.state= {
      lista: []
    }
  }

  componentDidMount(){
    this.api.listarClientes().then( response => {
      this.setState(state => {
        return {
          ...state,
          lista: response.data
        }
      })
    })
  }
  


  render (){
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Nome',
        dataIndex: 'nome',
        key: 'nome',
      },
      {
        title: 'CPF',
        dataIndex: 'cpf',
        key: 'cpf',
      }
    ]
    return(
      <React.Fragment>
        
        <Row>
          <Col >
          
        <Table style={{margin: '20px'}} columns={columns} dataSource={this.state.lista} />

          </Col>
        </Row>
      </React.Fragment>
    )
  }
}