import { Table } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaClientePacote extends Component {
  constructor(props) {
    super(props);
    this.api = new API();
    this.state = {
      lista: []
    }
  }

  componentDidMount() {
    this.api.buscarTodosClientePacote().then(response => {
      let data = response.data.map (clientePacote => {
        return { id: clientePacote.id, cliente: clientePacote.cliente.nome,
           pacote:clientePacote.pacote.id, quantidade:clientePacote.quantidade, valor: clientePacote.pacote.valor }
      })
      this.setState(state => {
        return {
          state,
          lista: data
        }
      })
    })
      
  }




  render() {
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Cliente',
        dataIndex: 'cliente',
        key: 'cliente',
      },
      {
        title: 'ID Pacote',
        dataIndex: 'pacote',
        key: 'espaco',
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade',
        key: 'quantidade'
      },
      {
        title: 'Preço',
        dataIndex: 'valor',
        key: 'valor'
      }
    ];

    return (
      <React.Fragment>

        <Table size="small" style={{margin: '30px'}} columns={columns} dataSource={this.state.lista} />


      </React.Fragment>
    )
  }
}