import { Table, Tag } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaPacotes extends Component{
  constructor( props ){
    super(props);
    this.api = new API();
    this.state= {
      lista: []
    }
  }

  componentDidMount(){
    this.api.buscarTodosPacotes().then( response => {
      let data = response.data.map( pacote => {
        return { id: pacote.id, preco: pacote.valor,
           espacos: pacote.espacoPacote.map(espacoPacote => espacoPacote.espaco.nome)}
      })
      this.setState( state => {
        return {
          ...state,
          lista: data
        }
      })
    })
  }
  


  render (){
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Preço',
        dataIndex: 'preco',
        key: 'preco'
      },
      {
        title: 'Espacos',
        key: 'espacos',
        dataIndex: 'espacos',
        render: tags => (
          <>
            {tags.map(tag => {
              return (
                <Tag color={'cyan'} key={tag}>
                  {tag.toUpperCase()}
                </Tag>
              );
            })}
          </>
        ),
      },
    ]


    return(

      <React.Fragment>
        
        <Table style={{margin: '20px'}} columns={columns} dataSource={this.state.lista}></Table>
        
      </React.Fragment>
    )
  }
}