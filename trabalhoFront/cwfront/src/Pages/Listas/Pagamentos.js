import { Table, Row, Col } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaPagamentos extends Component{
  constructor( props ){
    super(props);
    this.api = new API();
    this.state= {
      lista: []
    }
  }

  componentDidMount(){
    this.api.buscarPagamentos().then( response => {
      console.log(response.data)
      this.setState(state => {
        return {
          ...state,
          lista: response.data
        }
      })
    })
  }
  


  render (){
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Tipo Pagamento',
        dataIndex: 'tipoPagamento',
        key: 'nome',
      }
    ]
    return(
      <React.Fragment>
        
        <Row>
          <Col >
          
          <Table style={{margin: '20px'}} columns={columns} dataSource={this.state.lista} />

          </Col>
        </Row>
      </React.Fragment>
    )
  }
}