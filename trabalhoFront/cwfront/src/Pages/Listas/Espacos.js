import { Table } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaEspacos extends Component{
  constructor( props ){
    super(props);
    this.api = new API();
    this.state= {
      lista: []
    }
  }

  componentDidMount(){
    this.api.listarEspacos().then( response => {
      this.setState(state => {
        return {
          ...state,
          lista: response.data
        }
      })
    })
  }
  


  render (){
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Espaço',
        dataIndex: 'nome',
        key: 'nome',
      },
      {
        title: 'Capacidade',
        dataIndex: 'qntPessoas',
        key: 'qntPessoas',
      },
      {
        title: 'Preço',
        dataIndex: 'valor',
        key: 'valor',
      }
    ]
    return(
      <React.Fragment>
        
        <Table style={{margin: '20px'}} columns={columns} dataSource={this.state.lista} />

        
      </React.Fragment>
    )
  }
}