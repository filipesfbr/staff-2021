import { Table } from 'antd';
import React, { Component } from 'react';
import API from '../../Models/API';


export default class ListaContratacao extends Component {
  constructor(props) {
    super(props);
    this.api = new API();
    this.state = {
      lista: []
    }
  }

  componentDidMount() {
    this.api.buscarTodasContratacoes().then(response => {
      let data = response.data.map( contratacao => {
        return { id: contratacao.id, espaco: contratacao.espaco.nome, cliente: contratacao.cliente.nome,
           tipoContratacao: contratacao.tipoContratacao, quantidade: contratacao.quantidade, valor: contratacao.valor,
          prazo: contratacao.prazo}
      })
      this.setState(state => {
        return {
          state,
          lista: data
        }
      })
    })    
  }




  render() {
    const columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: 'Cliente',
        dataIndex: 'cliente',
        key: 'cliente',
      },
      {
        title: 'Espaço',
        dataIndex: 'espaco',
        key: 'espaco',
      },
      {
        title: 'Tipo Contratação',
        dataIndex: 'tipoContratacao',
        key: 'tipoContratacao',
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade',
        key: 'quantidade',
      },
      {
        title: 'Prazo',
        dataIndex: 'prazo',
        key: 'prazo',
      },
      {
        title: 'Valor R$',
        dataIndex: 'valor',
        key: 'valor',
      },
     
    ];

    return (
      <React.Fragment>

        <Table size="small" style={{margin: '40px'}} columns={columns} dataSource={this.state.lista} />


      </React.Fragment>
    )
  }
}