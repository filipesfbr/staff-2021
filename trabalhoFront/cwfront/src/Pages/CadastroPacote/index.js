import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col, Select, Layout } from 'antd';
import ListaEspacos from '../Listas/Espacos';
import API from '../../Models/API';
import EspacoPacoteModel from '../../Models/EspacoPacoteModel'
import PacoteModel from '../../Models/PacoteModel'
import '../../index.css'

export default class CadastroPacote extends React.Component {
  constructor(props) {
    super(props)
    this.api = new API();

    this.state = {
      espacoPacote: [],
      espacosAdd: [],
      mensagemSucesso: 'Pacote finalizado!',
      mensagemErro: 'Dados inválidos!',
      deveExibirSucesso: false,
      deveExibirErro: false
    }

    this.formEspacos = this.formEspacos.bind(this)
    this.formPacote = this.formPacote.bind(this)
  }

  formEspacos(values) {
    const { idEspaco, tipoContratacao, quantidade, prazo } = values
    let espaco = { id: idEspaco };
    let espacoPacote1 = new EspacoPacoteModel(espaco, tipoContratacao, parseInt(quantidade), parseInt(prazo));
    const { espacoPacote, espacosAdd } = this.state;
    espacoPacote.push(espacoPacote1);
    this.api.buscarEspaco(idEspaco).then( response => {
      espacosAdd.push(response);
      this.setState(state => {
        return {
          ...state,
          espacoPacote: espacoPacote,
          espacosAdd: espacosAdd
        }
      });
    })
    
  }

  


  formPacote(values) {
    const { valor, quantidade } = values;
    let pacote = new PacoteModel(valor, this.state.espacoPacote, quantidade);
    this.api.salvarPacote(pacote).then( response => {
      this.setState(state => {
        return {
          ...state,
          deveExibirSucesso: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirSucesso: false
          }
        });
      }, 5000)
    }).catch( e => {
      this.setState(state => {
        return {
          ...state,
          deveExibirErro: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirErro: false
          }
        });
      }, 5000)
    })

  }


  render() {
    const { deveExibirErro, deveExibirSucesso, mensagemErro, mensagemSucesso } = this.state;
    return (
      <React.Fragment>
        <Layout>

          <Row type="flex" justify="space-around" align="left">
            <Col span={12}>
              <h5>Espaços disponíveis:</h5>
              <ListaEspacos></ListaEspacos>
            </Col>

            <Col span={7}>
              <h5>1. Crie seu Pacote:</h5>

              <Row style={{ margin: '20px' }} type="flex" justify="center">
                <Col>
                  <Form name="basic" labelCol={{ span: 8 }} wrapperCol={{ span: 20 }} initialValues={{ remember: true, }} onFinish={this.formEspacos} >

                    <Form.Item label="ID Espaço" name="idEspaco" rules={[{ required: true, message: 'Insira o ID Espaço', }]}>
                      <Input placeholder="ID Espaço" type="number"/>
                    </Form.Item>

                    <Form.Item label="Tipo" name="tipoContratacao" rules={[{ required: true, message: 'Selecione o tempo' }]} >
                      <Select style={{ width: '100%' }} >
                        <Select.Option value="MINUTO">Minuto</Select.Option>
                        <Select.Option value="HORA">Hora</Select.Option>
                        <Select.Option value="TURNO">Turno</Select.Option>
                        <Select.Option value="DIARIA">Diária</Select.Option>
                        <Select.Option value="SEMANA">Semana</Select.Option>
                        <Select.Option value="MES">Mês</Select.Option>
                      </Select>
                    </Form.Item>

                    <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'Insira a quantidade' },]} >
                      <Input placeholder="Quantidade" type="number" />
                    </Form.Item>

                    <Form.Item label="Prazo" name="prazo" rules={[{ required: true, message: 'Insira o prazo' },]} >
                      <Input placeholder="Prazo" type="number"/>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

                      <Button type="primary" htmlType="submit">
                        Criar Pacote
                      </Button>

                    </Form.Item>
                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row type="flex" justify="space-around" align="left">
          
            <Col style={{border: "1px solid black"}} span={12}>
              {this.state.espacosAdd.length > 0 ?
                <ul className="lista-dados">
                  {this.state.espacosAdd.map((objeto, i) => {
                    return <li className="li-verde" key={i}>ID: {objeto.id} | Nome: {objeto.nome} </li>
                  })}
                </ul>
                : <h6>Adicione espaços ao pacote...</h6>
              }
            </Col>

            <Col span={7}>
              <h5>2. Finalize o Pacote:</h5>

              <Form name="basic" labelCol={{ span: 8 }} wrapperCol={{ span: 13 }} initialValues={{ remember: true, }} onFinish={this.formPacote} >

                <Form.Item label="Preço" name="valor" rules={[{ required: true, message: 'Insira o valor' },]} >
                  <Input prefix="R$ " />
                </Form.Item>

                <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'Insira a quantidade' },]} >
                  <Input type="number" />
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8, span: 16, }} >

                  <Button disabled={this.state.espacoPacote.length === 0} type="primary" htmlType="submit">
                    Finalizar Pacote
                  </Button>

                </Form.Item>
              </Form>

              <p className="mensagem-sucesso">{deveExibirSucesso ? mensagemSucesso : ''}</p>
              <p className="mensagem-erro">{deveExibirErro ? mensagemErro : ''}</p>
            </Col>
          </Row>

        </Layout>
      </React.Fragment>
    )
  }
}