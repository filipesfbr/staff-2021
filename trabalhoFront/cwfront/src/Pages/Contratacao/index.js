import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col, Select, Layout } from 'antd';
import ListaEspacos from '../Listas/Espacos';
import ListaClientes from '../Listas/Clientes';
import API from '../../Models/API';
import '../../index.css'
import ContratacaoModel from '../../Models/ContratacaoModel';

export default class Contratacao extends React.Component {
  constructor(props) {
    super(props)
    this.api = new API();

    this.state = {
      espacoPacote: [],
      espacosAdd: [],
      mensagemSucesso: 'Contratação realizada!',
      mensagemErro: 'Dados inválidos!',
      deveExibirSucesso: false,
      deveExibirErro: false
    }

    this.execute = this.execute.bind(this)
  }

  execute(values) {
    const { idEspaco, idCliente, tipoContratacao, quantidade, prazo } = values;
    let espaco = { id: idEspaco };
    let cliente = { id: idCliente };
    let contratacao = new ContratacaoModel(espaco, cliente, tipoContratacao, parseInt(quantidade), parseInt(prazo) );
    this.api.salvarContratacao(contratacao).then( response => {
      this.setState(state => {
        return {
          ...state,
          deveExibirSucesso: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirSucesso: false
          }
        });
      }, 2000);
    }).catch ( response => {
      this.setState(state => {
        return {
          ...state,
          deveExibirErro: true
        }
      });
      setTimeout( () => {
        this.setState(state => {
          return {
            ...state,
            deveExibirErro: false
          }
        });
      }, 2000);
    })
    
  }



  render() {
    const { deveExibirErro, deveExibirSucesso, mensagemErro, mensagemSucesso } = this.state;
    return (
      <React.Fragment>
        <Layout>

          <Row type="flex" justify="space-around" align="left">

            <Col span={12}>
              <h5>Espaços disponíveis: </h5>
              <ListaEspacos></ListaEspacos>
            </Col>

            <Col span={7}>
              <h5>1. Criar Contratação: </h5>

              <Row style={{ margin: '20px' }} type="flex" justify="center">
                <Col>
                  <Form name="basic" labelCol={{ span: 8 }} wrapperCol={{ span: 20 }} initialValues={{ remember: true, }} onFinish={this.execute} >

                    <Form.Item label="ID Espaço" name="idEspaco" rules={[{ required: true, message: 'Insira o ID Espaço', }]}>
                      <Input placeholder="ID Espaço" type="number"/>
                    </Form.Item>

                    <Form.Item label="ID Cliente" name="idCliente" rules={[{ required: true, message: 'Insira o ID Cliente', }]}>
                      <Input placeholder="ID Cliente" type="number"/>
                    </Form.Item>

                    <Form.Item label="Tipo" name="tipoContratacao" rules={[{ required: true, message: 'Selecione o tempo' }]} >
                      <Select style={{ width: '100%' }}>
                        <Select.Option value="MINUTO">Minuto</Select.Option>
                        <Select.Option value="HORA">Hora</Select.Option>
                        <Select.Option value="TURNO">Turno</Select.Option>
                        <Select.Option value="DIARIA">Diária</Select.Option>
                        <Select.Option value="SEMANA">Semana</Select.Option>
                        <Select.Option value="MES">Mês</Select.Option>
                      </Select>
                    </Form.Item>

                    <Form.Item label="Quantidade" name="quantidade" rules={[{ required: true, message: 'Insira a quantidade' },]} >
                      <Input placeholder="Quantidade" type="number" />
                    </Form.Item>

                    <Form.Item label="Prazo" name="prazo" rules={[{ required: true, message: 'Insira o prazo' },]} >
                      <Input placeholder="Prazo" type="number"/>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }} >

                      <Button type="primary" htmlType="submit">
                        Criar Contratação
                      </Button>
                    </Form.Item>

                    <p className="mensagem-sucesso">{deveExibirSucesso ? mensagemSucesso : ''}</p>
                    <p className="mensagem-erro">{deveExibirErro ? mensagemErro : ''}</p>
                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row type="flex" align="left" style={{marginLeft: '70px'}} >
            <Col span={12}>
              <h5>Clientes:</h5>
              <ListaClientes></ListaClientes>
            </Col>
          </Row>

        </Layout>
      </React.Fragment>
    )
  }
}