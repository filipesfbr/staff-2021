import React from 'react';
import 'antd/dist/antd.css';
import TipoContatoUi from '../../Components/FormularioUi/TipoContatoUi';
import API from '../../Models/API';
import { Row, Col } from 'antd';
import Layout from 'antd/lib/layout/layout';

export default class CadastroTipoContato extends React.Component {
  constructor(props) {
    super(props);
    this.api = new API();
    this.state={
      lista: []
    }
    
  }

  componentDidUpdate(){
    this.api.buscarTodosTiposContatos().then( response => {
      this.setState(state => {
        return {
          ...state,
          lista: response.data
        }
      })
    })
  }
  
  componentDidMount(){
    this.api.buscarTodosTiposContatos().then( response => {
      this.setState(state => {
        return {
          ...state,
          lista: response.data
        }
      })
    })
  }

  render() {
    return (
      <React.Fragment>
       <Layout>
        <Row type="flex" justify="space-around" align="left">
          <Col span={6}>
            <TipoContatoUi></TipoContatoUi>
          </Col>
          
          <Col span={8}>
          <div className="">
            {this.state.lista.length > 0 ? 
            <ul className="lista-dados coluna-tipo">
            {this.state.lista.map( (objeto, i) => {
              return <li className="li" key={ i }>ID: {objeto.id} | Tipo: {objeto.nome} </li>
            })}
            </ul>
            : <p>Nenhum tipo cadastrado</p>
            }
          </div>
          </Col>
        </Row>
      </Layout>
        


      </React.Fragment>
    )
  }
}