import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Row, Col, Select, Layout } from 'antd';
import API from '../../Models/API';
import '../../index.css'
import ListaClientePacote from '../Listas/ClientePacote';
import ListaContratacao from '../Listas/Contratacao'
import PagamentoModel from '../../Models/PagamentoModel';

export default class Pagamento extends React.Component {
  constructor(props) {
    super(props)
    this.api = new API();

    this.state = {
      espacoPacote: [],
      espacosAdd: [],
      mensagemSucessoPact: 'Pagamento de pacote realizado!',
      mensagemSucessoCont: 'Pagamento realizado!',
      mensagemErro: 'Dados inválidos!',
      deveExibirPct: false,
      deveExibirCont: false,
      deveExibirErro: false
    }

    this.execute = this.execute.bind(this)
  }

  execute(values) {
    const { idClientePacote, idContratacao, tipoPagamento} = values;
    if(idClientePacote === '' && idContratacao !== ''){
      let contratacao = { id: idContratacao };
      let pagamento = new PagamentoModel(null, contratacao, tipoPagamento)
      this.api.pagar(pagamento).then(response =>{
        this.setState(state => {
          return{
            ...state,
            deveExibirCont: true
          }
        })
        setTimeout(() => {
          this.setState(state => {
            return{
              ...state,
              deveExibirCont: false
            }
          })
        },2000)
      }).catch( response =>{
        this.setState(state => {
          return{
            ...state,
            deveExibirErro: true
          }
        })
        setTimeout(() => {
          this.setState(state => {
            return{
              ...state,
              deveExibirErro: false
            }
          })
        },2000)
      })
    } else{
      let clientePacote = { id: idClientePacote};
      let pagamento = new PagamentoModel(clientePacote, null,tipoPagamento)
      this.api.pagar(pagamento).then(response =>{
        this.setState(state => {
          return{
            ...state,
            deveExibirPct: true
          }
        })
        setTimeout(() => {
          this.setState(state => {
            return{
              ...state,
              deveExibirPct: false
            }
          })
        },2000)
      }).catch( response =>{
        this.setState(state => {
          return{
            ...state,
            deveExibirErro: true
          }
        })
        setTimeout(() => {
          this.setState(state => {
            return{
              ...state,
              deveExibirErro: false
            }
          })
        },2000)
      })
    }
  }



  render() {
    const { deveExibirCont, deveExibirErro, deveExibirPct, mensagemErro, mensagemSucessoCont, mensagemSucessoPact } = this.state;
    return (
      <React.Fragment>
        <Layout>

          <Row type="flex" justify="space-around" align="left">

            <Col span={12}>
              <h5>Contratações de Pacote: </h5>
              <ListaClientePacote></ListaClientePacote>
            </Col>

            <Col span={10}>
              <h5>1. Ordem de Pagamento </h5>
              <p className="orientacao-pagamento">Orientações:
                Você pode optar em pagar uma contratação de pacote,
                ou uma contratação de espaço avulso.
                Opte por um dos IDs
              </p>
              <Row style={{ margin: '20px' }} type="flex" justify="center">
                <Col>
                  <Form name="basic" labelCol={{ span: 12 }} wrapperCol={{ span: 20 }} initialValues={{ remember: true, }} onFinish={this.execute} >

                    <Form.Item label="ID Pacote" name="idClientePacote" >
                      <Input placeholder="ID Pacote" type="number" />
                    </Form.Item>

                    <Form.Item label="ID Contratação" name="idContratacao" >
                      <Input placeholder="ID Contratação" type="number" />
                    </Form.Item>

                    <Form.Item label="Tipo Pagamento" name="tipoPagamento" rules={[{ required: true, message: 'Selecione o pagamento' }]} >
                      <Select style={{ width: '100%' }}>
                        <Select.Option value="DEBITO">Débito</Select.Option>
                        <Select.Option value="CREDITO">Crédito</Select.Option>
                        <Select.Option value="DINHEIRO">Dinheiro</Select.Option>
                        <Select.Option value="TRANSFERENCIA">Transferência</Select.Option>
                      </Select>
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }} >

                      <Button type="primary" htmlType="submit">
                        Pagar
                      </Button>
                    </Form.Item>

                    <p className="mensagem-sucesso">{deveExibirCont ? mensagemSucessoCont : ''}</p>
                    <p className="mensagem-sucesso">{deveExibirPct ? mensagemSucessoPact : ''}</p>
                    <p className="mensagem-erro">{deveExibirErro ? mensagemErro : ''}</p>
                    
                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row type="flex" align="left"  >
            <Col span={20}>
              <h5>Contratações: </h5>
              <ListaContratacao></ListaContratacao>
            </Col>
          </Row>

        </Layout>
      </React.Fragment>
    )
  }
}