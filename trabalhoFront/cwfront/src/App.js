import './App.css';
import React, { Component } from 'react';
import Sidebar from './Components/Sidebar';

export default class App extends Component {
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <header>CW-Front Manager</header>
          <Sidebar></Sidebar>
        </header>
        

        <footer className="footer">
          <div>
            CW-Front Manager (front-end) em desenvolvimento por <a href="https://www.linkedin.com/in/filipeferreirabr/" target="_blank" rel="noreferrer">Filipe Ferreira</a>
          </div>
          <div>
            API Coworking de back-end desenvolvida por <a href="https://www.linkedin.com/in/artur-branco-49a836148/" target="_blank" rel="noreferrer">Artur Branco</a>
          </div>
          </footer>
      </div>
    );
  }
}
