export default class ClienteModel {
  constructor(nome, cpf, dataNascimento, contatos){
    this.nome = nome;
    this.cpf = cpf;
    this.dataNascimento = dataNascimento;
    this.contatos = contatos;
  }
}