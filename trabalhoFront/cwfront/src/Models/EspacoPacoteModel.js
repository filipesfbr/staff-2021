export default class EspacoPacoteModel{
  constructor( espaco, tipoContratacao, quantidade, prazo ){
    this.espaco = espaco;
    this.tipoContratacao = tipoContratacao;
    this.quantidade = quantidade;
    this.prazo = prazo;
  }
}