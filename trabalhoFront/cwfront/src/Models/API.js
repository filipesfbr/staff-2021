import axios from "axios"

const url = 'http://localhost:8080';
const token = localStorage.getItem('token');
const config = { headers: {Authorization : token }}

export default class API {
  
  // Usuário
  cadastrarUsuario( {nome, email, login, senha} ){
    return axios.post( `${url}/cadastrar`, { nome, email, login, senha } );
  }
  // Login
  autenticarLogin ( {login, senha} ) {
    return axios.post( `${url}/login`, { login, senha } ).then( res => localStorage.setItem('token', res.headers.authorization));
  }


  // Cliente
  listarClientes(){
    return axios.get( `${url}/cw/cliente/`, config );
  }
  salvarCliente(cliente){
    return axios.post( `${url}/cw/cliente/salvar`, cliente, config );
  }

  // Tipo Contato
  salvarTipoContato( tipoContato ){
    return axios.post( `${ url }/cw/tipoContato/salvar`, tipoContato, config ).then( res => res.data );
  };

  editarTipoContato( tipoContato, id ){
    return axios.put( `${ url }cw/tipoContato/editar/${ id }`, tipoContato, config );
  }

  buscarTodosTiposContatos(){
    return axios.get( `${url}/cw/tipoContato/`, config);
  }

  // Espaco
  salvarEspaco(espaco){
    return axios.post( `${url}/cw/espaco/salvar`, espaco, config );
  }

  listarEspacos(){
    return axios.get( `${url}/cw/espaco/`, config );
  }

  buscarEspaco(id){
    return axios.get( `${url}/cw/espaco/${id}`, config ).then( res => res.data);
  }

  // Pacote
  salvarPacote(pacote){
    return axios.post(`${url}/cw/pacote/salvar`, pacote, config);
  }

  buscarTodosPacotes(){
    return axios.get(`${url}/cw/pacote/`, config);
  }

  buscarPacote(id){
    return axios.get(`${url}/cw/pacote/${id}`, config);
  }

  // Contratação
  salvarContratacao(contratacao){
    return axios.post(`${url}/cw/contratacao/salvar`, contratacao, config);
  }

  buscarTodasContratacoes(){
    return axios.get(`${url}/cw/contratacao/`, config);
  }

  buscarContratacao(id){
    return axios.get(`${url}/cw/contratacao/${id}`, config);
  }

  // Cliente Pacote
  salvarClientePacote(clientePacote){
    return axios.post(`${url}/cw/clientePacote/salvar`, clientePacote, config);
  }

  buscarTodosClientePacote(){
    return axios.get(`${url}/cw/clientePacote/`, config);
  }

  buscarClientePacote(id){
    return axios.get(`${url}/cw/clientePacote/${id}`, config)
  }

  // Pagamento
  pagar(pagamento){
    return axios.post(`${url}/cw/pagamento/pagar`, pagamento, config)
  }
  
  buscarPagamentos(){
    return axios.get(`${url}/cw/pagamento/`, config);
  }
}