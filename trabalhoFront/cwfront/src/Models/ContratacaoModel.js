export default class ContratacaoModel {
  constructor(espaco, cliente, tipoContratacao, quantidade, prazo){
    this.espaco = espaco;
    this.cliente = cliente;
    this.tipoContratacao = tipoContratacao;
    this.quantidade = quantidade;
    this.prazo = prazo;
  }
}