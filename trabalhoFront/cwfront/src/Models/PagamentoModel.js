export default class PagamentoModel{
  constructor(clientePacote, contratacao, tipoPagamento){
    this.clientePacote = clientePacote;
    this.contratacao = contratacao;
    this.tipoPagamento = tipoPagamento;
  }
}