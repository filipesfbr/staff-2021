export default class Pacote {
  constructor( valor, espacoPacote = [], quantidade ){
    this.valor = valor;
    this.espacoPacote = espacoPacote;
    this.quantidade = quantidade;
  }
}