package br.com.dbccompany.coworkingLogs.coworkingLogs.Exception;

public class DadosInvalidosException extends Exception{
    public DadosInvalidosException(){
        super("Dados inválidos");
    }
}
