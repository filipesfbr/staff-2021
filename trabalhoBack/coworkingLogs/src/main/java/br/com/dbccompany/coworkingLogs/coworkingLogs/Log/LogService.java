package br.com.dbccompany.coworkingLogs.coworkingLogs.Log;

import br.com.dbccompany.coworkingLogs.coworkingLogs.Exception.DadosInvalidosException;
import br.com.dbccompany.coworkingLogs.coworkingLogs.Exception.LogNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LogService {

    @Autowired
    private LogRepository repository;

    private MongoTemplate mongoTemplate;

    public List<LogDTO> findAll(){
        return this.convertListToDTO(repository.findAll());
    }

    public LogDTO insert(LogDTO erroDTO) throws DadosInvalidosException {
        try{
            LogCollection erroLog = erroDTO.convert();
            return new LogDTO( repository.insert(erroLog) );
        }catch(Exception e){
            System.err.println("Erro ao inserir");
            throw new DadosInvalidosException();
        }
    }

    public List<LogDTO> buscarPorCodigo(String codigo) throws LogNaoEncontradoException {
        try{
            return this.convertListToDTO( repository.findAllByCodigo(codigo) );
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontradoException();
        }
    }

    public List<LogDTO> buscarPorTipo( String tipo ) throws LogNaoEncontradoException{
        try{
            return this.convertListToDTO( repository.findAllByTipo( tipo ) );
        }catch(Exception e){
            System.err.println("Erro ao realizar busca");
            throw new LogNaoEncontradoException();
        }
    }

    private List<LogDTO> convertListToDTO(List<LogCollection> entidades){
        ArrayList<LogDTO> itensNovo = new ArrayList<LogDTO>();
        for( LogCollection entidade:entidades){
            itensNovo.add( new LogDTO( entidade ) );
        }
        return itensNovo;
    }

}
