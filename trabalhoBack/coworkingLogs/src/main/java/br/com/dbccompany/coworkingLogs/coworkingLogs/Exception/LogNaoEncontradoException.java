package br.com.dbccompany.coworkingLogs.coworkingLogs.Exception;

public class LogNaoEncontradoException extends Exception{
    public LogNaoEncontradoException(){
        super("Não foi possível encontrar o Log informado!");
    }
}
