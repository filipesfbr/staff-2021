package br.com.dbccompany.coworkingLogs.coworkingLogs.Log;

import br.com.dbccompany.coworkingLogs.coworkingLogs.Exception.DadosInvalidosException;
import br.com.dbccompany.coworkingLogs.coworkingLogs.Exception.LogNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = "/coworkingLogs" )
public class LogController {

    @Autowired
    private LogService service;

    @GetMapping(path = "/")
    public List<LogDTO> retornarTodos() {
        return service.findAll();
    }

    @PostMapping(path = "/salvar")
    public ResponseEntity<LogDTO> salvar(@RequestBody LogDTO erro) {
        try {
            return new ResponseEntity<LogDTO>(service.insert(erro), HttpStatus.ACCEPTED);
        } catch (DadosInvalidosException e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/buscarPorCodigo/{codigo}")
    public ResponseEntity<List<LogDTO>> buscarPorCodigo(@PathVariable String codigo) {
        try {
            return new ResponseEntity<List<LogDTO>>(service.buscarPorCodigo(codigo), HttpStatus.ACCEPTED);
        } catch (LogNaoEncontradoException e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/buscarPorCodigo/{tipo}")
    public ResponseEntity<List<LogDTO>> buscarPorTipo(@PathVariable String tipo) {
        try {
            return new ResponseEntity<List<LogDTO>>(service.buscarPorTipo(tipo), HttpStatus.ACCEPTED);
        } catch (LogNaoEncontradoException e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
