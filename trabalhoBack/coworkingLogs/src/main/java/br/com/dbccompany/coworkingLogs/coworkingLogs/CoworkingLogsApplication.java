package br.com.dbccompany.coworkingLogs.coworkingLogs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingLogsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingLogsApplication.class, args);
	}

}
