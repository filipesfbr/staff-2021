package br.com.dbccompany.coworkingLogs.coworkingLogs.Log;

import org.apache.commons.logging.Log;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<LogCollection, String> {

    List<LogCollection> findAllByCodigo(String codigo);

    List<LogCollection> findAllByTipo(String tipo);

}
