package br.com.dbccompany.coworking.Repository;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class EspacoRepositoryTest {
    @Autowired
    public EspacoRepository repository;

    @Test
    public void salvarEspaco(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        repository.save(espaco);

        assertEquals(espaco.getNome(), repository.findById(espaco.getId()).get().getNome());
    }

    @Test
    public void findByNome(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        repository.save(espaco);
        assertEquals(espaco.getNome(), repository.findByNome("Sala").getNome());
    }

    @Test
    public void findById(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        repository.save(espaco);
        Optional<EspacoEntity> teste = repository.findById(espaco.getId());
        assertEquals(espaco.getNome(), teste.get().getNome());
    }


}
