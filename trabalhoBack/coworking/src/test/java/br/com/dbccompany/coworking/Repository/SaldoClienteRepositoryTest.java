package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class SaldoClienteRepositoryTest {
    @Autowired
    private SaldoClienteRepository repository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private EspacoRepository espacoRepository;

/*
    @Test
    public void salvar(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        espacoRepository.save(espaco);

        SaldoClienteId id = new SaldoClienteId();
        id.setId_espaco(espaco.getId());
        id.setId_cliente(cliente.getId());
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        saldoCliente.setQuantidade(15);
        saldoCliente.setVencimento(LocalDate.now().plusDays(10));
        repository.save(saldoCliente);

        assertEquals(1, repository.findAll().size());
    }

 */

    @Test
    public void findById(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        espacoRepository.save(espaco);

        SaldoClienteId id = new SaldoClienteId();
        id.setId_espaco(espaco.getId());
        id.setId_cliente(cliente.getId());
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        saldoCliente.setQuantidade(15);
        saldoCliente.setVencimento(LocalDate.now().plusDays(10));
        repository.save(saldoCliente);

        assertEquals("Sala", repository.findById(id).get().getEspaco().getNome());
    }

    /*
    @Test
    public void findByCliente(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        espacoRepository.save(espaco);

        SaldoClienteId id = new SaldoClienteId();
        id.setId_espaco(espaco.getId());
        id.setId_cliente(cliente.getId());
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setCliente(cliente);
        saldoCliente.setEspaco(espaco);
        saldoCliente.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        saldoCliente.setQuantidade(15);
        saldoCliente.setVencimento(LocalDate.now().plusDays(10));
        repository.save(saldoCliente);

        assertEquals("Filipe", repository.findByCliente(cliente).getCliente().getNome());
    }

     */
}
