package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Espaco_PacoteRepositoryTest {

    @Autowired
    private Espaco_PacoteRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Autowired
    private PacoteRepository pacoteRepository;

    @Test
    public void salvarEspacoPacote(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Local");
        espaco.setQtdPessoas(1);
        espaco.setValor(180.0);
        espacoRepository.save(espaco);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(70.0);
        pacoteRepository.save(pacote);

        Espaco_PacoteEntity espacoPacote = new Espaco_PacoteEntity();
        espacoPacote.setEspaco(espaco);
        espacoPacote.setPacote(pacote);
        espacoPacote.setTipoContratacao(TipoContratacaoEnum.DIARIA);
        espacoPacote.setPrazo(1);
        espacoPacote.setQuantidade(1);
        repository.save(espacoPacote);

        assertEquals(espacoPacote.getEspaco().getNome(), repository.findById(1).get().getEspaco().getNome());
    }
}
