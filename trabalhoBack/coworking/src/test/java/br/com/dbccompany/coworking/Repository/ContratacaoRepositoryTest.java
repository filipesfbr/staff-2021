package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContratacaoRepositoryTest {

    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Test
    public void salvar(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto(0.0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        repository.save(contratacao);

        assertEquals(1, repository.findAll().size());

    }
/*
    @Test
    public void findAll(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("Sala");
        espaco.setQtdPessoas(15);
        espaco.setValor(15.0);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto(0.0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        repository.save(contratacao);

        EspacoEntity espaco1 = new EspacoEntity();
        espaco1.setNome("Sala");
        espaco1.setQtdPessoas(15);
        espaco1.setValor(15.0);
        espacoRepository.save(espaco1);

        ClienteEntity cliente1 = new ClienteEntity();
        Character [] array1 = {1,2,3,1,1,1,1,1,1,1,1};
        cliente1.setNome("Filip48e");
        cliente1.setCpf(array);
        cliente1.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente1);

        ContratacaoEntity contratacao1 = new ContratacaoEntity();
        contratacao1.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao1.setEspaco(espaco1);
        contratacao1.setCliente(cliente1);
        contratacao1.setDesconto(0.0);
        contratacao1.setPrazo(1);
        contratacao1.setQuantidade(1);
        repository.save(contratacao1);


        assertEquals(2, repository.findAll().size());
    }

 */


}
