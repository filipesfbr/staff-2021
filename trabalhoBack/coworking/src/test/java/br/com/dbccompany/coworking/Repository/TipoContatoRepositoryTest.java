package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class TipoContatoRepositoryTest {
    @Autowired
    public TipoContatoRepository repository;


    @Test
    public void salvarTipo(){
        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setNome("email");
        repository.save(tipo);

        assertEquals("email", repository.findByNome("email").getNome());
    }

    @Test
    public void testerFindByNome(){
        TipoContatoEntity tipo = new TipoContatoEntity();

        tipo.setNome("email");
        repository.save(tipo);

        TipoContatoEntity resultado = repository.findByNome("email");

        assertEquals(tipo, resultado);
    }

    @Test
    public void testarFindById(){
        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setId(1);
        tipo.setNome("telefone");
        repository.save(tipo);
        Optional<TipoContatoEntity> resultado = this.repository.findById(1);

        assertEquals(resultado, this.repository.findById(1));
    }
}
