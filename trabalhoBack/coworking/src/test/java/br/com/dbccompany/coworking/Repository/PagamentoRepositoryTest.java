package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

@DataJpaTest
@Profile("test")
public class PagamentoRepositoryTest {

    @Autowired
    PagamentosRepository repository;

    @Autowired
    Cliente_PacoteRepository clientesPacotesRepository;

    @Autowired
    ContratacaoRepository contratacaoRepository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Test
    public void salvarPagamentoDeContratacao(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        espaco.setValor(30.0);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto(2.0);
        contratacao.setPrazo(156);
        contratacao.setQuantidade(8);
        contratacaoRepository.save(contratacao);

        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setContratacao(contratacao);
        pagamento.setClientePacote(null);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);

        assertEquals(1, repository.findAll().size());
    }

    @Test
    public void salvarPagamentoComClientePacote(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(75.5);
        pacoteRepository.save(pacote);

        Cliente_PacoteEntity clientePacote = new Cliente_PacoteEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(13);
        clientesPacotesRepository.save(clientePacote);

        PagamentosEntity pagamento = new PagamentosEntity();
        pagamento.setContratacao(null);
        pagamento.setClientePacote(clientePacote);
        pagamento.setTipoPagamento(TipoPagamentoEnum.DEBITO);
        repository.save(pagamento);

        assertEquals(1, repository.findAll().size());
    }
}
