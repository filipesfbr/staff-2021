package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ClienteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    /*
    @Test
    public void retornar200QuandoCosultarCliente() throws Exception {
        URI uri = new URI("/api/cliente/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .status()
                        .is(200)
                );
    }

    @Test
    public void salvarCliente() throws Exception {
        URI uri = new URI("/api/cliente/salvar");
        StringBuilder strr = new StringBuilder();
        strr.append("{\n" +
                "    \"nome\": \"jorge\",\n" +
                "    \"cpf\": \"15412541251\",\n" +
                "    \"dataNascimento\": \"2020-01-29\",\n" +
                "    \"contatos\": [\n" +
                "        {\n" +
                "            \"tipo\":{\n" +
                "                \"nome\" : \"telefone\"\n" +
                "            },\n" +
                "            \"valor\" : \"123\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"tipo\":{\n" +
                "                \"nome\" : \"email\"\n" +
                "            },\n" +
                "            \"valor\" : \"1asdasdasd\"\n" +
                "        }\n" +
                "    ]\n" +
                "}");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(strr.toString())
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

    @Test
    public void editarCliente() throws Exception {
        URI uri = new URI("/api/cliente/editar/1");
        StringBuilder strr = new StringBuilder();
        strr.append("{\n" +
                "    \"nome\": \"jorge\",\n" +
                "    \"cpf\": \"15412541251\",\n" +
                "    \"dataNascimento\": \"2020-01-29\",\n" +
                "    \"contatos\": [\n" +
                "        {\n" +
                "            \"tipo\":{\n" +
                "                \"nome\" : \"telefone\"\n" +
                "            },\n" +
                "            \"valor\" : \"123\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"tipo\":{\n" +
                "                \"nome\" : \"email\"\n" +
                "            },\n" +
                "            \"valor\" : \"1asdasdasd\"\n" +
                "        }\n" +
                "    ]\n" +
                "}");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .put(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(strr.toString())
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

     */
}
