package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class PacoteRepositoryTest {

    @Autowired
    private PacoteRepository repository;
/*
    @Test
    public void salvarPacote(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(98.0);
        repository.save(pacote);

        assertEquals(pacote.getId(), repository.findById(1).get().getId());
    }

    @Test
    public void buscarPorId(){
        PacoteEntity pacote1 = new PacoteEntity();
        PacoteEntity pacote2 = new PacoteEntity();
        PacoteEntity pacote3 = new PacoteEntity();
        pacote1.setValor(75.5);
        pacote2.setValor(45.2);
        pacote3.setValor(98.3);
        repository.save(pacote1);
        repository.save(pacote2);
        repository.save(pacote3);

        assertEquals(pacote2.getId(), repository.findById(2).get().getId());
    }

 */

    @Test
    public void pacoteNaoExiste(){
        assertFalse(repository.findById(1).isPresent());
    }

    @Test
    public void findAll(){
        PacoteEntity pacote1 = new PacoteEntity();
        PacoteEntity pacote2 = new PacoteEntity();
        PacoteEntity pacote3 = new PacoteEntity();
        pacote1.setValor(75.5);
        pacote2.setValor(45.2);
        pacote3.setValor(98.3);
        repository.save(pacote1);
        repository.save(pacote2);
        repository.save(pacote3);

        assertEquals(3, repository.findAll().size());
    }
}

