package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Cliente_PacoteRepositoryTest {

    @Autowired
    public Cliente_PacoteRepository repository;
    @Autowired
    public ClienteRepository clienteRepository;
    @Autowired
    public PacoteRepository pacoteRepository;

    /*
    @Test
    public void salvar(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(10.0);
        pacote.setId(1);
        pacoteRepository.save(pacote);

        Cliente_PacoteEntity clientePacote = new Cliente_PacoteEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        repository.save(clientePacote);
        assertEquals(1, repository.findAll().size());
    }



    @Test
    public void buscarPorPacote(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(10.0);
        pacote.setId(1);
        pacoteRepository.save(pacote);

        Cliente_PacoteEntity clientePacote = new Cliente_PacoteEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        repository.save(clientePacote);
        assertEquals(1, repository.findByPacote(pacote).getQuantidade());
    }

     */
}
