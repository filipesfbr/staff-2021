package br.com.dbccompany.coworking.Repository;


import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.Date;

@DataJpaTest
@Profile("test")
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;

    @Test
    public void salvarContato(){

        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setNome("telefone");

        ContatoEntity contato = new ContatoEntity();
        contato.setValor("35472133");
        contato.setTipo(tipo);
        repository.save(contato);

        assertEquals("35472133", repository.findById(1).get().getValor());
    }
    @Test
    public void buscarPorTipo(){
        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setNome("telefone");

        ContatoEntity contato = new ContatoEntity();
        contato.setValor("35472133");
        contato.setTipo(tipo);
        repository.save(contato);

        assertEquals("telefone", repository.findByTipo(tipo).getTipo().getNome());
    }

    @Test
    public void buscarPorCliente(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));


        TipoContatoEntity tipo = new TipoContatoEntity();
        tipo.setNome("telefone");

        ContatoEntity contato = new ContatoEntity();
        contato.setValor("35472133");
        contato.setCliente(cliente);
        contato.setTipo(tipo);
        repository.save(contato);

        assertEquals("Filipe", repository.findByCliente(cliente).getCliente().getNome());
    }

}
