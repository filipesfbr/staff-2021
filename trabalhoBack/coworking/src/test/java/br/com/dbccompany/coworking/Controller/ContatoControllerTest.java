package br.com.dbccompany.coworking.Controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;
/*
    @Test
    public void retornar200QuandoCosultarContato() throws Exception {
        URI uri = new URI("/api/contato/");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                        .status()
                        .is(200)
                );
    }


    @Test
    public void salvarContato() throws Exception {
        URI uri = new URI("/api/contato/salvar");
        StringBuilder strr = new StringBuilder();
        strr.append("{\n" +
                "    \"valor\" : \"645831\",\n" +
                "    \"tipo\": {\n" +
                "        \"nome\" : \"banana\"\n" +
                "    }\n" +
                "}");
        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(strr.toString())
                        .accept(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1));
    }

 */
}
