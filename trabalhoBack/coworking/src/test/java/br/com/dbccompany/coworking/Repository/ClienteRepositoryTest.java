package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import net.bytebuddy.asm.Advice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;


import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ClienteRepositoryTest {

    @Autowired
    private ClienteRepository repository;

    @Test
    public void salvarCliente(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setId(1);
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        repository.save(cliente);

        assertEquals("Filipe", repository.findById(1).get().getNome());
    }
/*
    @Test
    public void buscarPorId(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        repository.save(cliente);

        ClienteEntity cliente1 = new ClienteEntity();
        Character [] array1 = {1,2,1,1,1,1,1,1,1,1,1};
        cliente1.setNome("Jorge");
        cliente1.setCpf(array);
        cliente1.setDataNascimento(LocalDate.parse("2020-10-05"));
        repository.save(cliente1);

        //Optional<ClienteEntity> resultado = this.repository.findById(1);

        assertEquals("Filipe", this.repository.findById(1).get().getNome());
        assertEquals("Jorge", this.repository.findById(cliente1.getId()).get().getNome());
        assertEquals(2, cliente1.getId());

    }

 */

    @Test
    public void buscarPorNome(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        repository.save(cliente);

        assertEquals(cliente, this.repository.findByNome("Filipe"));

    }

    @Test
    public void buscarPorData(){
        ClienteEntity cliente = new ClienteEntity();
        Character [] array = {1,2,1,1,1,1,1,1,1,1,1};
        cliente.setNome("Filipe");
        cliente.setCpf(array);
        cliente.setDataNascimento(LocalDate.parse("2020-10-05"));
        repository.save(cliente);

        LocalDate data = LocalDate.parse("2020-10-05");
        assertEquals(cliente, this.repository.findByDataNascimento(data));
    }

}

