package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity {

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "id_espacos")
    protected EspacoEntity espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn (name = "id_clientes")
    protected ClienteEntity cliente;

    //@JsonIgnore
    //@OneToMany (mappedBy = "contratacao", cascade = CascadeType.ALL)
    //protected List<PagamentosEntity> pagamento;

    @Enumerated ()
    @Column (nullable = false)
    protected TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    protected Integer quantidade;

    @Column(nullable = false)
    protected Double desconto;

    @Column(nullable = false)
    protected Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

/*
    public List<PagamentosEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosEntity> pagamento) {
        this.pagamento = pagamento;
    }

 */



    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
