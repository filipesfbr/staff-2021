package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

public class PagamentoDTO {
    private Integer id;
    private Cliente_PacoteDTO clientePacote;
    private ContratacaoDTO contratacao;
    private TipoPagamentoEnum tipoPagamento;
    private SaldoClienteDTO saldoCliente;

    public PagamentoDTO(PagamentosEntity pagamentos){
        this.id = pagamentos.getId();
        this.clientePacote = pagamentos.getClientePacote() != null ? new Cliente_PacoteDTO(pagamentos.getClientePacote()) : null;
        this.contratacao = pagamentos.getContratacao() != null ? new ContratacaoDTO(pagamentos.getContratacao()) : null;
        this.tipoPagamento = pagamentos.getTipoPagamento();
    }
    public PagamentoDTO(){}

    public PagamentosEntity converter(){
        PagamentosEntity pagamentos = new PagamentosEntity();
        pagamentos.setId(this.id);
        pagamentos.setClientePacote(this.getClientePacote() != null ? this.clientePacote.converter() : null);
        pagamentos.setContratacao(this.getContratacao() != null ? this.contratacao.converter() : null);
        pagamentos.setTipoPagamento(this.tipoPagamento);
        return pagamentos;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    


    public Cliente_PacoteDTO getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_PacoteDTO clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoDTO getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoDTO contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
