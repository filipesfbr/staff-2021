package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Cliente_PacoteEntity {

    @Id
    @SequenceGenerator(name = "CLIENTE_PCT_SEQ", sequenceName = "CLIENTE_PCT_SEQ")
    @GeneratedValue(generator = "CLIENTE_PCT_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @ManyToOne (cascade = CascadeType.REFRESH)
    @JoinColumn (name = "id_cliente")
    protected ClienteEntity cliente;

    @ManyToOne (cascade = CascadeType.REFRESH)
    @JoinColumn(name = "id_pacotes")
    protected PacoteEntity pacote;

    //@OneToMany(mappedBy = "clientePacote", cascade = CascadeType.ALL)
   //protected List<PagamentosEntity> pagamento;

    protected Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }
/*
    public List<PagamentosEntity> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentosEntity> pagamento) {
        this.pagamento = pagamento;
    }

 */

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
