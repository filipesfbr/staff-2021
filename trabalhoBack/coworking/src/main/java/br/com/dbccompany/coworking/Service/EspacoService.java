package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    @Transactional (rollbackOn = Exception.class)
    public EspacoDTO salvar(EspacoEntity espaco) throws DadosInvalidosException {
        try{
            return new EspacoDTO(this.repository.save(espaco));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public EspacoDTO editar(EspacoEntity espaco, Integer id) throws DadosInvalidosException {
        try {
            espaco.setId(id);
            EspacoDTO espacoNovo = this.salvar(espaco);
            return new EspacoDTO(espacoNovo.converter());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }

    public List<EspacoDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public EspacoDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new EspacoDTO(this.repository.findById(id).get());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    //método de conversão de lista
    public List<EspacoDTO> converterLista(List<EspacoEntity> listaAConverter){
        List<EspacoDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            EspacoDTO dto = new EspacoDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
