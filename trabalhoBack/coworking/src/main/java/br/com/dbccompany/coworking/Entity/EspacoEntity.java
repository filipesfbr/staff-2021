package br.com.dbccompany.coworking.Entity;

import jdk.jfr.Enabled;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity {
    @Id
    @SequenceGenerator(name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column(nullable = false, unique = true)
    protected String nome;

    @Column(nullable = false)
    protected Integer qtdPessoas;

    @Column(nullable = false)
    protected Double valor;

    @OneToMany
    protected List<ContratacaoEntity> contratacao;

    //@OneToMany (cascade = CascadeType.ALL)
    //protected List<Espaco_PacoteEntity> espacos;

    @OneToMany(mappedBy = "espaco")
    protected List<SaldoClienteEntity> saldoCliente;

    public EspacoEntity(Integer id, String nome, Integer qtdPessoas, Double valor) {
        this.id = id;
        this.nome = nome;
        this.qtdPessoas = qtdPessoas;
        this.valor = valor;
    }

    public EspacoEntity() {
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    /*
    public List<Espaco_PacoteEntity> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<Espaco_PacoteEntity> espacos) {
        this.espacos = espacos;
    }

     */

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
