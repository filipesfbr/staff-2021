package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.Cliente_PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/clientePacote")
public class Cliente_PacoteController {

    @Autowired
    private Cliente_PacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvar(@RequestBody Cliente_PacoteDTO clientePacote){
        try {
            return new ResponseEntity<>(this.service.salvar(clientePacote.converter()), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editar(@RequestBody Cliente_PacoteDTO clientePacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(this.service.editar(clientePacote.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping (value = "/")
    @ResponseBody
    public ResponseEntity<List<Cliente_PacoteDTO>> findAll(){
        try{
            return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping (value = "/cliente/")
    @ResponseBody
    public ResponseEntity<Object> findByCliente(@RequestBody ClienteDTO cliente){
        try {
            return new ResponseEntity<>(this.service.findByCliente(cliente.converter()), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/pacote/")
    @ResponseBody
    public ResponseEntity<Object> findByPacote(@RequestBody PacoteDTO pacote){
        try{
            return new ResponseEntity<>(this.service.findByPacote(pacote.converter()), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/quantidade/{quantidade}")
    @ResponseBody
    public ResponseEntity<Object> findByQuantidade(@PathVariable Integer quantidade){
        try{
            return new ResponseEntity<>(this.service.findByQuantidade(quantidade), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/quantidades/{quantidade}")
    @ResponseBody
    public ResponseEntity<List<Cliente_PacoteDTO>> findAllByQuantidade(@PathVariable Integer quantidade){
        try{
            return new ResponseEntity<>(this.service.findAllByQuantidade(quantidade), HttpStatus.OK);
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
