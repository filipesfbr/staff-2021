package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/contratacao")
public class ContratacaoController {
    @Autowired
    private ContratacaoService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvar(@RequestBody ContratacaoDTO contratacao){
        try{
            return new ResponseEntity<>(this.service.salvar(contratacao.converter()), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editar(@RequestBody ContratacaoDTO contratacao, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(this.service.editar(contratacao.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContratacaoDTO>> findAll(){
        try {
            return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try {
            return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/espaco")
    @ResponseBody
    public ResponseEntity<Object> findByEspaco(@RequestBody EspacoDTO espacoDTO){
        try{
            return new ResponseEntity<>(this.service.findByEspaco(espacoDTO.converter()), HttpStatus.OK);
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/cliente")
    @ResponseBody
    public ResponseEntity<Object> findByCliente(@RequestBody ClienteDTO clienteDTO){
        try{
            return new ResponseEntity<>( this.service.findByCliente(clienteDTO.converter()), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/tipoContratacao")
    @ResponseBody
    public ResponseEntity<Object> findByTipoContratacao(@RequestBody TipoContratacaoEnum tipo){
        try{
            return new ResponseEntity<>(this.service.findByTipoContratacao(tipo), HttpStatus.OK);
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/quantidade/{quantidade}")
    @ResponseBody
    public ResponseEntity<Object> findByQuantidade(@PathVariable Integer quantidade){
        try{
            return new ResponseEntity<>(this.service.findByQuantidade(quantidade), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/prazo/{prazo}")
    @ResponseBody
    public ResponseEntity<Object> findByPrazo(@PathVariable Integer prazo){
        try{
            return new ResponseEntity<>(this.service.findByPrazo(prazo), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


}
