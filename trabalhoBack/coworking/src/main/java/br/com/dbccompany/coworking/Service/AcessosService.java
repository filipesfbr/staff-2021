package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository repository;


    public AcessosDTO entrar(AcessosEntity acesso) throws DadosInvalidosException {
        try {
            if (acesso.getSaldoCliente().getQuantidade() <= 0 || acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now())) {
                if (acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now())) {
                    acesso.getSaldoCliente().setQuantidade(0);
                }
                AcessosDTO dto = new AcessosDTO();
                dto.setMensagem("Saldo Insuficiente");
                return dto;
            }
            if (acesso.getData() == null) {
                acesso.setData(LocalDateTime.now());
            }
            acesso.setEntrada(true);
            AcessosDTO dto = new AcessosDTO(this.repository.save(acesso));
            dto.setMensagem(StringBuilder(acesso.getSaldoCliente().getTipoContratacao(), acesso.getSaldoCliente().getQuantidade()));
            return dto;
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public AcessosDTO sair(AcessosEntity acesso) throws DadosInvalidosException {
        try {
            if (acesso.getData() == null) {
                acesso.setData(LocalDateTime.now());
            }
            acesso.setEntrada(false);
            List<AcessosEntity> acessos = repository.findAllBySaldoClienteAndEntradaOrderByData(acesso.getSaldoCliente(), true);
            AcessosEntity ultimoAcesso = acessos.get(acessos.size() - 1);
            Duration tempoUtilizado = Duration.between(ultimoAcesso.getData(), acesso.getData());
            acesso.getSaldoCliente().setQuantidade(descontarSaldo(acesso.getSaldoCliente().getTipoContratacao(), acesso.getSaldoCliente().getQuantidade(), tempoUtilizado));
            AcessosDTO AcessoRetornoDTO = new AcessosDTO(repository.save(acesso));
            AcessoRetornoDTO.setMensagem(StringBuilder(acesso.getSaldoCliente().getTipoContratacao(), acesso.getSaldoCliente().getQuantidade()));
            return AcessoRetornoDTO;
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }


    public List<AcessosDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public AcessosDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new AcessosDTO(this.repository.findById(id).get());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public AcessosDTO findByData(Date data) throws ObjetoNaoEncontradoException {
        try {
            return new AcessosDTO(this.repository.findByData(data));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<AcessosDTO> findAllByData(Date data) throws ObjetoNaoEncontradoException {
        try {
            return this.converterLista(repository.findAllByData(data));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }



    public List<AcessosDTO> converterLista(List<AcessosEntity> listaAConverter){
        List<AcessosDTO> listaConvertida = new ArrayList<>();
        for (AcessosEntity acessosEntity : listaAConverter) {
            AcessosDTO dto = new AcessosDTO(acessosEntity);
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }

    public Integer descontarSaldo(TipoContratacaoEnum tipo, Integer saldoAtual, Duration tempoUtilizado){
        if(tipo == TipoContratacaoEnum.MINUTO){
            Integer tempoGasto = (int) tempoUtilizado.toMinutes();
            return saldoAtual - tempoGasto;
        }
        if(tipo == TipoContratacaoEnum.HORA){
            Integer tempoGasto = (int) tempoUtilizado.toHours();
            return saldoAtual - tempoGasto;
        }
        if(tipo == TipoContratacaoEnum.TURNO){
            Integer tempoGasto = (int) tempoUtilizado.toHours() / 5;
            return saldoAtual - tempoGasto;
        }
        if(tipo == TipoContratacaoEnum.DIARIA){
            Integer tempoGasto = (int) tempoUtilizado.toDays();
            return saldoAtual - tempoGasto;
        }
        if(tipo == TipoContratacaoEnum.SEMANA){
            Integer tempoGasto = (int) tempoUtilizado.toDays() / 5;
            return saldoAtual - tempoGasto;
        }
        if(tipo == TipoContratacaoEnum.MES){
            Integer tempoGasto = (int) tempoUtilizado.toDays() / 20;
            return saldoAtual - tempoGasto;
        }
        return null;
    }

    public String StringBuilder(TipoContratacaoEnum tipoContratacao, Integer saldo){
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("Tipo Contratacao: ");
        mensagem.append(tipoContratacao);
        mensagem.append("\n Saldo: ");
        mensagem.append(saldo);
        return mensagem.toString();
    }


}
