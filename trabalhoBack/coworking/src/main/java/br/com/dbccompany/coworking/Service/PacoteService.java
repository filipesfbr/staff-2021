package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.Espaco_PacoteRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {

    @Autowired
    private PacoteRepository repository;

    @Autowired
    private Espaco_PacoteRepository EPrepository;
    @Autowired
    private Espaco_PacoteRepository espacoPacoteRepository;


    @Transactional(rollbackOn = Exception.class)
    public PacoteDTO salvar(PacoteEntity pacote) throws DadosInvalidosException {
        try{
            return new PacoteDTO(this.repository.save(pacote));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public PacoteDTO editar(PacoteEntity pacote, Integer id) throws DadosInvalidosException {
        try{
            pacote.setId(id);
            PacoteDTO pacoteNovo = this.salvar(pacote);
            return new PacoteDTO(pacoteNovo.converter());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }


    public PacoteDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new PacoteDTO(this.repository.findById(id).get());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<PacoteDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAll());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public PacoteDTO findByValor(Double valor) throws ObjetoNaoEncontradoException {
        try{
            return new PacoteDTO(this.repository.findByValor(valor));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }


    public List<PacoteDTO> converterLista(List<PacoteEntity> listaAConverter){
        List<PacoteDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            PacoteDTO dto = new PacoteDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
