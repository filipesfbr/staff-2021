package br.com.dbccompany.coworking.Entity;

import org.apache.tomcat.jni.Local;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity {

    @Id
    @SequenceGenerator(name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column(nullable = false)
    protected String nome;

    @Column(nullable = false, unique = true)
    protected Character[] cpf = new Character[11];

    @Column(nullable = false)
    protected LocalDate dataNascimento;

    @OneToMany (cascade = CascadeType.ALL)
    protected List<ContatoEntity> contatos;

    @OneToMany (mappedBy = "cliente", cascade = CascadeType.ALL)
    protected List<ContratacaoEntity> contratacao;

    @OneToMany (mappedBy = "cliente", cascade = CascadeType.ALL)
    protected List<Cliente_PacoteEntity> pacotes;

    @OneToMany (mappedBy = "cliente", cascade = CascadeType.ALL)
    protected List<SaldoClienteEntity> saldoCliente;


    public ClienteEntity(Integer id, String nome, Character[] cpf, LocalDate dataNascimento) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }

    public ClienteEntity() {}

    //getters e setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character[] getCpf() {
        return cpf;
    }

    public void setCpf(Character[] cpf) {
        this.cpf = cpf;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public List<Cliente_PacoteEntity> getPacotes() {
        return pacotes;
    }

    public void setPacotes(List<Cliente_PacoteEntity> pacotes) {
        this.pacotes = pacotes;
    }

    public List<SaldoClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNacimento(LocalDate dataNacimento) {
        this.dataNascimento = dataNacimento;
    }

    public List<ContatoEntity> getContato() {
        return contatos;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contatos = contato;
    }
}
