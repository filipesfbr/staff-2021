package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EspacoDTO {
    private Integer id;
    private String nome;
    private Integer qtdPessoas;
    private String valor;
    private List<ContratacaoDTO> contratacao;
    private List<Espaco_PacoteDTO> espacos;
    private List<SaldoClienteDTO> saldoCliente;

    public EspacoDTO(EspacoEntity espaco){
        this.id = espaco.getId();
        this.nome = espaco.getNome();
        this.qtdPessoas = espaco.getQtdPessoas();
        this.valor = espaco.getValor() != null ? this.doubleParaString(espaco.getValor()) : null;
        this.contratacao = espaco.getContratacao() != null ? this.converterToDTOContr(espaco.getContratacao()) : null;
        //this.espacos = espaco.getEspacos() != null ? this.converterToDTOEspacPacot(espaco.getEspacos()) : null;
        this.saldoCliente = espaco.getSaldoCliente() != null ? this.converterToDTOSaldoClie(espaco.getSaldoCliente()): null;
    }

    public EspacoDTO() {}

    public EspacoEntity converter(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setId(this.id);
        espaco.setNome(this.nome);
        espaco.setQtdPessoas(this.qtdPessoas);
        espaco.setValor(this.getValor() != null ? this.stringParaDouble(this.valor) : null);
        espaco.setContratacao(this.contratacao != null ? this.converterToEntityContr(this.contratacao) : null);
        //espaco.setEspacos(this.espacos != null ? this.converterToEntityEspacPacot(this.espacos) : null);
        espaco.setSaldoCliente(this.saldoCliente != null ? this.converterToEntitySaldoClie(this.saldoCliente) : null);
        return espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoDTO> contratacao) {
        this.contratacao = contratacao;
    }

    public List<Espaco_PacoteDTO> getEspacos() {
        return espacos;
    }

    public void setEspacos(List<Espaco_PacoteDTO> espacos) {
        this.espacos = espacos;
    }

    public List<SaldoClienteDTO> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteDTO> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    //contratacoes
    public List<ContratacaoDTO> converterToDTOContr(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> listaConvertida = new ArrayList<>();
        for(ContratacaoEntity contratacao : contratacoes){
            listaConvertida.add(new ContratacaoDTO(contratacao));
        }
        return listaConvertida;
    }
    public List<ContratacaoEntity> converterToEntityContr(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> listaConvertida = new ArrayList<>();
        for(ContratacaoDTO contratacao : contratacoes){
            listaConvertida.add(contratacao.converter());
        }
        return listaConvertida;
    }

    //saldo_cliente
    public List<SaldoClienteDTO> converterToDTOSaldoClie(List<SaldoClienteEntity> saldoClientes){
        List<SaldoClienteDTO> listaConvertida = new ArrayList<>();
        for(SaldoClienteEntity saldoCliente : saldoClientes){
            listaConvertida.add(new SaldoClienteDTO(saldoCliente));
        }
        return listaConvertida;
    }
    public List<SaldoClienteEntity> converterToEntitySaldoClie(List<SaldoClienteDTO> saldoClientes){
        List<SaldoClienteEntity> listaConvertida = new ArrayList<>();
        for(SaldoClienteDTO saldoCliente : saldoClientes){
            listaConvertida.add(saldoCliente.converter());
        }
        return listaConvertida;
    }

    //espaco_pacote
    public List<Espaco_PacoteDTO> converterToDTOEspacPacot(List<Espaco_PacoteEntity> espacoPacotes){
        List<Espaco_PacoteDTO> listaConvertida = new ArrayList<>();
        for(Espaco_PacoteEntity espacoPacote : espacoPacotes){
            listaConvertida.add(new Espaco_PacoteDTO(espacoPacote));
        }
        return listaConvertida;
    }
    public List<Espaco_PacoteEntity> converterToEntityEspacPacot(List<Espaco_PacoteDTO> espacoPacotes){
        List<Espaco_PacoteEntity> listaConvertida = new ArrayList<>();
        for(Espaco_PacoteDTO espacoPacote : espacoPacotes){
            listaConvertida.add(espacoPacote.converter());
        }
        return listaConvertida;
    }

    public String converterEmString(Double valor){
        Locale padraoBR = new Locale("pt", "BR");
        return NumberFormat.getCurrencyInstance(padraoBR).format(valor);
    }

    public Double converterEmDouble(String valor){
        String novoValor = valor.replace("R$", "").replaceAll("\\s+", "").replace(",", ".");
        Double preco = Double.parseDouble(novoValor);
        return preco;
    }

    public String doubleParaString(Double valor){
        StringBuilder novoValor = new StringBuilder();
        novoValor.append("R$ ");
        novoValor.append(valor);
        return novoValor.toString().replace(".", ",");
    }

    public Double stringParaDouble(String valor) {
        String novoValor = valor.replace("R$", "")
                .replace(".", "")
                .replace(" ", "")
                .replace(",", ".");
        return Double.parseDouble(novoValor);
    }

}
