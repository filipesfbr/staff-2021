package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    List<TipoContatoEntity> findAll();
    Optional<TipoContatoEntity> findById(Integer id);
    List<TipoContatoEntity> findAllById(Integer id);

    TipoContatoEntity findByNome(String nome);
    List<TipoContatoEntity> findAllByNome(String nome);

    TipoContatoEntity findByContatoIn(List<ContatoEntity> contatos);
}
