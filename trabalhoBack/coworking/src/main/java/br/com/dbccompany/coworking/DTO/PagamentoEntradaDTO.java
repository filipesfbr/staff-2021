package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

public class PagamentoEntradaDTO {
        private Integer idContratacao;
        private Integer idClientePacote;
        TipoPagamentoEnum tipoPagamento;


        public PagamentoEntradaDTO(PagamentosEntity pagamento) {
            if(pagamento.getContratacao() != null){
                this.idContratacao = pagamento.getContratacao().getId();
            }
            if(pagamento.getClientePacote().getId() != null){
                this.idClientePacote = pagamento.getClientePacote().getId();
            }
            this.tipoPagamento = pagamento.getTipoPagamento();
        }
        public PagamentoEntradaDTO(){}

        public Integer getIdContratacao() {
            return idContratacao;
        }

        public void setIdContratacao(Integer idContratacao) {
            this.idContratacao = idContratacao;
        }

        public Integer getIdClientePacote() {
            return idClientePacote;
        }

        public void setIdClientePacote(Integer idClientePacote) {
            this.idClientePacote = idClientePacote;
        }

        public TipoPagamentoEnum getTipoPagamento() {
            return tipoPagamento;
        }

        public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
            this.tipoPagamento = tipoPagamento;
        }
}
