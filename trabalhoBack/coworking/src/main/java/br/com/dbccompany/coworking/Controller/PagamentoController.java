package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.*;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService service;
    @Autowired
    private EspacoService espacoService;
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ContratacaoService contratacaoService;
    @Autowired
    Cliente_PacoteService clientePacoteService;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvar(@RequestBody PagamentoEntradaDTO pagamento) {
        try {
            if (pagamento.getIdContratacao() != null) {
                ContratacaoDTO contratacaoDTO = contratacaoService.findById(pagamento.getIdContratacao());
                PagamentoDTO pagamentoDTO = new PagamentoDTO();
                pagamentoDTO.setContratacao(contratacaoDTO);
                pagamentoDTO.setTipoPagamento(pagamento.getTipoPagamento());
                return new ResponseEntity<>(this.service.salvar(pagamentoDTO.converter()), HttpStatus.CREATED);
            } else {
                Cliente_PacoteDTO clientePacoteDTO = clientePacoteService.findById(pagamento.getIdClientePacote());
                PagamentoDTO pagamentoDTO = new PagamentoDTO();
                pagamentoDTO.setClientePacote(clientePacoteDTO);
                pagamentoDTO.setTipoPagamento(pagamento.getTipoPagamento());
                return new ResponseEntity<>(this.service.salvar(pagamentoDTO.converter()), HttpStatus.CREATED);
            }
        } catch (DadosInvalidosException | ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<PagamentoDTO>> findAll(){
        try{
            return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
