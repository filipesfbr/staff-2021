package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {
    Optional<UsuarioEntity> findById(Integer integer );

    List<UsuarioEntity> findAll();

    UsuarioEntity findByLogin(String login);

    UsuarioEntity findByNome(String nome);

    UsuarioEntity findByEmail(String email);
}
