package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Espaco_PacoteRepository extends CrudRepository<Espaco_PacoteEntity, Integer> {
    List<Espaco_PacoteEntity> findAll();
    Optional<Espaco_PacoteEntity> findById(Integer id);
    List<Espaco_PacoteEntity> findAllById(Integer id);

    Espaco_PacoteEntity findByEspaco(EspacoEntity espaco);
    //Espaco_PacoteEntity findByPacote(PacoteEntity pacote);
    List<Espaco_PacoteEntity> findAllByPacote(PacoteEntity pacote);
    Espaco_PacoteEntity findByTipoContratacao(TipoContratacaoEnum tipoContratacao);

    Espaco_PacoteEntity findByQuantidade(Integer quantidade);
    List<Espaco_PacoteEntity> findAllByQuantidade(Integer quantidade);

    Espaco_PacoteEntity findByPrazo(Integer prazo);
    List<Espaco_PacoteEntity> findAllByPrazo(Integer prazo);
}
