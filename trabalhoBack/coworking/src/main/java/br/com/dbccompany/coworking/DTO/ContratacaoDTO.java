package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

import java.util.ArrayList;
import java.util.List;

public class ContratacaoDTO {
    private Integer id;
    private EspacoDTO espaco;
    private ClienteDTO cliente;
    //private List<PagamentoDTO> pagamento;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Double desconto;
    private Integer prazo;
    private String valor;

    public ContratacaoDTO(ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.espaco = new EspacoDTO(contratacao.getEspaco());
        this.cliente = new ClienteDTO(contratacao.getCliente());
        //this.pagamento = contratacao.getPagamento() != null ? this.converterToDTO(contratacao.getPagamento()) : null;
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();

    }
    public ContratacaoDTO(){}

    public ContratacaoEntity converter(){
        ContratacaoEntity cont = new ContratacaoEntity();
        cont.setId(this.id);
        cont.setEspaco(this.espaco != null ? this.espaco.converter() : null);
        cont.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
       // cont.setPagamento(this.getPagamento() != null ? this.converterToEntity(this.pagamento) : null);
        cont.setTipoContratacao(this.tipoContratacao);
        cont.setQuantidade(this.quantidade);
        cont.setDesconto(this.desconto);
        cont.setPrazo(this.prazo);
        return cont;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }
/*
    public List<PagamentoDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoDTO> pagamento) {
        this.pagamento = pagamento;
    }

 */

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<PagamentoDTO> converterToDTO(List<PagamentosEntity> pagamentos){
        List<PagamentoDTO> listaConvertida = new ArrayList<>();
        for(PagamentosEntity pagamento : pagamentos){
            listaConvertida.add(new PagamentoDTO(pagamento));
        }
        return listaConvertida;
    }
    public List<PagamentosEntity> converterToEntity(List<PagamentoDTO> pagamentos){
        List<PagamentosEntity> listaConvertida = new ArrayList<>();
        for(PagamentoDTO pagamento : pagamentos){
            listaConvertida.add(pagamento.converter());
        }
        return listaConvertida;
    }
}
