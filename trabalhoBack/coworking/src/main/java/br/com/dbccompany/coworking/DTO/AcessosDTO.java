package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;

import java.time.LocalDateTime;

public class AcessosDTO {

    private Integer id;
    protected Boolean isEntrada;
    protected LocalDateTime data;
    private SaldoClienteDTO saldoCliente;
    private String mensagem;

    public AcessosDTO(AcessosEntity acessos){
        this.id = acessos.getId();
        this.isEntrada = acessos.getEntrada();
        this.data = acessos.getData();
        //this.saldoCliente = new SaldoClienteDTO(acessos.getSaldoCliente());
    }
    public AcessosDTO(){}

    public AcessosEntity converter(){
        AcessosEntity acesso = new AcessosEntity();
        acesso.setId(this.id);
        acesso.setEntrada(this.isEntrada);
        acesso.setData(this.data);
        //acesso.setSaldoCliente(this.saldoCliente.converter());
        return acesso;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public SaldoClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteDTO saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
