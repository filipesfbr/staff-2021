package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.AcessosDTO;
import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@Controller
@RequestMapping(value = "api/acessos")
public class AcessosController {

    @Autowired
    private AcessosService service;

    @PostMapping(value = "/acessar")
    @ResponseBody
    public ResponseEntity<Object> acessar(@RequestBody AcessosDTO acessos){
        try{
            return new ResponseEntity<>(service.entrar(acessos.converter()), HttpStatus.ACCEPTED) ;
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/sair")
    @ResponseBody
    public ResponseEntity<Object> sair(@RequestBody AcessosDTO acessos){
       try{
           return new ResponseEntity<>(service.sair(acessos.converter()), HttpStatus.ACCEPTED);
        } catch (DadosInvalidosException e){
           return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
       }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<AcessosDTO>> findAll(){
        try{
            return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/data/{data}")
    @ResponseBody
    public ResponseEntity<List<AcessosDTO>> findAllByData(@RequestBody Date data){
        try{
            return new ResponseEntity<>(this.service.findAllByData(data), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
