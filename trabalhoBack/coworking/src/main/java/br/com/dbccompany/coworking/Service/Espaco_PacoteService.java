package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.DTO.Espaco_PacoteDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.Espaco_PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Espaco_PacoteService {

    @Autowired
    private Espaco_PacoteRepository repository;

    @Autowired
    private EspacoRepository espacoRepository;

    @Transactional(rollbackOn = Exception.class)
    public Espaco_PacoteDTO salvar(Espaco_PacoteEntity espacoPacote){
        espacoPacote.setEspaco(espacoRepository.findById(espacoPacote.getEspaco().getId()).get());
        return new Espaco_PacoteDTO(this.repository.save(espacoPacote));
    }

    @Transactional(rollbackOn = Exception.class)
    public Espaco_PacoteDTO editar(Espaco_PacoteEntity espacoPacote, Integer id){
        espacoPacote.setId(id);
        Espaco_PacoteDTO espacoNovo = this.salvar(espacoPacote);
        return new Espaco_PacoteDTO(espacoNovo.converter());
    }

    public Espaco_PacoteDTO findById(Integer id){
        Optional<Espaco_PacoteEntity> espaco = this.repository.findById(id);
        if(espaco.isPresent()){
            return new Espaco_PacoteDTO(espaco.get());
        }
        return null;
    }

    public List<Espaco_PacoteDTO> findAll(){
        return this.converterLista(this.repository.findAll());
    }

    public Espaco_PacoteDTO findByEspaco(EspacoEntity espaco){
        return new Espaco_PacoteDTO(this.repository.findByEspaco(espaco));
    }
    public Espaco_PacoteDTO findByTipoContratacao(TipoContratacaoEnum tipo){
        return new Espaco_PacoteDTO(this.repository.findByTipoContratacao(tipo));
    }
    public Espaco_PacoteDTO findByQuantidade(Integer quantidade){
        return new Espaco_PacoteDTO(this.repository.findByQuantidade(quantidade));
    }
    public List<Espaco_PacoteDTO> findAllByQuantidade(Integer quantidade){
        return this.converterLista(this.repository.findAllByQuantidade(quantidade));
    }


    public List<Espaco_PacoteDTO> converterLista(List<Espaco_PacoteEntity> listaAConverter){
        List<Espaco_PacoteDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            Espaco_PacoteDTO dto = new Espaco_PacoteDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
