package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Configuration
@RequestMapping(value = "/api/saldoCliente")
public class SaldoClienteController {

    @Autowired
    private SaldoClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    private ResponseEntity<List<SaldoClienteDTO>> findAll(){
       try{
           return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK);
       } catch (ObjetoNaoEncontradoException e){
           return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
       }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    private ResponseEntity<Object> findbyId(@RequestBody SaldoClienteId id){
        try {
            return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
