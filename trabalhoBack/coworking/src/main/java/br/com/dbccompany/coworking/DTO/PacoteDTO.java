package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class PacoteDTO {
    private Integer id;
    private String valor;
    private List<Espaco_PacoteDTO> espacoPacotes;

    public PacoteDTO(PacoteEntity pacote){
        this.id = pacote.getId();
        this.valor = this.doubleParaString(pacote.getValor());
        this.espacoPacotes =  pacote.getEspacoPacotes() != null ? this.converterToDTOEspacPacot(pacote.getEspacoPacotes()) : null;
    }
    public PacoteDTO(){}

    public PacoteEntity converter(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setId(this.id);
        pacote.setValor(this.getValor() != null ? this.stringParaDouble(this.valor): null);
        pacote.setEspacoPacotes(this.espacoPacotes != null ? this.converterToEntityEspacPacot(this.espacoPacotes) : null);
        return pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public List<Espaco_PacoteDTO> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espaco_PacoteDTO> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }


    //espaco_pacote
    public List<Espaco_PacoteDTO> converterToDTOEspacPacot(List<Espaco_PacoteEntity> espacoPacotes){
        List<Espaco_PacoteDTO> listaConvertida = new ArrayList<>();
        for(Espaco_PacoteEntity espacoPacote : espacoPacotes){
            listaConvertida.add(new Espaco_PacoteDTO(espacoPacote));
        }
        return listaConvertida;
    }
    public List<Espaco_PacoteEntity> converterToEntityEspacPacot(List<Espaco_PacoteDTO> espacoPacotes){
        List<Espaco_PacoteEntity> listaConvertida = new ArrayList<>();
        for(Espaco_PacoteDTO espacoPacote : espacoPacotes){
            listaConvertida.add(espacoPacote.converter());
        }
        return listaConvertida;
    }

    //cliente_pacote
    public List<Cliente_PacoteDTO> converterToDTOCliePac(List<Cliente_PacoteEntity> clientePacotes){
        List<Cliente_PacoteDTO> listaConvertida = new ArrayList<>();
        for(Cliente_PacoteEntity clientePacote : clientePacotes){
            listaConvertida.add(new Cliente_PacoteDTO(clientePacote));
        }
        return listaConvertida;
    }
    public List<Cliente_PacoteEntity> converterToEntityCliePac(List<Cliente_PacoteDTO> clientePacotes){
        List<Cliente_PacoteEntity> listaConvertida = new ArrayList<>();
        for(Cliente_PacoteDTO clientePacote : clientePacotes){
            listaConvertida.add(clientePacote.converter());
        }
        return listaConvertida;
    }

    public String converterEmString(Double valor){
        Locale padraoBR = new Locale("pt", "BR");
        return NumberFormat.getCurrencyInstance(padraoBR).format(valor);
    }

    public Double converterEmDouble(String valor){
        String novoValor = valor.replace("R$", "").replaceAll("\\s", "").replaceAll(",", ".");
        return Double.parseDouble(novoValor);
    }
    public String doubleParaString(Double valor){
        StringBuilder novoValor = new StringBuilder();
        novoValor.append("R$ ");
        novoValor.append(valor);
        return novoValor.toString().replace(".", ",");
    }

    public Double stringParaDouble(String valor) {
        String novoValor = valor.replace("R$", "")
                .replace(".", "")
                .replace(" ", "")
                .replace(",", ".");
        Double preco = Double.parseDouble(novoValor);
        return preco;
    }


}
