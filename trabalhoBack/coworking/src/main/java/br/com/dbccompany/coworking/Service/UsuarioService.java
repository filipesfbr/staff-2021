package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackOn = Exception.class)
    public UsuarioDTO save(UsuarioEntity usuario) throws DadosInvalidosException {
        try{
            BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
            usuario.setSenha(bcrypt.encode(usuario.getSenha()));
            return new UsuarioDTO(repository.save(usuario));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public  UsuarioDTO update(UsuarioEntity u, Integer id) throws DadosInvalidosException {
        try{
            u.setId(id);
            return new UsuarioDTO(this.save(u).converter());
        } catch (Exception e) {
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public UsuarioDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try {
            return new UsuarioDTO(repository.findById(id).get());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();

        }
    }

    public UsuarioDTO findByNome(String nome) throws ObjetoNaoEncontradoException {
        try {
            return new UsuarioDTO(repository.findByNome(nome));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public UsuarioDTO findByEmail(String email) throws ObjetoNaoEncontradoException {
        try {
            return new UsuarioDTO(repository.findByEmail(email));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public UsuarioDTO findByLogin(String login) throws ObjetoNaoEncontradoException {
        try {
            return new UsuarioDTO(repository.findByLogin(login));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<UsuarioDTO> findAll() throws ObjetoNaoEncontradoException {
        try {
            return parseList(repository.findAll());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<UsuarioDTO> parseList(List<UsuarioEntity> list){
        if(list == null || list.size() == 0) return null;

        List<UsuarioDTO> dtos = new ArrayList<>();
        for (UsuarioEntity usuario : list ){
            dtos.add(new UsuarioDTO(usuario));
        }
        return dtos;
    }
}
