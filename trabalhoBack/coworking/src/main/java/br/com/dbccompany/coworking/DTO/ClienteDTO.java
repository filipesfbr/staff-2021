package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClienteDTO {

    private Integer id;
    private String nome;
    private String cpf;
    private LocalDate dataNascimento;
    private List<ContatoDTO> contatos;
    //private List<ContratacaoDTO> contratacao;
    //private List<Cliente_PacoteDTO> clientePacote;
    //private List<SaldoClienteDTO> saldoCliente;

    public ClienteDTO(ClienteEntity cliente) {
        this.id = cliente.getId();
        this.nome = cliente.getNome();
        this.cpf = this.conveterParaString(cliente.getCpf());
        this.dataNascimento = cliente.getDataNascimento();
        this.contatos = this.converterToDTOContato(cliente.getContato());
        /*this.contratacao = this.converterToDTOContr(cliente.getContratacao());
        this.clientePacote = this.converterToDTOCliePac(cliente.getPacotes());
        this.saldoCliente = this.converterToDTOSaldoClie(cliente.getSaldoCliente());*/
    }
    public ClienteDTO(){}


    public ClienteEntity converter(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setId(this.id);
        cliente.setNome(this.nome);
        cliente.setCpf(this.cpf != null ? this.converteParaChar(this.cpf) : null);
        cliente.setDataNascimento(this.dataNascimento);
        cliente.setContatos(this.contatos != null ? this.converterToEntityContato(this.contatos) : null);
        /*cliente.setContratacao(this.converterToEntityContr(this.contratacao));
        cliente.setPacotes(this.converterToEntityCliePac(this.clientePacote));
        cliente.setSaldoCliente(this.converterToEntitySaldoClie(this.saldoCliente));*/
        return cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoDTO> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoDTO> contatos) {
        this.contatos = contatos;
    }

    /*
    public List<ContratacaoDTO> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoDTO> contratacao) {
        this.contratacao = contratacao;
    }

    public List<Cliente_PacoteDTO> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<Cliente_PacoteDTO> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public List<SaldoClienteDTO> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<SaldoClienteDTO> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

     */

    //contato
    public List<ContatoDTO> converterToDTOContato(List<ContatoEntity> contatos){
        List<ContatoDTO> listaConvertida = new ArrayList<>();
        for(ContatoEntity contato : contatos){
            listaConvertida.add(new ContatoDTO(contato));
        }
        return listaConvertida;
    }
    public List<ContatoEntity> converterToEntityContato(List<ContatoDTO> contatos){
        List<ContatoEntity> listaConvertida = new ArrayList<>();
        for(ContatoDTO contato : contatos){
            listaConvertida.add(contato.converter());
        }
        return listaConvertida;
    }

    //contratacoes
    public List<ContratacaoDTO> converterToDTOContr(List<ContratacaoEntity> contratacoes){
        List<ContratacaoDTO> listaConvertida = new ArrayList<>();
        for(ContratacaoEntity contratacao : contratacoes){
            listaConvertida.add(new ContratacaoDTO(contratacao));
        }
        return listaConvertida;
    }
    public List<ContratacaoEntity> converterToEntityContr(List<ContratacaoDTO> contratacoes){
        List<ContratacaoEntity> listaConvertida = new ArrayList<>();
        for(ContratacaoDTO contratacao : contratacoes){
            listaConvertida.add(contratacao.converter());
        }
        return listaConvertida;
    }

    //cliente_pacote
    public List<Cliente_PacoteDTO> converterToDTOCliePac(List<Cliente_PacoteEntity> clientePacotes){
        List<Cliente_PacoteDTO> listaConvertida = new ArrayList<>();
        for(Cliente_PacoteEntity clientePacote : clientePacotes){
            listaConvertida.add(new Cliente_PacoteDTO(clientePacote));
        }
        return listaConvertida;
    }
    public List<Cliente_PacoteEntity> converterToEntityCliePac(List<Cliente_PacoteDTO> clientePacotes){
        List<Cliente_PacoteEntity> listaConvertida = new ArrayList<>();
        for(Cliente_PacoteDTO clientePacote : clientePacotes){
            listaConvertida.add(clientePacote.converter());
        }
        return listaConvertida;
    }

    //saldo_cliente
    public List<SaldoClienteDTO> converterToDTOSaldoClie(List<SaldoClienteEntity> saldoClientes){
        List<SaldoClienteDTO> listaConvertida = new ArrayList<>();
        for(SaldoClienteEntity saldoCliente : saldoClientes){
            listaConvertida.add(new SaldoClienteDTO(saldoCliente));
        }
        return listaConvertida;
    }
    public List<SaldoClienteEntity> converterToEntitySaldoClie(List<SaldoClienteDTO> saldoClientes){
        List<SaldoClienteEntity> listaConvertida = new ArrayList<>();
        for(SaldoClienteDTO saldoCliente : saldoClientes){
            listaConvertida.add(saldoCliente.converter());
        }
        return listaConvertida;
    }

    public Character [] converteParaChar(String valor){
        Character [] array = new Character[11];
        for(int i = 0; i < valor.length(); i++){
            array[i] = valor.charAt(i);
        }
        return array;
    }

    public String conveterParaString(Character [] array){
        String cpf = "";
        for (int i = 0; i < array.length; i++) {
            cpf += array[i];

        }
        return cpf;
    }
}
