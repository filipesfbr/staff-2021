package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Espaco_PacoteEntity {

    @Id
    @SequenceGenerator(name = "ESPACO_PAC_SEQ", sequenceName = "ESPACO_PAC_SEQ")
    @GeneratedValue(generator = "ESPACO_PAC_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @ManyToOne //(cascade = CascadeType.ALL)
    @JoinColumn (name = "id_espaco")
    protected EspacoEntity espaco;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "id_pacote")
    protected PacoteEntity pacote;

    @Enumerated (EnumType.STRING)
    protected TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    protected Integer quantidade;

    @Column(nullable = false)
    protected Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }



    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
