package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTO;
import br.com.dbccompany.coworking.DTO.Espaco_PacoteDTO;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;
import br.com.dbccompany.coworking.Service.Espaco_PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/espacoPacote")
public class Espaco_PacoteController {

    @Autowired
    private Espaco_PacoteService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public Espaco_PacoteDTO salvar(@RequestBody Espaco_PacoteDTO espacoPacoteDTO){
        return this.service.salvar(espacoPacoteDTO.converter());
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espaco_PacoteDTO editar(@RequestBody Espaco_PacoteDTO espacoPacoteDTO, @PathVariable Integer id){
        return this.service.editar(espacoPacoteDTO.converter(), id);
    }

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espaco_PacoteDTO> findAll(){
        return this.service.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public Espaco_PacoteDTO findById(@PathVariable Integer id){
        return this.service.findById(id);
    }

    @GetMapping(value = "/tipoContratacao")
    @ResponseBody
    public Espaco_PacoteDTO findByTipoContratacao(@RequestBody TipoContratacaoEnum tipo){
        return this.service.findByTipoContratacao(tipo);
    }

}
