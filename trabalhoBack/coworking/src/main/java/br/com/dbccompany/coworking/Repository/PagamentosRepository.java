package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {
    List<PagamentosEntity> findAll();
    Optional<PagamentosEntity> findById(Integer id);
    List<PagamentosEntity> findAllById(Integer id);

    PagamentosEntity findByClientePacote(Cliente_PacoteEntity clientePacote);
    PagamentosEntity findByContratacao(ContratacaoEntity contratacao);
    PagamentosEntity findByTipoPagamento(TipoPagamentoEnum tipoPagamento);
}
