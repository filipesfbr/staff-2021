package br.com.dbccompany.coworking.Entity;

public enum TipoPagamentoEnum {
    DEBITO, CREDITO, DINHEIRO, TRANSFERENCIA;
}
