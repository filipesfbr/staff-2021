package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {
    List<SaldoClienteEntity> findAll();
    Optional<SaldoClienteEntity> findById(SaldoClienteId id);
    List<SaldoClienteEntity> findAllById(SaldoClienteId id);

    SaldoClienteEntity findByCliente(ClienteEntity cliente);
    SaldoClienteEntity findByEspaco(EspacoEntity espaco);

    SaldoClienteEntity findByTipoContratacao(TipoContratacaoEnum tipoContratacao);
    SaldoClienteEntity findAllByTipoContratacao(TipoContratacaoEnum tipoContratacao);

    SaldoClienteEntity findByQuantidade(Integer id);
    List<SaldoClienteEntity> findAllByQuantidade(Integer id);

    SaldoClienteEntity findByVencimento(Date data);
    List<SaldoClienteEntity> findAllByVencimento(Date data);

}
