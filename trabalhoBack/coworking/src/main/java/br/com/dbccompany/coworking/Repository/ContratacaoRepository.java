package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {
    List<ContratacaoEntity> findAll();
    Optional<ContratacaoEntity> findById(Integer id);
    List<ContratacaoEntity> findAllById(Integer id);

    ContratacaoEntity findByEspaco(EspacoEntity espaco);
    List<ContratacaoEntity> findAllByEspaco(EspacoEntity espaco);

    ContratacaoEntity findByCliente(ClienteEntity cliente);
    List<ContratacaoEntity> findAllByCliente(ClienteEntity cliente);

    ContratacaoEntity findByTipoContratacao(TipoContratacaoEnum tipoContratacao);

    ContratacaoEntity findByQuantidade(Integer quantidade);
    List<ContratacaoEntity> findAllByQuantidade(Integer quantidade);

    ContratacaoEntity findByDesconto(Double desconto);

    ContratacaoEntity findByPrazo(Integer prazo);
}
