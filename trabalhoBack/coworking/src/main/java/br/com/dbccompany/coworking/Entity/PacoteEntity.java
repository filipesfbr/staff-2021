package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column (nullable = false)
    protected Double valor;

    @OneToMany (cascade = CascadeType.ALL)
    protected List<Espaco_PacoteEntity> espacoPacotes;

    @OneToMany (mappedBy = "pacote", cascade = CascadeType.ALL)
    protected List<Cliente_PacoteEntity> clientePacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<Espaco_PacoteEntity> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espaco_PacoteEntity> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<Cliente_PacoteEntity> getClientePacotes() {
        return clientePacotes;
    }

    public void setClientePacotes(List<Cliente_PacoteEntity> clientePacotes) {
        this.clientePacotes = clientePacotes;
    }
}
