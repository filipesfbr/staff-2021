package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity {

    @Id
    @SequenceGenerator(name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
    @GeneratedValue(generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column(nullable = false)
    protected String nome;

    @OneToMany (mappedBy = "tipo")
    protected List<ContatoEntity> contato;


    public TipoContatoEntity(String nome) {
        this.nome = nome;
    }

    public TipoContatoEntity() {
    }

//getters e setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }
}
