package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;

    @Autowired
    private TipoContatoRepository repositoryTipo;

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO salvar( ClienteEntity cliente ) throws DadosInvalidosException {
        try {
            if(this.repository.findByCpf(cliente.getCpf()) == null){
                if (contemEmailTelefone(cliente))
                    return new ClienteDTO(repository.save(cliente));
            }
            throw new NullPointerException();
        } catch (Exception e) {
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public ClienteDTO editar(ClienteEntity cliente, Integer id) throws DadosInvalidosException {
       try{
           cliente.setId(id);
           ClienteDTO clienteNovo = this.salvar(cliente);
           return new ClienteDTO(clienteNovo.converter());
       } catch (Exception e){
           ErroLogsRTException.excecaoErro400(e);
           throw new DadosInvalidosException();
       }
    }

    public Boolean contemEmailTelefone(ClienteEntity cliente){
        boolean email = false;
        boolean telefone = false;
        List<ContatoEntity> contatosClientes = cliente.getContatos();
        for(ContatoEntity contato : contatosClientes){
            //TipoContatoEntity tipoContato = this.repositoryTipo.findById(contato.getTipo().getId()).orElse(null);
                if(contato.getTipo().getNome().equalsIgnoreCase("email")){
                    email = true;
                }
                if(contato.getTipo().getNome().equalsIgnoreCase("telefone")){
                    telefone = true;
                }

        }
        return email && telefone;
    }

    public ClienteDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new ClienteDTO(this.repository.findById(id).get());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<ClienteDTO> findAll() throws ObjetoNaoEncontradoException {
        try {
            return this.converterLista(this.repository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public ClienteDTO findByNome(String nome) throws ObjetoNaoEncontradoException {
        try{
            return new ClienteDTO(repository.findByNome(nome));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ClienteDTO findByCpf(Character [] cpf) throws ObjetoNaoEncontradoException {
        try{
            return new ClienteDTO(repository.findByCpf(cpf));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ClienteDTO findByDataNascimento(LocalDate data) throws ObjetoNaoEncontradoException {
        try{
            return new ClienteDTO(repository.findByDataNascimento(data));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    //método de conversão de lista
    public List<ClienteDTO> converterLista(List<ClienteEntity> listaAConverter){
        List<ClienteDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            ClienteDTO dto = new ClienteDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
    public Character [] converteParaChar(String valor){
        Character [] array = new Character[11];
        for(int i = 0; i < valor.length(); i++){
            array[i] = valor.charAt(i);
        }
        return array;
    }
    public String conveterParaString(Character [] array){
        String cpf = "";
        for (int i = 0; i < array.length; i++) {
            cpf += array[i];

        }
        return cpf;
    }
}
