package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;

public class ContatoDTO {
    private Integer id;
    private TipoContatoDTO tipo;
    //private ClienteDTO cliente;
    private String valor;

    public ContatoDTO(ContatoEntity contato){
        this.id = contato.getId();
        this.tipo = contato.getTipo() != null ? new TipoContatoDTO(contato.getTipo()) : null;
       // this.cliente = new ClienteDTO(contato.getCliente());
        this.valor = contato.getValor();
    }

    public ContatoEntity converter(){
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setTipo(this.tipo != null ? this.tipo.converter() : null);
       // contato.setCliente(this.cliente.converter());
        contato.setValor(this.valor);
        return contato;
    }

    public ContatoDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoDTO getTipo() {
        return tipo;
    }

    public void setTipo(TipoContatoDTO tipo) {
        this.tipo = tipo;
    }

    /*public ClienteDTO getCliente() {
        return cliente;
    }*/

    /*public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }*/

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
