package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.Cliente_PacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.Cliente_PacoteRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Repository.PacoteRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class Cliente_PacoteService {
    @Autowired
    private Cliente_PacoteRepository repository;

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private PacoteRepository pacoteRepository;

    @Transactional(rollbackOn = Exception.class)
    public Cliente_PacoteDTO salvar(Cliente_PacoteEntity clientePacote) throws DadosInvalidosException {
        try{
            clientePacote.setCliente(clienteRepository.findById(clientePacote.getCliente().getId()).get());
            clientePacote.setPacote(pacoteRepository.findById(clientePacote.getPacote().getId()).get());
            return new Cliente_PacoteDTO(this.repository.save(clientePacote));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }

    @Transactional(rollbackOn = Exception.class)
    public Cliente_PacoteDTO editar(Cliente_PacoteEntity clientePacote, Integer id) throws DadosInvalidosException {
        try {
            clientePacote.setId(id);
            Cliente_PacoteDTO clientePacoteNovo = this.salvar(clientePacote);
            return new Cliente_PacoteDTO(clientePacoteNovo.converter());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public List<Cliente_PacoteDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public Cliente_PacoteDTO findById(Integer id) throws ObjetoNaoEncontradoException {
       try{
           return new Cliente_PacoteDTO(this.repository.findById(id).get());
       } catch (Exception e){
           ErroLogsRTException.excecaoErro404(e);
           throw new ObjetoNaoEncontradoException();
       }
    }

    public Cliente_PacoteDTO findByCliente(ClienteEntity cliente) throws ObjetoNaoEncontradoException {
        try {
            return new Cliente_PacoteDTO(this.repository.findByCliente(cliente));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public Cliente_PacoteDTO findByPacote(PacoteEntity pacote) throws ObjetoNaoEncontradoException {
        try {
            return new Cliente_PacoteDTO(this.repository.findByPacote(pacote));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public Cliente_PacoteDTO findByQuantidade(Integer quantidade) throws ObjetoNaoEncontradoException {
        try{
            return new Cliente_PacoteDTO(this.repository.findByQuantidade(quantidade));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public List<Cliente_PacoteDTO> findAllByQuantidade(Integer quantidade) throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAllByQuantidade(quantidade));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<Cliente_PacoteDTO> converterLista(List<Cliente_PacoteEntity> listaAConverter){
        List<Cliente_PacoteDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            Cliente_PacoteDTO dto = new Cliente_PacoteDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
