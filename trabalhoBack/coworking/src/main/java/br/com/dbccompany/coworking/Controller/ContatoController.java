package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "api/contato")
public class ContatoController {

    @Autowired
    private ContatoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarContato(@RequestBody ContatoDTO contato){
        try {
            return new ResponseEntity<>(service.salvar(contato.converter()), HttpStatus.CREATED) ;
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarContato(@RequestBody ContatoDTO contato, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(contato.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try {
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/tipo/")
    @ResponseBody
    public ResponseEntity<List<ContatoDTO>> findAllByTipo(@RequestBody TipoContatoDTO tipo){
        try{
            return new ResponseEntity<>(service.findAllByTipo(tipo.converter()), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/cliente/")
    @ResponseBody
    public ResponseEntity<Object> findByCliente (@RequestBody ClienteDTO cliente){
        try {
            return new ResponseEntity<>(service.findByCliente(cliente.converter()), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
