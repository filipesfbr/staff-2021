package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackOn = Exception.class)
    public TipoContatoDTO salvar(TipoContatoEntity tipoContato) throws DadosInvalidosException {
        try{
            return new TipoContatoDTO(this.repository.save(tipoContato));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public TipoContatoDTO editar(TipoContatoEntity tipoContato, Integer id) throws DadosInvalidosException {
        try{
            tipoContato.setId(id);
            TipoContatoDTO tipoContatoNovo = this.salvar(tipoContato);
            return new TipoContatoDTO(tipoContatoNovo.converter());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public TipoContatoDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new TipoContatoDTO(this.repository.findById(id).get());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<TipoContatoDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(repository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public TipoContatoDTO findByNome(String nome) throws ObjetoNaoEncontradoException {
        try {
            return new TipoContatoDTO(repository.findByNome(nome));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }

    }
    public List<TipoContatoDTO> findAllByNome(String nome) throws ObjetoNaoEncontradoException {
        try {
            return this.converterLista(repository.findAllByNome(nome));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }


    public List<TipoContatoDTO> converterLista(List<TipoContatoEntity> listaAConverter){
        List<TipoContatoDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            TipoContatoDTO dto = new TipoContatoDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
