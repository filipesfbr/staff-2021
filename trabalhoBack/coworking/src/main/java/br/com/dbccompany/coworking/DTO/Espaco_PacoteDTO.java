package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratacaoEnum;

public class Espaco_PacoteDTO {
    private Integer id;
    private EspacoDTO espaco;
    private PacoteDTO pacote;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;


    public Espaco_PacoteDTO(Espaco_PacoteEntity espacoPacote){
        this.id = espacoPacote.getId();
        this.espaco = new EspacoDTO(espacoPacote.getEspaco());
        //this.pacote = new PacoteDTO(espacoPacote.getPacote());
        this.tipoContratacao = espacoPacote.getTipoContratacao();
        this.quantidade = espacoPacote.getQuantidade();
        this.prazo = espacoPacote.getPrazo();
    }
    public Espaco_PacoteDTO(){}

    public Espaco_PacoteEntity converter(){
        Espaco_PacoteEntity espacoPacote = new Espaco_PacoteEntity();
        espacoPacote.setId(this.id);
        espacoPacote.setEspaco(this.espaco.converter());
        //espacoPacote.setPacote(this.pacote.converter());
        espacoPacote.setTipoContratacao(this.tipoContratacao);
        espacoPacote.setQuantidade(this.quantidade);
        espacoPacote.setPrazo(this.prazo);
        return espacoPacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
