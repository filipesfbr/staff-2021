package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface Cliente_PacoteRepository extends CrudRepository<Cliente_PacoteEntity, Integer> {
    List<Cliente_PacoteEntity> findAll();
    Optional<Cliente_PacoteEntity> findById(Integer id);
    List<Cliente_PacoteEntity> findAllById(Integer id);

    Cliente_PacoteEntity findByCliente(ClienteEntity cliente);
    Cliente_PacoteEntity findByPacote(PacoteEntity pacote);
    //Cliente_PacoteEntity findByPagamentoIn(List<PagamentosEntity> pagamentos);

    Cliente_PacoteEntity findByQuantidade(Integer quantidade);
    List<Cliente_PacoteEntity> findAllByQuantidade(Integer quantidade);


}
