package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.Cliente_PacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.PagamentosEntity;

import java.util.ArrayList;
import java.util.List;

public class Cliente_PacoteDTO {

    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    //private List<PagamentoDTO> pagamento;
    private Integer quantidade;

    public Cliente_PacoteDTO(Cliente_PacoteEntity clientePacote){
        this.id = clientePacote.getId();
        this.cliente = new ClienteDTO(clientePacote.getCliente());
        this.pacote = new PacoteDTO(clientePacote.getPacote());
        //this.pagamento = clientePacote.getPagamento() != null ? this.converterToDTO(clientePacote.getPagamento()) : null;
        this.quantidade = clientePacote.getQuantidade();
    }
    public Cliente_PacoteDTO(){}

    public Cliente_PacoteEntity converter(){
        Cliente_PacoteEntity clientePacote = new Cliente_PacoteEntity();
        clientePacote.setId(this.id);
        clientePacote.setCliente(this.getCliente() != null ? this.cliente.converter() : null);
        clientePacote.setPacote(this.getPacote() != null ? this.pacote.converter() : null);
        //clientePacote.setPagamento(this.getPagamento() != null ? this.converterToEntity(this.pagamento): null);
        clientePacote.setId(this.quantidade);
        return clientePacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }
/*
    public List<PagamentoDTO> getPagamento() {
        return pagamento;
    }

    public void setPagamento(List<PagamentoDTO> pagamento) {
        this.pagamento = pagamento;
    }

 */

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public List<PagamentoDTO> converterToDTO(List<PagamentosEntity> pagamentos){
        List<PagamentoDTO> listaConvertida = new ArrayList<>();
        for(PagamentosEntity pagamento : pagamentos){
            listaConvertida.add(new PagamentoDTO(pagamento));
        }
        return listaConvertida;
    }
    public List<PagamentosEntity> converterToEntity(List<PagamentoDTO> pagamentos){
        List<PagamentosEntity> listaConvertida = new ArrayList<>();
        for(PagamentoDTO pagamento : pagamentos){
            listaConvertida.add(pagamento.converter());
        }
        return listaConvertida;
    }
}
