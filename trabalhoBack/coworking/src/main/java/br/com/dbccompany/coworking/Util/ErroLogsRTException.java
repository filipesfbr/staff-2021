package br.com.dbccompany.coworking.Util;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.ErroDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

public class ErroLogsRTException {
    private static Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);
    private static RestTemplate restTemplate = new RestTemplate(); //comunicação com a outra coworkingLogs
    private static String url = "http://localhost:8081/coworkingLogs/salvar";

    public static void excecaoErro400( Exception e ){
        String mensagemLogger = "Dados inválidos";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erroDTO = new ErroDTO( e, "ERROR", mensagemLogger, "400" );
        restTemplate.postForObject(url, erroDTO, Object.class); //responsável pela comunicação com a outra API.
    }

    public static void excecaoErro404( Exception e ){
        String mensagemLogger = "Elemento não encontrado";
        System.err.println( e.getMessage() );
        logger.error(mensagemLogger);
        ErroDTO erroDTO = new ErroDTO( e, "ERROR", mensagemLogger, "404" );
        restTemplate.postForObject(url, erroDTO, Object.class);
    }
    //A cada execução do método, ele faz a comunicação com a outra API, gerando assim a mensagem de erro.
}
