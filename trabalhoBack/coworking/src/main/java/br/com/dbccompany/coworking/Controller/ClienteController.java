package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping(value = "/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarCliente(@RequestBody ClienteDTO cliente){
        try{
            return new ResponseEntity<>(service.salvar(cliente.converter()), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarItem(@RequestBody ClienteDTO cliente, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(cliente.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<ClienteDTO>> findAll(){
       try{
           return new ResponseEntity<>(service.findAll(), HttpStatus.OK) ;
       } catch (ObjetoNaoEncontradoException e){
           return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
       }
    }

    @GetMapping(value = "{id}")
    @ResponseBody
    public ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<Object> findByNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>(service.findByNome(nome), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping (value = "/cpf/{cpf}")
    @ResponseBody
    public ResponseEntity<Object> findByCpf(@PathVariable Character [] cpf){
        try{
            return new ResponseEntity<>(service.findByCpf(cpf), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/dataNascimento/")
    @ResponseBody
    public ResponseEntity<Object> findByDataNascimento(@RequestBody LocalDate data){
        try{
            return new ResponseEntity<>(service.findByDataNascimento(data), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }


}
