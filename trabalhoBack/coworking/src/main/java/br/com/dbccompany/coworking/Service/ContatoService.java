package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.Espaco_PacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.Espaco_PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository repository;

    @Transactional (rollbackOn = Exception.class)
    public ContatoDTO salvar(ContatoEntity contato) throws DadosInvalidosException {
        try{
            return new ContatoDTO(this.repository.save(contato));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();

        }
    }

    @Transactional(rollbackOn = Exception.class)
    public ContatoDTO editar(ContatoEntity contato, Integer id) throws DadosInvalidosException {
        try {
            contato.setId(id);
            ContatoDTO contatoNovo = this.salvar(contato);
            return new ContatoDTO(contatoNovo.converter());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }

    public ContatoDTO findById(Integer id) throws ObjetoNaoEncontradoException {
       try{
           return new ContatoDTO(this.repository.findById(id).get());
       } catch (Exception e){
           ErroLogsRTException.excecaoErro404(e);
           throw new ObjetoNaoEncontradoException();
       }
    }

    public ContatoDTO findByCliente(ClienteEntity cliente) throws ObjetoNaoEncontradoException {
        try{
            return new ContatoDTO(this.repository.findByCliente(cliente));
        } catch (Exception e){
            throw new ObjetoNaoEncontradoException();
        }
    }
    public List<ContatoDTO> findAllByTipo(TipoContatoEntity tipo) throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(repository.findAllByTipo(tipo));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public List<ContatoDTO> findAllByCliente(ClienteEntity cliente){
        return this.converterLista(repository.findAllByCliente(cliente));
    }

    public List<ContatoDTO> findAll() throws ObjetoNaoEncontradoException {
        try {
            return this.converterLista(this.repository.findAll());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }


    public List<ContatoDTO> converterLista(List<ContatoEntity> listaAConverter){
        List<ContatoDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            ContatoDTO dto = new ContatoDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
