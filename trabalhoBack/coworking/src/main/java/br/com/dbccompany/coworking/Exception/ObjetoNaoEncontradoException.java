package br.com.dbccompany.coworking.Exception;

public class ObjetoNaoEncontradoException extends Exception{
    public ObjetoNaoEncontradoException(){
        super("Objeto não encontrado");
    }

}
