package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/espaco")
public class EspacoController {
    @Autowired
    private EspacoService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    private ResponseEntity<Object> salvar(@RequestBody EspacoDTO espacoDTO) {
        try{
            return new ResponseEntity<>(this.service.salvar(espacoDTO.converter()), HttpStatus.CREATED);
        }catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    private ResponseEntity<Object> editar(@RequestBody EspacoDTO espacoDTO, @PathVariable Integer id) {
        try{
            return new ResponseEntity<>(this.service.editar(espacoDTO.converter(), id), HttpStatus.CREATED);
        }catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    private ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(this.service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    private ResponseEntity<List<EspacoDTO>> findAll(){
        try {
            return new ResponseEntity<>(this.service.findAll(), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
