package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping (value = "/api/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService service;

    @PostMapping(value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarUsuario(@RequestBody UsuarioDTO usuario){
        try{
            return new ResponseEntity<>( service.save(usuario.converter()), HttpStatus.CREATED);
        }catch (DadosInvalidosException e){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editUsuario(@RequestBody UsuarioDTO u, @PathVariable Integer id){
        try{
            return new ResponseEntity<>( service.update(u.converter(),id), HttpStatus.CREATED);
        }catch (DadosInvalidosException e){
            return new ResponseEntity<>( null, HttpStatus.BAD_REQUEST );
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<Object> findAll(){
        try{
            return new ResponseEntity<>( service.findAll(), HttpStatus.OK);
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>( null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<Object> findByNome(@PathVariable String nome ){
        try{
            return new ResponseEntity<>(service.findByNome(nome), HttpStatus.OK);
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND );
        }
    }

    @GetMapping(value = "/email/{email}")
    @ResponseBody
    public ResponseEntity<Object> findByEmail( @PathVariable String email ){
        try{
            return new ResponseEntity<>(service.findByEmail(email), HttpStatus.OK );
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>( e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/login/{login}")
    @ResponseBody
    public ResponseEntity<Object> findByLogin( @PathVariable String login ){
        try{
            return new ResponseEntity<>( service.findByLogin(login), HttpStatus.OK );
        }catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND );
        }
    }

}
