package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.CoworkingApplication;
import br.com.dbccompany.coworking.DTO.SaldoClienteDTO;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository repository;


    @Transactional (rollbackOn = Exception.class)
    public SaldoClienteDTO salvar(SaldoClienteEntity saldoCliente) throws DadosInvalidosException {
        try{
            return new SaldoClienteDTO(this.repository.save(saldoCliente));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public List<SaldoClienteDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.repository.findAll());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public SaldoClienteDTO findById(SaldoClienteId id) throws ObjetoNaoEncontradoException {
        try{
            return new SaldoClienteDTO(repository.findById(id).get());
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    public List<SaldoClienteDTO> converterLista(List<SaldoClienteEntity> listaAConverter){
        List<SaldoClienteDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            SaldoClienteDTO dto = new SaldoClienteDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }


}
