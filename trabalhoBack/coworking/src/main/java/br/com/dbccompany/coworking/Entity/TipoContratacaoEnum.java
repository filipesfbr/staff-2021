package br.com.dbccompany.coworking.Entity;

import javax.persistence.Enumerated;

public enum TipoContratacaoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES;
}
