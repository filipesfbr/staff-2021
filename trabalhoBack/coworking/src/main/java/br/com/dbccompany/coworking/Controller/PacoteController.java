package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api/pacote")
public class PacoteController {

    @Autowired
    private PacoteService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    private ResponseEntity<Object> salvar(@RequestBody PacoteDTO pacote){
        try{
            return new ResponseEntity<>(service.salvar(pacote.converter()), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    private ResponseEntity<Object> editar(@RequestBody PacoteDTO pacote, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(pacote.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    private ResponseEntity<List<PacoteDTO>> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    private ResponseEntity<Object> findById(@PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/valor/{valor}")
    @ResponseBody
    private ResponseEntity<Object> findByValor(@PathVariable Double valor){
        try{
            return new ResponseEntity<>(service.findByValor(valor), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
