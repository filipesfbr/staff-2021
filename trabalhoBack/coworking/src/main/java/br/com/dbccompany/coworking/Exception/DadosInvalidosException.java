package br.com.dbccompany.coworking.Exception;

public class DadosInvalidosException extends Exception{
    public DadosInvalidosException(){
        super("Dados inválidos para criar o objeto");
    }
}
