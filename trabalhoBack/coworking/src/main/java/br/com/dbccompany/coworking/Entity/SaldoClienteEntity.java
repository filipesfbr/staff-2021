package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoClienteEntity {

    @EmbeddedId
    protected SaldoClienteId id;

    @JsonIgnore
    @ManyToOne (cascade = CascadeType.ALL)
    @MapsId("id_cliente")
    protected ClienteEntity cliente;

    @JsonIgnore //rever conceito
    @ManyToOne (cascade = CascadeType.ALL)
    @MapsId("id_espaco")
    protected EspacoEntity espaco;

    @OneToMany(mappedBy = "saldoCliente", cascade = CascadeType.ALL)
    protected List<AcessosEntity> acessos;

    @Column(nullable = false)
    protected TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    protected Integer quantidade;

    @Column(nullable = false)
    protected LocalDate vencimento;


    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }
}
