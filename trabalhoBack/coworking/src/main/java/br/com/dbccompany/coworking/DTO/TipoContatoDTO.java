package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

import java.util.ArrayList;
import java.util.List;

public class TipoContatoDTO {
    private Integer id;
    private String nome;
    private List<ContatoDTO> contato;

    public TipoContatoDTO(TipoContatoEntity tipoContato){
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
      //  this.contato = this.converterToDTOContato(tipoContato.getContato());
    }
    public TipoContatoDTO(){}

    public TipoContatoEntity converter(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);
        //tipoContato.setContato(this.converterToEntityContato(this.contato));
        return tipoContato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoDTO> getContato() {
        return contato;
    }

    public void setContato(List<ContatoDTO> contato) {
        this.contato = contato;
    }

    public List<ContatoDTO> converterToDTOContato(List<ContatoEntity> contatos){
        List<ContatoDTO> listaConvertida = new ArrayList<>();
        for(ContatoEntity contatoEntity : contatos){
            listaConvertida.add(new ContatoDTO(contatoEntity));
        }
        return listaConvertida;
    }
    public List<ContatoEntity> converterToEntityContato(List<ContatoDTO> contatos){
        List<ContatoEntity> listaConvertida = new ArrayList<>();
        for(ContatoDTO contatoEntity : contatos){
            listaConvertida.add(contatoEntity.converter());
        }
        return listaConvertida;
    }
}
