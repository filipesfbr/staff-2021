package br.com.dbccompany.coworking.Entity;


import javax.persistence.*;

@Entity
public class ContatoEntity {

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "id_tipo")
    protected TipoContatoEntity tipo;

    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "id_cliente")
    protected ClienteEntity cliente;

    @Column(nullable = false, unique = true)
    protected String valor;

    public ContatoEntity(TipoContatoEntity tipoContato, String valor) {
        this.tipo = tipoContato;
        this.valor = valor;
    }

    public ContatoEntity(){}

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipo() {
        return tipo;
    }

    public void setTipo(TipoContatoEntity tipoContato) {
        this.tipo = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
