package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class PagamentosEntity {
    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn (name = "id_cliente_pacote")
    protected Cliente_PacoteEntity clientePacote;

    @JsonIgnore
    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "id_contratacao")
    protected ContratacaoEntity contratacao;

    @Column(nullable = false)
    protected TipoPagamentoEnum tipoPagamento;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente_PacoteEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Cliente_PacoteEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
