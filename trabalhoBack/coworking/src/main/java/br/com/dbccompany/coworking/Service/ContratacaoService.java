package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.ClienteRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacoRepository;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private EspacoRepository espacoRepository;

    @Transactional (rollbackOn = Exception.class)
    public ContratacaoDTO salvar(ContratacaoEntity contratacao) throws DadosInvalidosException {
        try{
            contratacao.setCliente(clienteRepository.findById(contratacao.getCliente().getId()).get());
            contratacao.setEspaco(espacoRepository.findById(contratacao.getEspaco().getId()).get());

            ContratacaoEntity contratac = this.contratacaoRepository.save(contratacao);
            Integer quantidadeEmMinuto = this.converterParaMinuto(contratacao);
            Double precoContratacao = contratac.getEspaco().getValor() * quantidadeEmMinuto;
            Double precoDesconto = this.desconto(precoContratacao, contratac.getDesconto());
            String conversaoPreco = this.converterDinheiro(precoDesconto);
            ContratacaoDTO dto = new ContratacaoDTO(contratac);
            dto.setValor(conversaoPreco);
            return dto;
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }

    @Transactional(rollbackOn = Exception.class)
    public ContratacaoDTO editar(ContratacaoEntity contratacao, Integer id) throws DadosInvalidosException {
        try {
            contratacao.setId(id);
            ContratacaoDTO contratacaoNova = this.salvar(contratacao);
            return new ContratacaoDTO(contratacaoNova.converter());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }

    }

    public ContratacaoDTO findById(Integer id) throws ObjetoNaoEncontradoException {
        try{
            return new ContratacaoDTO(this.contratacaoRepository.findById(id).get());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public List<ContratacaoDTO> findAll() throws ObjetoNaoEncontradoException {
        try{
            return this.converterLista(this.contratacaoRepository.findAll());
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ContratacaoDTO findByEspaco(EspacoEntity espaco) throws ObjetoNaoEncontradoException {
        try{
            return new ContratacaoDTO(this.contratacaoRepository.findByEspaco(espaco));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ContratacaoDTO findByCliente(ClienteEntity cliente) throws ObjetoNaoEncontradoException {
        try{
            return new ContratacaoDTO(this.contratacaoRepository.findByCliente(cliente));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ContratacaoDTO findByTipoContratacao(TipoContratacaoEnum tipo) throws ObjetoNaoEncontradoException {
        try{
            return new ContratacaoDTO(this.contratacaoRepository.findByTipoContratacao(tipo));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ContratacaoDTO findByQuantidade(Integer quantidade) throws ObjetoNaoEncontradoException {
        try {
            return new ContratacaoDTO(this.contratacaoRepository.findByQuantidade(quantidade));
        } catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }
    public ContratacaoDTO findByPrazo(Integer prazo) throws ObjetoNaoEncontradoException {
        try{
            return new ContratacaoDTO(this.contratacaoRepository.findByPrazo(prazo));
        }catch (Exception e){
            ErroLogsRTException.excecaoErro404(e);
            throw new ObjetoNaoEncontradoException();
        }
    }

    //funções auxiliares
    public Double desconto(Double valor, Double desconto){
        return valor - ((valor * desconto)/100);
    }

    public Integer converterParaMinuto(ContratacaoEntity contratacao){
        TipoContratacaoEnum tipoContratacao = contratacao.getTipoContratacao();
        Integer quantidade = contratacao.getQuantidade();
        if(tipoContratacao == TipoContratacaoEnum.MINUTO)
            return quantidade;
        if(tipoContratacao == TipoContratacaoEnum.HORA)
            return quantidade * 60;
        if(tipoContratacao == TipoContratacaoEnum.TURNO)
            return quantidade * 60 * 5;
        if (tipoContratacao == TipoContratacaoEnum.DIARIA)
            return quantidade * 60 * 10;
        if(tipoContratacao == TipoContratacaoEnum.SEMANA)
            return quantidade * 60 * 70;
        if(tipoContratacao == TipoContratacaoEnum.MES)
            return quantidade * 60 * 70 * 4;

        return null;
    }

    public String converterDinheiro(Double valor){
        StringBuilder str = new StringBuilder();
        str.append("R$ " ).append(valor).toString().replace(".", ",");
        return str.toString();
    }

    public List<ContratacaoDTO> converterLista(List<ContratacaoEntity> listaAConverter){
        List<ContratacaoDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            ContratacaoDTO dto = new ContratacaoDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }
}
