package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessosEntity {
    @Id
    @SequenceGenerator(name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column(nullable = false)
    protected Boolean entrada;

    @Column
    protected LocalDateTime data;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumns({
            @JoinColumn( name = "id_cliente", nullable = false ),
            @JoinColumn( name = "id_espaco", nullable = false )
    })
    private SaldoClienteEntity saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
