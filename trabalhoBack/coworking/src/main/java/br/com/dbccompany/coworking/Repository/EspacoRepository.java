package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    List<EspacoEntity> findAll();
    Optional<EspacoEntity> findById(Integer id);
    List<EspacoEntity> findAllById(Integer id);

    EspacoEntity findByNome(String nome);
    List<EspacoEntity> findAllByNome(String nome);

    List<EspacoEntity> findAllByQtdPessoas(Integer qtdPessoas);

}
