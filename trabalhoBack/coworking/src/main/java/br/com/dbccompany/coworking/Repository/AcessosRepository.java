package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.AcessosEntity;
import br.com.dbccompany.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {

    List<AcessosEntity> findAll();

    Optional<AcessosEntity> findById(Integer id);
    List<AcessosEntity> findAllById(Integer id);

    AcessosEntity findByData(Date data);
    List<AcessosEntity> findAllByData(Date data);

    List<AcessosEntity> findAllBySaldoClienteAndEntradaOrderByData (SaldoClienteEntity saldoCliente, Boolean is_entrada);


}
