package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping (value = "/api/tipocontato")
public class TipoContatoController {

    @Autowired
    private TipoContatoService service;

    @PostMapping (value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> salvarTipoContato(@RequestBody TipoContatoDTO tipo){
        try{
            return new ResponseEntity<>(service.salvar(tipo.converter()), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping (value = "/editar/{id}")
    @ResponseBody
    public ResponseEntity<Object> editarTipoContato(@RequestBody TipoContatoDTO tipo, @PathVariable Integer id){
        try{
            return new ResponseEntity<>(service.editar(tipo.converter(), id), HttpStatus.CREATED);
        } catch (DadosInvalidosException e){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/")
    @ResponseBody
    public ResponseEntity<List<TipoContatoDTO>> findAll(){
        try{
            return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public ResponseEntity<Object> findByNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>(service.findByNome(nome), HttpStatus.OK) ;
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/nomes/{nome}")
    @ResponseBody
    public ResponseEntity<List<TipoContatoDTO>> findAllByNome(@PathVariable String nome){
        try{
            return new ResponseEntity<>(service.findAllByNome(nome), HttpStatus.OK);
        } catch (ObjetoNaoEncontradoException e){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
