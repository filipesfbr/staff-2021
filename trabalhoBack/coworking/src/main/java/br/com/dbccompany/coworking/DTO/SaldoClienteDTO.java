package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaldoClienteDTO {
    private Integer idCliente;
    private Integer idEspaco;
    private List<AcessosDTO> acessos;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;

    public SaldoClienteDTO(SaldoClienteEntity saldoCliente){
        this.idCliente = saldoCliente.getId().getId_cliente();
        this.idEspaco = saldoCliente.getId().getId_espaco();
        this.acessos = this.converterToDTO(saldoCliente.getAcessos());
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
    }
    public SaldoClienteDTO(){}

    public SaldoClienteEntity converter(){
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        SaldoClienteId id = new SaldoClienteId(this.idCliente, this.idEspaco);
        saldoCliente.setId(id);
        saldoCliente.setAcessos(this.converterToEntity(this.acessos));
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        return saldoCliente;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessosDTO> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosDTO> acessos) {
        this.acessos = acessos;
    }

    public List<AcessosDTO> converterToDTO(List<AcessosEntity> acessos){
        List<AcessosDTO> listaConvertida = new ArrayList<>();
        for(AcessosEntity acesso : acessos){
            listaConvertida.add(new AcessosDTO(acesso));
        }
        return listaConvertida;
    }
    public List<AcessosEntity> converterToEntity(List<AcessosDTO> acessos){
        List<AcessosEntity> listaConvertida = new ArrayList<>();
        for(AcessosDTO acesso : acessos){
            listaConvertida.add(acesso.converter());
        }
        return listaConvertida;
    }
}
