package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClienteDTO;
import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.DadosInvalidosException;
import br.com.dbccompany.coworking.Exception.ObjetoNaoEncontradoException;
import br.com.dbccompany.coworking.Repository.*;
import br.com.dbccompany.coworking.Util.ErroLogsRTException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentosRepository repository;

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;
    @Autowired
    private ContratacaoRepository contratacaoRepository;
    @Autowired
    private Cliente_PacoteRepository clientePacoteRepository;
    @Autowired
    private PacoteRepository pacoteRepository;
    @Autowired
    private Espaco_PacoteRepository espacoPacoteRepository;

    public PagamentoDTO salvar(PagamentosEntity pagamento) throws DadosInvalidosException {
        try {
            //pagamento.setContratacao(contratacaoRepository.findById(pagamento.getContratacao().getId()).get());
            //pagamento.setClientePacote(clientePacoteRepository.findById(pagamento.getClientePacote().getId()).get());
            if (pagamento.getContratacao() == null && pagamento.getClientePacote() == null || pagamento.getContratacao() != null && pagamento.getClientePacote() != null) {
                return null;
            }
            PagamentoDTO pagamentoDTO = new PagamentoDTO(repository.save(pagamento));
            if (pagamento.getContratacao() != null)
                this.pagarContratacao(pagamento.getContratacao());
            else
                this.pagarClientePacote(pagamento.getClientePacote());

            return pagamentoDTO;
        } catch (Exception e){
            ErroLogsRTException.excecaoErro400(e);
            throw new DadosInvalidosException();
        }
    }

    public List<PagamentoDTO> findAll() throws ObjetoNaoEncontradoException {
       try{
           return this.converterLista(this.repository.findAll());
       } catch (Exception e){
           ErroLogsRTException.excecaoErro404(e);
           throw new ObjetoNaoEncontradoException();
       }
    }

    @Transactional(rollbackOn = Exception.class)
    public void pagarContratacao(ContratacaoEntity contratacao){
        SaldoClienteId id = new SaldoClienteId();
        id.setId_cliente(contratacao.getCliente().getId());
        id.setId_espaco(contratacao.getEspaco().getId());

        if(existeSaldoCliente(id)){
            SaldoClienteEntity saldoCliente = this.saldoClienteRepository.findById(id).get();
            saldoCliente.setVencimento(saldoCliente.getVencimento().plusDays(contratacao.getPrazo()));
            saldoCliente.setQuantidade(saldoCliente.getQuantidade() + contratacao.getQuantidade());
            this.saldoClienteRepository.save(saldoCliente);
        }else{
            SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
            saldoCliente.setId(id);
            saldoCliente.setCliente(contratacao.getCliente());
            saldoCliente.setEspaco(contratacao.getEspaco());
            saldoCliente.setTipoContratacao(contratacao.getTipoContratacao());
            saldoCliente.setQuantidade(contratacao.getQuantidade());
            saldoCliente.setVencimento(LocalDate.now().plusDays(contratacao.getPrazo()));
            this.saldoClienteRepository.save(saldoCliente);
        }
    }

    @Transactional(rollbackOn = Exception.class)
    public void pagarClientePacote(Cliente_PacoteEntity clientePacote) {
        Optional<PacoteEntity> pacote = pacoteRepository.findById(clientePacote.getPacote().getId());
        ClienteEntity cliente = clientePacote.getCliente();
        Integer idCliente = cliente.getId();
        if (pacote.isPresent()) {
            List<Espaco_PacoteEntity> lista = espacoPacoteRepository.findAllByPacote(pacote.get());
            for (Espaco_PacoteEntity espacos : lista) {

                Integer idEspaco = espacos.getEspaco().getId();
                SaldoClienteId id = new SaldoClienteId(idCliente, idEspaco);
                Optional<SaldoClienteEntity> saldoCliente = saldoClienteRepository.findById(id);

                if (saldoCliente.isPresent()) {
                    LocalDate vencimento = saldoCliente.get().getVencimento();
                    vencimento = vencimento.plusDays(espacos.getPrazo());
                    saldoCliente.get().setVencimento(vencimento);
                    saldoClienteRepository.save(saldoCliente.get());
                } else {
                    SaldoClienteEntity saldoClienteNovo = new SaldoClienteEntity();
                    Integer quantidade = espacos.getQuantidade();
                    TipoContratacaoEnum tipoContratacao = espacos.getTipoContratacao();
                    Integer prazo = espacos.getPrazo();
                    saldoClienteNovo.setCliente(clientePacote.getCliente());
                    saldoClienteNovo.setEspaco(espacos.getEspaco());
                    saldoClienteNovo.setVencimento(LocalDate.now().plusDays(espacos.getPrazo()));
                    saldoClienteNovo.setQuantidade(espacos.getQuantidade());
                    saldoClienteNovo.setTipoContratacao(espacos.getTipoContratacao());
                    saldoClienteNovo.setId(id);
                    saldoClienteRepository.save(saldoClienteNovo);
                }
            }
        }
    }




    public List<PagamentoDTO> converterLista(List<PagamentosEntity> listaAConverter){
        List<PagamentoDTO> listaConvertida = new ArrayList<>();
        for(int i = 0; i < listaAConverter.size(); i++){
            PagamentoDTO dto = new PagamentoDTO(listaAConverter.get(i));
            listaConvertida.add(dto);
        }
        return listaConvertida;
    }

    public Boolean existeSaldoCliente(SaldoClienteId id){
        return this.saldoClienteRepository.findById(id).isPresent();
    }
}
